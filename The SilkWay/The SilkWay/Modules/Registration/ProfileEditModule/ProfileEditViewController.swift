//
//  RegistrationModuleViewController.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 22/06/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import UIKit
import SnapKit
import BetterSegmentedControl

final class ProfileEditViewController: UIViewController {
    // MARK: Constants
    private enum Constants {
        static let offset: CGFloat = 20
    }
    
    // MARK: Subviews
    private let textFieldBankCardView: TextFieldView = {
        let textField = TextFieldView()
        textField.text = "..."
        textField.configure(id: "Номер карты", title: "Номер карты", placeholder: "Номер карты", maskString: nil)
        textField.isEnable = false
        
        return textField
    }()
    
    
    // MARK: IBOutlets
    
    // MARK: Properties
    var scrollView = UIScrollView()
    var box = UIView()
    
    var selectFoto = SelectFotoView()
    var textFieldViewName = TextFieldView()
    var textFieldViewSurName = TextFieldView()
    var textFieldViewOrganizationName = TextFieldView()
    var textFieldViewOrganizationINN = TextFieldView()
    var textFieldViewOrganizationOGRN = TextFieldView()
    var textFieldViewOrganizationRepresentativeName = TextFieldView()
    var textFieldViewOrganizationRepresentativeSurName = TextFieldView()
    var textFieldViewOrganizationRepresentativePosition = TextFieldView()
    
    var last = UIView()
    var presenter: ProfileEditPresenterProtocol?
    
    // MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter?.start()
    }
     override func loadView() {
        super.loadView()
    }
    
    func setPresenter(presenter: ProfileEditPresenterProtocol) {
        self.presenter = presenter
    }
    
    func configureLayoutBox() {
        self.view.addSubview(scrollView)
        scrollView.addSubview(box)
        scrollView.showsVerticalScrollIndicator = false
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        box.backgroundColor = .clear
        box.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.centerX.top.bottom.equalToSuperview()
        }
        if #available(iOS 12.0, *) , traitCollection.userInterfaceStyle == .dark {
            view.backgroundColor = .theBlack()
        } else {
            view.backgroundColor = .white
        }
    }
    
    func configureLayoutPerson(userProfile: UserProfile) {
        for obj in box.subviews {
            obj.removeFromSuperview()
        }
        
        let newPrifoleMode = presenter?.newProfileMode ?? true
        
        let backButton = UIButton()
        backButton.setImage(R.image.back(), for: .normal)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: -20, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(tapBack), for: .touchUpInside)
        box.addSubview(backButton)
        backButton.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(box)
            make.left.equalTo(box).offset(Constants.offset)
            make.height.width.equalTo(40)
        }
        backButton.isHidden = newPrifoleMode
        
        let titleLabel = UILabel()
        box.addSubview(titleLabel)
        titleLabel.text = newPrifoleMode ? R.string.localizable.registrationModuleTitle() : R.string.localizable.registrationModuleTitleEdit()
        titleLabel.font = .h1TitleFont()
        titleLabel.numberOfLines = 0
        titleLabel.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(backButton.snp.bottom).offset(8)
            make.trailing.equalTo(box).inset(Constants.offset)
            make.leading.equalTo(box).offset(Constants.offset)
            make.height.equalTo(80)
        }
        
        if newPrifoleMode {
            let segmentedControl = BetterSegmentedControl()
            box.addSubview(segmentedControl)
            var defaultIndex = 0
            if userProfile.type == .organization {
                defaultIndex = 1
            }
            segmentedControl.setStyle(style: .blue, defaultIndex: defaultIndex, withTitles: [R.string.localizable.registrationModuleTitlePrivatePerson(), R.string.localizable.registrationModuleTitleOrganization()])
            segmentedControl.addTarget(self, action: #selector(segmentControl(_:)), for: .valueChanged)
            segmentedControl.snp.makeConstraints{ (make) -> Void in
                make.top.equalTo(titleLabel.snp.bottom).offset(16)
                make.trailing.equalTo(box).inset(Constants.offset)
                make.leading.equalTo(box).offset(Constants.offset)
                make.height.equalTo(38)
            }
        }
        
        let titleFoto = UILabel()
        last = box.subviews.last ?? box
        box.addSubview(titleFoto)
        titleFoto.text = R.string.localizable.registrationModuleTitlePrivatePersonFoto()

        titleFoto.font = .h4TitleFontSB()
        titleFoto.numberOfLines = 0
        titleFoto.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(last.snp.bottom).offset(16)
            make.trailing.equalTo(box).inset(Constants.offset)
            make.leading.equalTo(box).offset(Constants.offset)
            make.height.equalTo(20)
        }
        
        selectFoto = SelectFotoView()
        box.addSubview(selectFoto)
        selectFoto.delegate = self
        selectFoto.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(titleFoto.snp.bottom).offset(16)
            make.trailing.equalTo(box).inset(Constants.offset)
            make.leading.equalTo(box).offset(Constants.offset)
            make.height.equalTo(128)
        }

        selectFoto.cornerRadius = 64
        selectFoto.setImage(img: presenter?.personAvatar)
        selectFoto.setImageUrl(ImageUrl: userProfile.avatarUrl)
        
        let subTitleFoto = UILabel()
        box.addSubview(subTitleFoto)
        subTitleFoto.text = R.string.localizable.registrationModuleSubtitlePrivatePersonFoto()
        subTitleFoto.font =  .typeFont()
        
        subTitleFoto.numberOfLines = 0
        subTitleFoto.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(selectFoto.snp.bottom).offset(16)
            make.trailing.equalTo(box).inset(Constants.offset)
            make.leading.equalTo(box).offset(Constants.offset)
            make.height.equalTo(40)
        }
        
        // profileType == .person
        textFieldViewName = TextFieldView()
        box.addSubview(textFieldViewName)
        textFieldViewName.configure(id: R.string.localizable.registrationModuleTitlePrivatePersonName(),
                                    title: R.string.localizable.registrationModuleTitlePrivatePersonName(),
                                    placeholder: R.string.localizable.registrationModuleTitlePrivatePersonName(),
                                    maskString: nil)
        textFieldViewName.setPrimaryKey(returnKeyType: .next)
        textFieldViewName.delegate = self
        textFieldViewName.text = userProfile.firstName
        textFieldViewName.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(subTitleFoto.snp.bottom).offset(12)
            make.trailing.equalTo(box).inset(Constants.offset)
            make.leading.equalTo(box).offset(Constants.offset)
            make.height.equalTo(54)
        }
        
        textFieldViewSurName = TextFieldView()
        box.addSubview(textFieldViewSurName)
        textFieldViewSurName.configure(id: R.string.localizable.registrationModuleTitlePrivatePersonSurName(),
                                       title: R.string.localizable.registrationModuleTitlePrivatePersonSurName(),
                                       placeholder: R.string.localizable.registrationModuleTitlePrivatePersonSurName(),
                                       maskString: nil)
        textFieldViewSurName.delegate = self
        textFieldViewSurName.setPrimaryKey(returnKeyType: .done)
        textFieldViewSurName.text = userProfile.lastName
        textFieldViewSurName.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(textFieldViewName.snp.bottom).offset(12)
            make.trailing.equalTo(box).inset(Constants.offset)
            make.leading.equalTo(box).offset(Constants.offset)
            make.height.equalTo(54)
        }
        
        box.addSubview(textFieldBankCardView)
        textFieldBankCardView.snp.makeConstraints {
            $0.top.equalTo(textFieldViewSurName.snp.bottom).offset(12)
            $0.trailing.equalTo(box).inset(Constants.offset)
            $0.leading.equalTo(box).offset(Constants.offset)
            $0.height.equalTo(54)
        }
        textFieldBankCardView.addGestureRecognizer(UITapGestureRecognizer(target: self,
                                                                          action: #selector(tapAddBankCard)))
        
        let mainButton = UIButton()
        last = box.subviews.last ?? box
        mainButton.setStyle(style: .blue)
        box.addSubview(mainButton)
        mainButton.setTitle(R.string.localizable.registrationModuleTitleMainButton(), for: .normal)
        mainButton.snp.makeConstraints { (make) -> Void in

            make.top.equalTo(last.snp.bottom).offset(24)
            make.left.equalTo(box).offset(Constants.offset)
            make.right.equalTo(box).inset(Constants.offset)
            make.height.equalTo(52)
            make.bottom.equalTo(box)
        }
        mainButton.addTarget(self, action: #selector(tapMainButton), for: .touchUpInside)
    }
    
    func configureLayoutOrganization(userProfile: UserProfile) {
        for obj in box.subviews {
            obj.removeFromSuperview()
        }
        
        let newPrifoleMode = presenter?.newProfileMode ?? true
        
        let backButton = UIButton()
        backButton.setImage(R.image.back(), for: .normal)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: -20, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(tapBack), for: .touchUpInside)
        box.addSubview(backButton)
        backButton.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(box)
            make.left.equalTo(box).offset(Constants.offset)
            make.height.width.equalTo(40)
        }
        backButton.isHidden = newPrifoleMode
        
        let titleLabel = UILabel()
        box.addSubview(titleLabel)
        titleLabel.text = newPrifoleMode ? R.string.localizable.registrationModuleTitle() : R.string.localizable.registrationModuleTitleEdit()
        titleLabel.font = .h1TitleFont()
        titleLabel.numberOfLines = 0
        titleLabel.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(backButton.snp.bottom).offset(8)
            make.trailing.equalTo(box).inset(Constants.offset)
            make.leading.equalTo(box).offset(Constants.offset)
            make.height.equalTo(80)
        }
        
        if newPrifoleMode {
            let segmentedControl = BetterSegmentedControl()
            box.addSubview(segmentedControl)
            var defaultIndex = 0
            if userProfile.type == .organization {
                defaultIndex = 1
            }
            segmentedControl.setStyle(style: .blue, defaultIndex: defaultIndex, withTitles: [R.string.localizable.registrationModuleTitlePrivatePerson(), R.string.localizable.registrationModuleTitleOrganization()])
            segmentedControl.addTarget(self, action: #selector(segmentControl(_:)), for: .valueChanged)
            segmentedControl.snp.makeConstraints{ (make) -> Void in
                make.top.equalTo(titleLabel.snp.bottom).offset(16)
                make.trailing.equalTo(box).inset(Constants.offset)
                make.leading.equalTo(box).offset(Constants.offset)
                make.height.equalTo(38)
            }
        }
        
        let titleFoto = UILabel()
        last = box.subviews.last ?? box
        box.addSubview(titleFoto)
        titleFoto.text = R.string.localizable.registrationModuleTitleOrganizationFoto()
        titleFoto.font = .h4TitleFontSB()
        titleFoto.numberOfLines = 0
        titleFoto.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(last.snp.bottom).offset(16)
            make.trailing.equalTo(box).inset(Constants.offset)
            make.leading.equalTo(box).offset(Constants.offset)
            make.height.equalTo(20)
        }
        
        selectFoto = SelectFotoView()
        box.addSubview(selectFoto)
        selectFoto.delegate = self
        selectFoto.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(titleFoto.snp.bottom).offset(16)
            make.trailing.equalTo(box).inset(Constants.offset)
            make.leading.equalTo(box).offset(Constants.offset)
            make.height.equalTo(128)
        }
   
        selectFoto.setImage(img: presenter?.organizationAvatar)
        selectFoto.setImageUrl(ImageUrl: userProfile.avatarUrl)
        
        let subTitleFoto = UILabel()
        box.addSubview(subTitleFoto)
        subTitleFoto.text = R.string.localizable.registrationModuleSubtitleOrganizationFoto()
 
        subTitleFoto.font =  .typeFont()
        subTitleFoto.numberOfLines = 0
        subTitleFoto.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(selectFoto.snp.bottom).offset(16)
            make.trailing.equalTo(box).inset(Constants.offset)
            make.leading.equalTo(box).offset(Constants.offset)
            make.height.equalTo(40)
        }
        
        // profileType == .organization ///
        textFieldViewOrganizationName = TextFieldView()
        box.addSubview(textFieldViewOrganizationName)
        textFieldViewOrganizationName.configure(id: R.string.localizable.registrationModuleTitleOrganizationName(),
                                                title: R.string.localizable.registrationModuleTitleOrganizationName(),
                                                placeholder: R.string.localizable.registrationModuleTitleOrganizationName(),
                                                maskString: nil)
        textFieldViewOrganizationName.delegate = self
        textFieldViewOrganizationName.setPrimaryKey(returnKeyType: .next)
        textFieldViewOrganizationName.text = userProfile.organizationName
        textFieldViewOrganizationName.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(subTitleFoto.snp.bottom).offset(12)
            make.trailing.equalTo(box).inset(Constants.offset)
            make.leading.equalTo(box).offset(Constants.offset)
            make.height.equalTo(54)
        }
        
        textFieldViewOrganizationINN = TextFieldView()
        box.addSubview(textFieldViewOrganizationINN)
        textFieldViewOrganizationINN.configure(id: R.string.localizable.registrationModuleTitleOrganizationINN(),
                                               title: R.string.localizable.registrationModuleTitleOrganizationINN(),
                                               placeholder: R.string.localizable.registrationModuleTitleOrganizationINN(),
                                               maskString: nil)
        textFieldViewOrganizationINN.delegate = self
        textFieldViewOrganizationINN.setPrimaryKey(returnKeyType: .next)
        textFieldViewOrganizationINN.text = userProfile.organizationInnNumber
        textFieldViewOrganizationINN.setKeybord(keyboardType: .numberPad)
        textFieldViewOrganizationINN.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(textFieldViewOrganizationName.snp.bottom).offset(12)
            make.trailing.equalTo(box).inset(Constants.offset)
            make.leading.equalTo(box).offset(Constants.offset)
            make.height.equalTo(54)
        }
        
        textFieldViewOrganizationOGRN = TextFieldView()
        box.addSubview(textFieldViewOrganizationOGRN)
        textFieldViewOrganizationOGRN.configure(id: R.string.localizable.registrationModuleTitleOrganizationOGRN(),
                                                title: R.string.localizable.registrationModuleTitleOrganizationOGRN(),
                                                placeholder: R.string.localizable.registrationModuleTitleOrganizationOGRN(),
                                                maskString: nil)
        textFieldViewOrganizationOGRN.delegate = self
        textFieldViewOrganizationOGRN.setPrimaryKey(returnKeyType: .next)
        textFieldViewOrganizationOGRN.text = userProfile.organizationOgrnNumber
        textFieldViewOrganizationOGRN.setKeybord(keyboardType: .numberPad)
        textFieldViewOrganizationOGRN.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(textFieldViewOrganizationINN.snp.bottom).offset(12)
            make.trailing.equalTo(box).inset(Constants.offset)
            make.leading.equalTo(box).offset(Constants.offset)
            make.height.equalTo(54)
        }
        
        textFieldViewOrganizationRepresentativeName = TextFieldView()
        box.addSubview(textFieldViewOrganizationRepresentativeName)
        textFieldViewOrganizationRepresentativeName.configure(id: R.string.localizable.registrationModuleTitleOrganizationRepresentativeName(),
                                                              title: R.string.localizable.registrationModuleTitleOrganizationRepresentativeName(),
                                                              placeholder: R.string.localizable.registrationModuleTitleOrganizationRepresentativeName(),
                                                              maskString: nil)
        textFieldViewOrganizationRepresentativeName.delegate = self
        textFieldViewOrganizationRepresentativeName.setPrimaryKey(returnKeyType: .next)
        textFieldViewOrganizationRepresentativeName.text = userProfile.organizationRepresentativeName
        textFieldViewOrganizationRepresentativeName.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(textFieldViewOrganizationOGRN.snp.bottom).offset(12)
            make.trailing.equalTo(box).inset(Constants.offset)
            make.leading.equalTo(box).offset(Constants.offset)
            make.height.equalTo(54)
        }
        
        textFieldViewOrganizationRepresentativeSurName = TextFieldView()
        box.addSubview(textFieldViewOrganizationRepresentativeSurName)
        textFieldViewOrganizationRepresentativeSurName.configure(id: R.string.localizable.registrationModuleTitleOrganizationRepresentativeSurName(),
                                                                 title: R.string.localizable.registrationModuleTitleOrganizationRepresentativeSurName(),
                                                                 placeholder: R.string.localizable.registrationModuleTitleOrganizationRepresentativeSurName(),
                                                                 maskString: nil)
        textFieldViewOrganizationRepresentativeSurName.delegate = self
        textFieldViewOrganizationRepresentativeSurName.setPrimaryKey(returnKeyType: .next)
        textFieldViewOrganizationRepresentativeSurName.text = userProfile.organizationRepresentativeSurName
        textFieldViewOrganizationRepresentativeSurName.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(textFieldViewOrganizationRepresentativeName.snp.bottom).offset(12)
            make.trailing.equalTo(box).inset(Constants.offset)
            make.leading.equalTo(box).offset(Constants.offset)
            make.height.equalTo(54)
        }
        
        textFieldViewOrganizationRepresentativePosition = TextFieldView()
        box.addSubview(textFieldViewOrganizationRepresentativePosition)
        textFieldViewOrganizationRepresentativePosition.configure(id: R.string.localizable.registrationModuleTitleOrganizationRepresentativePosition(),
                                                                  title: R.string.localizable.registrationModuleTitleOrganizationRepresentativePosition(),
                                                                  placeholder: R.string.localizable.registrationModuleTitleOrganizationRepresentativePosition(),
                                                                  maskString: nil)
        textFieldViewOrganizationRepresentativePosition.delegate = self
        textFieldViewOrganizationRepresentativePosition.setPrimaryKey(returnKeyType: .done)
        textFieldViewOrganizationRepresentativePosition.text = userProfile.organizationRepresentativePosition
        textFieldViewOrganizationRepresentativePosition.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(textFieldViewOrganizationRepresentativeSurName.snp.bottom).offset(12)
            make.trailing.equalTo(box).inset(Constants.offset)
            make.leading.equalTo(box).offset(Constants.offset)
            make.height.equalTo(54)
        }
        
        let mainButton = UIButton()
        last = box.subviews.last ?? box
        mainButton.setStyle(style: .blue)
        box.addSubview(mainButton)
        mainButton.setTitle(R.string.localizable.registrationModuleTitleMainButton(), for: .normal)
        mainButton.snp.makeConstraints { (make) -> Void in

            make.top.equalTo(last.snp.bottom).offset(24)
            make.left.equalTo(box).offset(Constants.offset)
            make.right.equalTo(box).inset(Constants.offset)
            make.height.equalTo(52)
            make.bottom.equalTo(box)
        }
        mainButton.addTarget(self, action: #selector(tapMainButton), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    // MARK: Methods
    func checkPersionDateValidation() -> Bool {
        var result = true
        textFieldViewName.checkValidation(minChars: 1, result: &result)
        textFieldViewSurName.checkValidation(minChars: 1, result: &result)
        
        return result
    }
    
    func checkOrganizationDateValidation() -> Bool {
        var result = true
        textFieldViewOrganizationName.checkValidation(minChars: 1, result: &result)
        textFieldViewOrganizationINN.checkValidation([10,12], result: &result)
        textFieldViewOrganizationOGRN.checkValidation([13,15], result: &result)
        textFieldViewOrganizationRepresentativeName.checkValidation(minChars: 1, result: &result)
        textFieldViewOrganizationRepresentativeSurName.checkValidation(minChars: 1, result: &result)
        textFieldViewOrganizationRepresentativePosition.checkValidation(minChars: 1, result: &result)
        
        return result
    }
    
    // MARK: IBActions
    @objc func tapBack(_ sender: UIButton) {
        print(#function)
        navigationController?.popViewController(animated: false)
    }
    @objc func segmentControl(_ segmentedControl: BetterSegmentedControl) {
        presenter?.tapSegmentControl(segmentedIndex: segmentedControl.index)
        print("segmentedControl.index>",segmentedControl.index)
    }
    
    @objc func tapMainButton(_ sender: UIButton) {
        print(#function)
        presenter?.personAvatar = selectFoto.imageView.image
        presenter?.organizationAvatar = selectFoto.imageView.image
        presenter?.tapSave()
    }
    @objc func tapAddBankCard(_ sender: UIButton) {
        print(#function)
        presenter?.tapAddBankCard()
    }
}

extension ProfileEditViewController: ProfileEditViewProtocol {
    
    func showMessage(title: String, message: String, buttonTitle: String) {
        if selectFoto.selectedImage == nil {
            selectFoto.imageView.image = R.image.addPhotoError()
        }
        
        Popup().showAlertView(title: title, message: message, buttonTitle: buttonTitle)
    }
    func configureViews(userProfile: UserProfile) {
        configureLayoutBox()
        if userProfile.type == .organization {
            configureLayoutOrganization(userProfile: userProfile)
        }
        if userProfile.type == .individual {
            configureLayoutPerson(userProfile: userProfile)
        }
    }
    func updateUserProfileModel(userProfile: UserProfile) -> UserProfile {
        userProfile.firstName = textFieldViewName.text
        userProfile.lastName = textFieldViewSurName.text
        userProfile.organizationName = textFieldViewOrganizationName.text
        userProfile.organizationInnNumber = textFieldViewOrganizationINN.text
        userProfile.organizationOgrnNumber = textFieldViewOrganizationOGRN.text
        userProfile.organizationRepresentativeName = textFieldViewOrganizationRepresentativeName.text
        userProfile.organizationRepresentativeSurName = textFieldViewOrganizationRepresentativeSurName.text
        userProfile.organizationRepresentativePosition = textFieldViewOrganizationRepresentativePosition.text
        return userProfile
    }
}

extension ProfileEditViewController: SelectFotoViewDelegate {
    func dismiss() {
        print(#function)
        self.dismiss(animated: true, completion: nil)
    }
    
    func showCamera(imagePicker: UIImagePickerController) {
        print(#function)
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func showAlert(alert: UIAlertController) {
        print(#function)
        self.present(alert, animated: true, completion: nil)
    }
    func selectImage(image: UIImage?) {
        presenter?.selectImage(image: image)
    }
}

extension ProfileEditViewController : TextFieldViewDelegate {
    func tapLeftButtonAction(Action: Action) {
        
    }
    
    func tapRightButtonAction(Action: Action) {
        
    }
    
    func tapPrimaryAction(id: String, text: String) {
        switch id {
        case R.string.localizable.registrationModuleTitlePrivatePersonName():
            textFieldViewSurName.showKeybord()
        case R.string.localizable.registrationModuleTitleOrganizationName():
            textFieldViewOrganizationINN.showKeybord()
        case R.string.localizable.registrationModuleTitleOrganizationINN():
            textFieldViewOrganizationOGRN.showKeybord()
        case R.string.localizable.registrationModuleTitleOrganizationOGRN():
            textFieldViewOrganizationRepresentativeName.showKeybord()
        case R.string.localizable.registrationModuleTitleOrganizationRepresentativeName():
            textFieldViewOrganizationRepresentativeSurName.showKeybord()
        case R.string.localizable.registrationModuleTitleOrganizationRepresentativeSurName():
            textFieldViewOrganizationRepresentativePosition.showKeybord()
        default:
            break
        }
    }
}
