//
//  TravelModulePresenter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 14/02/2021.
//  Copyright © 2021 SilkWay. All rights reserved.
//

import Foundation
import CoreLocation
import Firebase
import Alamofire
import GoogleMaps
import SwiftyJSON

protocol TravelModuleViewProtocol: AnyObject {
    func updataTable()
    func updateMaps(markers: [MapMarkerModel])
    func drowLine(linePoints: [GMSPolyline])
}

protocol TravelModulePresenterProtocol: AnyObject {
    func close()
    var tripModel: TripModel? { get }
}

final class TravelModulePresenter: TravelModulePresenterProtocol {
    
    // MARK: Constants
    
    // MARK: Properties
    private let orderService = OrderService()
    weak var view: TravelModuleViewProtocol?
    let router: TravelModuleRouter
    
    let travelModelId: String
    private var ordersMarkers = [MapMarkerModel]()
    private var trackMarkers = [MapMarkerModel]()
    var tripModel: TripModel?
    
    init(with view: TravelModuleViewProtocol, router: TravelModuleRouter, travelModelId: String) {
        self.view = view
        self.router = router
        self.travelModelId = travelModelId
        trackMarkers.removeAll()
        
        //MOK
        orderService.getTrip(tripsID: travelModelId) { [weak self] (object, soccess, error) in
            self?.tripModel = object
            
            var num = 0
            object?.waypoints?.forEach {
                guard let location = $0.locationPointModel?.location else { return }
                let marker = MapMarkerModel(
                    location:location,
                    title: $0.locationPointModel?.address ?? "",
                    snippet: "",
                    icon: R.image.trackMarker()?.addText(String.toChar(num: num)),
                    type: .trackPoint)
                num += 1
                self?.trackMarkers.append(marker)
            }
        }
        
        orderService.getOrders(tripsID: travelModelId) { [weak self] (list, soccess, error) in
            if soccess {
                var orders = [Order]()
                self?.ordersMarkers.removeAll()
                
                list.forEach {
                    var addressFrom: AddressPoint?
                    var addressTo: AddressPoint?
                    var cost: Money?
                    var compensation: Money?
                    
                    if let addressModel = $0.addressFrom {
                        addressFrom = AddressPoint(model: addressModel)
                    }
                    
                    if let addressModel = $0.addressTo {
                        addressTo = AddressPoint(model: addressModel)
                    }
                    if let costModel = $0.cost {
                        cost = Money(model: costModel)
                    }
                    if let costModel = $0.compensation {
                        compensation = Money(model: costModel)
                    }
                    
                    var deliveryDateTime: Date?
                    if let dataModel = $0.dataModel {
                        let timestamp = Timestamp(
                            seconds: dataModel.seconds,
                            nanoseconds: dataModel.nanoseconds)
                        deliveryDateTime = timestamp.dateValue()
                    }
                    
                    let product = Product(
                        name: $0.productName ?? "",
                        imageUrl: $0.fileUrl,
                        width: $0.weight,
                        length: 0,
                        depth: 0,
                        weight: 0)
                    
                    let order = Order(
                        id: $0.id,
                        addressFrom: addressFrom,
                        addressTo: addressTo,
                        deliveryDateTime: deliveryDateTime,
                        comment: $0.comment,
                        product: product,
                        compensation: compensation,
                        cost: cost,
                        isSafeDelivery: true,
                        owner: $0.owner ?? "")
                    
                    orders.append(order)
                    
                    let markerModel = MapMarkerModel(
                        location: $0.addressFrom?.location ?? CLLocationCoordinate2D(latitude: 0, longitude: 0),
                        title: $0.addressFrom?.address ?? "",
                        snippet: "snippet",
                        icon: R.image.trackMarker(),
                        type: .orderPoint)
                    self?.ordersMarkers.append(markerModel)
                    
                }
                // self?.orders = list
                self?.tripModel?.orders = orders
                self?.view?.updataTable()
                self?.mapUpdate()
            }
        }
    }
    
    // MARK: Methods
    private func mapUpdate() {
        var markers = [MapMarkerModel]()
        trackMarkers.forEach {
            markers.append($0)
        }
        ordersMarkers.forEach {
            markers.append($0)
        }
        view?.updateMaps(markers: markers)
        
        let trackMarkers = markers.filter{ $0.type == .trackPoint }
        if let first = trackMarkers.first, let last = trackMarkers.last, trackMarkers.count > 1 {
            drawPath(currentLocation: first.location, destinationLoc: last.location)
        }
    }
    
    private func drawPath(currentLocation: CLLocationCoordinate2D, destinationLoc: CLLocationCoordinate2D)
     {
         let origin = "\(currentLocation.latitude),\(currentLocation.longitude)"
         let destination = "\(destinationLoc.latitude),\(destinationLoc.longitude)"
         
     let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=AIzaSyDgiF3Il3RAEbPC0tYuufMlp2rdkJLJ6Oc"
         
         print("url>" + url)
         
         AF.request(url).responseJSON { [weak self] response in
             
             switch response.result {
             case .success(let value):
                 let json = JSON(value)
//                 print("drawPath>" , response.request)  // original URL request
//                 print("drawPath>" , response.response) // HTTP URL response
//                 print("drawPath>" , response.data)     // server data
//                 print("drawPath>" , response.result)   // result of response serialization
                 let routes = json["routes"].arrayValue

                 var list = [GMSPolyline]()
                 for route in routes {
                   let routeOverviewPolyline = route["overview_polyline"].dictionary
                   let points = routeOverviewPolyline?["points"]?.stringValue
                   let path = GMSPath.init(fromEncodedPath: points!)
                   let polyline = GMSPolyline.init(path: path)
                    list.append(polyline)
                 }
                self?.view?.drowLine(linePoints: list)
                
             case .failure(let error):
                 print("getOrders>error>",error)
                 return
             }
             
             

         }
       }
    
    func close() {
        router.popOver()
    }
}
