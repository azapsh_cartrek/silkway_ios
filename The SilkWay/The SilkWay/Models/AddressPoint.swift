//
//  Address.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 21.02.2021.
//  Copyright © 2021 Ахмед Фокичев. All rights reserved.
//

import Foundation
import CoreLocation
import Firebase

class AddressPoint: NSObject, NSCoding {
    var country: String?
    var state: String?
    var city: String?
    var address1: String
    var address2: String
    var isCache = false
    
    var location: CLLocationCoordinate2D?
    
    required init(coder decoder: NSCoder) {
        country = decoder.decodeObject(forKey: "country") as? String
        city = decoder.decodeObject(forKey: "city") as? String
        state = decoder.decodeObject(forKey: "state") as? String
        address1 = decoder.decodeObject(forKey: "address1") as? String ?? ""
        address2 = decoder.decodeObject(forKey: "address2") as? String ?? ""
        let lat = decoder.decodeDouble(forKey: "lat")
        let long = decoder.decodeDouble(forKey: "long")
        location = CLLocationCoordinate2D(latitude: lat, longitude: long)
    }

    init(country: String? = nil, state: String? = nil, city: String?, address1: String,  address2: String, location: CLLocationCoordinate2D) {
        self.country = country
        self.state = state
        self.city = city
        self.address1 = address1
        self.address2 = address2
        self.location = location
        self.country = country
    }
    
    init(model: LocationPointModel) {
        self.address1 = model.address ?? ""
        self.address2 = ""
        self.location = CLLocationCoordinate2D(latitude: model.point?.latitude ?? 0, longitude: model.point?.longitude ?? 0)
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(country, forKey: "country")
        coder.encode(city, forKey: "city")
        coder.encode(state, forKey: "state")
        coder.encode(address1, forKey: "address1")
        coder.encode(address2, forKey: "address2")
        let lat = location?.latitude ?? 0
        let long = location?.longitude ?? 0
        coder.encode(lat, forKey: "lat")
        coder.encode(long, forKey: "long")
    }
    
    var fullAddress: String {
        var address = ""

        addData(&address, country)
        addData(&address, state)
        addData(&address, city)
        addData(&address, address1)
        addData(&address, address2)
        
        return address
    }
    
    var stateAddress: String {
        var address = ""
        addData(&address, city)
        addData(&address, state)
        addData(&address, country)
        
        return address
    }
    
    var streetAddress: String {
        var address = ""
        addData(&address, city)
        addData(&address, address1)
        addData(&address, address2)
        
        return address
    }
    
    private func addData(_ result: inout String, _ data: String?) {
        if let _data = data,  !_data.isEmpty {
            if result.isEmpty {
                result = _data
            } else {
                result += ", " + _data
            }
        }
    }
    
    var toJSON: [String : Any] {
        let latitude = location?.latitude ?? 0
        let longitude = location?.longitude ?? 0
        return [
            "address": fullAddress,
            "point": GeoPoint(latitude: latitude, longitude: longitude)
        ]
    }
}
