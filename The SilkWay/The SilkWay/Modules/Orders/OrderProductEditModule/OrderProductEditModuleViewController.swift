//
//  OrderProductEditModuleViewController.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 19/09/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import UIKit

final class OrderProductEditModuleViewController: UIViewController {
    
    // MARK: Constants
    private enum Constants {
        static let offset: CGFloat = 20
        static let topItemsOffset: CGFloat = 16
        static let fotoImageSize: CGFloat = 108
        static let heightTextView: CGFloat = 48
    }
    
    // MARK: IBOutlets
    
    // MARK: Properties
    var scrollView = UIScrollView()
    var box = UIView()
    
    // MARK: - Subviews
    private let backButton: UIButton = {
        let button = UIButton()
        button.setImage(R.image.back(), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -20, bottom: 0, right: 0)
        button.contentMode = .scaleAspectFit
        
        return button
    }()
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = R.string.localizable.orderProductEditTitle()
        label.font = .h1TitleFont()
        label.textAlignment = .left
        return label
    }()
    private let titleImageLabel: UILabel = {
        let label = UILabel()
        label.text = R.string.localizable.orderProductEditFotoTitle()
        label.font = .h4TitleFontSB()
        label.textAlignment = .left
        return label
    }()
    private let fotoImageView: SelectFotoView = {
        let imageView = SelectFotoView()
        imageView.setPlaceHolderImages(img: R.image.addProductPhoto(), mini: nil)
        imageView.cornerRadius = 10
        
        return imageView
    }()
    private let nameTextView: TextFieldView = {
        let textView = TextFieldView()
        textView.configure(id: "",
                           title: R.string.localizable.orderProductEditName(),
                           placeholder: R.string.localizable.orderProductEditName())
        
        return textView
    }()
    private let infoLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.text = R.string.localizable.orderProductEditInfo()
        label.numberOfLines = 0
        label.font = .subtitleFont()
        label.textColor = R.color.infoLabelTextColor()
        
        return label
    }()
    private let lengthTextView: TextFieldView = {
        let textView = TextFieldView()
        textView.configure(id: "",
                           title: R.string.localizable.orderProductEditLength(),
                           placeholder: R.string.localizable.orderProductEditLength())
        textView.setKeybord(keyboardType: .numberPad)
        
        return textView
    }()
    private let widthTextView: TextFieldView = {
        let textView = TextFieldView()
        textView.configure(id: "",
                           title: R.string.localizable.orderProductEditWidth(),
                           placeholder: R.string.localizable.orderProductEditWidth())
        textView.setKeybord(keyboardType: .numberPad)
        
        return textView
    }()
    private let depthTextView: TextFieldView = {
        let textView = TextFieldView()
        textView.configure(id: "",
                           title: R.string.localizable.orderProductEditDepth(),
                           placeholder: R.string.localizable.orderProductEditDepth())
        textView.setKeybord(keyboardType: .numberPad)
        
        return textView
    }()
    private let weightTextView: TextFieldView = {
        let textView = TextFieldView()
        textView.configure(id: "",
                           title: R.string.localizable.orderProductEditWeight(),
                           placeholder: R.string.localizable.orderProductEditWeight())
        textView.setKeybord(keyboardType: .numberPad)
        
        return textView
    }()
    private let saveButton: UIButton = {
        let button = UIButton()
        button.setTitle(R.string.localizable.save(), for: .normal)
        
        return button
    }()
    
    var presenter: OrderProductEditPresenterProtocol?
    
    // MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func loadView() {
        super.loadView()
        
        configureLayoutBox()
        box.addSubview(backButton)
        box.addSubview(titleLabel)
        box.addSubview(titleImageLabel)
        box.addSubview(fotoImageView)
        box.addSubview(nameTextView)
        box.addSubview(infoLabel)
        box.addSubview(lengthTextView)
        box.addSubview(widthTextView)
        box.addSubview(depthTextView)
        box.addSubview(weightTextView)
        box.addSubview(saveButton)
        
        backButton.addTarget(self, action: #selector(tapBack), for: .touchUpInside)
        backButton.snp.makeConstraints {
            $0.top.equalTo(box).offset(8)
            $0.left.equalTo(box).offset(Constants.offset)
            $0.height.width.equalTo(40)
        }
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(backButton.snp.bottom).offset(Constants.topItemsOffset)
            $0.trailing.equalToSuperview().inset(Constants.offset)
            $0.leading.equalToSuperview().offset(Constants.offset)
        }
        titleImageLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(Constants.topItemsOffset)
            $0.trailing.equalToSuperview().inset(Constants.offset)
            $0.leading.equalToSuperview().offset(Constants.offset)
        }
        fotoImageView.snp.makeConstraints {
            $0.top.equalTo(titleImageLabel.snp.bottom).offset(Constants.topItemsOffset)
            $0.width.height.equalTo(Constants.fotoImageSize)
            $0.leading.equalToSuperview().offset(Constants.offset)
        }
        fotoImageView.delegate = self

        nameTextView.snp.makeConstraints {
            $0.top.equalTo(fotoImageView.snp.bottom).offset(Constants.topItemsOffset + 8)
            $0.trailing.equalToSuperview().inset(Constants.offset)
            $0.leading.equalToSuperview().offset(Constants.offset)
            $0.height.equalTo(Constants.heightTextView)
        }
        infoLabel.snp.makeConstraints {
            $0.top.equalTo(nameTextView.snp.bottom).offset(Constants.topItemsOffset)
            $0.trailing.equalToSuperview().inset(Constants.offset)
            $0.leading.equalToSuperview().offset(Constants.offset)
        }
        widthTextView.snp.makeConstraints {
            $0.top.equalTo(infoLabel.snp.bottom).offset(Constants.topItemsOffset)
            $0.trailing.equalToSuperview().inset(Constants.offset)
            $0.leading.equalToSuperview().offset(Constants.offset)
            $0.height.equalTo(Constants.heightTextView)
        }
        lengthTextView.snp.makeConstraints {
            $0.top.equalTo(widthTextView.snp.bottom).offset(Constants.topItemsOffset)
            $0.trailing.equalToSuperview().inset(Constants.offset)
            $0.leading.equalToSuperview().offset(Constants.offset)
            $0.height.equalTo(Constants.heightTextView)
        }
        depthTextView.snp.makeConstraints {
            $0.top.equalTo(lengthTextView.snp.bottom).offset(Constants.topItemsOffset)
            $0.trailing.equalToSuperview().inset(Constants.offset)
            $0.leading.equalToSuperview().offset(Constants.offset)
            $0.height.equalTo(Constants.heightTextView)
        }
        weightTextView.snp.makeConstraints {
            $0.top.equalTo(depthTextView.snp.bottom).offset(Constants.topItemsOffset)
            $0.trailing.equalToSuperview().inset(Constants.offset)
            $0.leading.equalToSuperview().offset(Constants.offset)
            $0.height.equalTo(Constants.heightTextView)
        }
        saveButton.snp.makeConstraints {
            $0.top.equalTo(weightTextView.snp.bottom).offset(32)
            $0.trailing.equalToSuperview().inset(Constants.offset)
            $0.leading.equalToSuperview().offset(Constants.offset)
            $0.height.equalTo(52)
            $0.bottom.equalTo(box.snp.bottom).offset(-32)
        }
        saveButton.setStyle(style: .blue)
        saveButton.addTarget(self, action: #selector(tapSave), for: .touchUpInside)
        
    }
    
    // MARK: ConfigureLayouts
    func configureLayoutBox() {
        view.addSubview(scrollView)
        scrollView.addSubview(box)
        
        box.backgroundColor = .clear
        view.backgroundColor = R.color.backgroundColor()
        
        scrollView.showsVerticalScrollIndicator = false
        scrollView.delegate = self
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        box.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.centerX.top.bottom.equalToSuperview()
        }
    }

    // MARK: Methods
    func checkDateValidation() -> Bool {
        var result = true
        nameTextView.checkValidation(minChars: 1, result: &result)
        lengthTextView.checkValidation(minChars: 1, result: &result)
        widthTextView.checkValidation(minChars: 1, result: &result)
        depthTextView.checkValidation(minChars: 1, result: &result)
        weightTextView.checkValidation(minChars: 1, result: &result)
        fotoImageView.checkValidation(result: &result)
        
        return result
    }
    
    // MARK: IBActions
    @objc func tapBack(_ sender: UIButton) {
        print(#function)
        navigationController?.popViewController(animated: true)
    }
    @objc func tapSave(_ sender: UIButton) {
        print(#function)
        if checkDateValidation() {
            let product = Product(name: nameTextView.text,
                                  image: fotoImageView.selectedImage,
                                  width: Double(widthTextView.text) ?? 0.0,
                                  length: Double(lengthTextView.text) ?? 0.0,
                                  depth: Double(depthTextView.text) ?? 0.0,
                                  weight: Double(weightTextView.text) ?? 0.0 )
            presenter?.tapSaveProduct(product: product)
        }
        
    }
    @objc func tapMakeFoto(_ sender: UIButton) {
        print(#function)

    }
}

extension OrderProductEditModuleViewController: SelectFotoViewDelegate {
    func showCamera(imagePicker: UIImagePickerController) {
        print(#function)
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func showAlert(alert: UIAlertController) {
        print(#function)
        self.present(alert, animated: true, completion: nil)
    }
    
    func dismiss() {
        print(#function)
        self.dismiss(animated: true, completion: nil)
    }
    
    func selectImage(image: UIImage?) {
        print("selectImage>", image?.size.width)
    }
}

extension OrderProductEditModuleViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    }
}

extension OrderProductEditModuleViewController: OrderProductEditModuleViewProtocol {
    func updateView(product: Product?) {
        guard let product = product else { return }

        nameTextView.text = product.name

        if let image = product.image {
            fotoImageView.setImage(img: image)
        }
        if let width = product.width {
            widthTextView.text  = String(width)
        }
        if let weight = product.weight {
            weightTextView.text  = String(weight)
        }
        if let length = product.length {
            lengthTextView.text  = String(length)
        }
        if let depth = product.depth {
            depthTextView.text  = String(depth)
        }
    }
    
    
}
