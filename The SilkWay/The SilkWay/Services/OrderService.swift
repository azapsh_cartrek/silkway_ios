//
//  OrderService.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 21.02.2021.
//  Copyright © 2021 Ахмед Фокичев. All rights reserved.
//

import Firebase
import Alamofire
import SwiftyJSON
import ObjectMapper

final class OrderService {
    
    let netServive = NetService()
    let url = ""
    
    func saveOrder(orderModel: Order, completion: @escaping (_ success: Bool, _ error: String) -> Void) {
        
    }
    
    func getOrders (tripsID: String , completion: @escaping (_ list: [OrderModel], _ success: Bool, _ error: String?) -> Void) {
        
        let accessToken = AuthService.shared.fbToken ?? ""
        let url = "https://us-central1-the-silk-way.cloudfunctions.net/v1_searchOrdersByTrip"
        let data: [String: Any] = [
            "id": tripsID
        ]
        let parameters: [String: Any] = [
            "data": data
        ]
        let headers: HTTPHeaders = [
            "Authorization": "Bearer " + accessToken,
            "Content-Type": "application/json",
        ]
    
        print("data>", data)
        
        AF.request(
            url,
            method: .post,
            parameters: parameters,
            encoding: JSONEncoding.default,
            headers: headers)
            .responseJSON { response in
                
                let statusCode = response.response?.statusCode ?? 0
                print("|" + url + ":", statusCode)
                switch response.result {
                case .success(let value):
                    let json = JSON(value)

                    print(json)
                    if statusCode == 200 {
                        if let models = Mapper<OrderModel>().mapArray(JSONObject: json["result"].object) {
                            completion(models,true, nil)
                        } else {
                            completion([OrderModel](), false, "error mapping")
                        }
                    }
                    
                case .failure(let error):
                    print("getOrders>error>",error)
                    completion([OrderModel](), false, error.errorDescription)
                    return
                }
            }
        
    }
    
    func getTrip (tripsID: String , completion: @escaping (_ object: TripModel?, _ success: Bool, _ error: String?) -> Void) {
        
        let accessToken = AuthService.shared.fbToken ?? ""
        let url = "https://us-central1-the-silk-way.cloudfunctions.net/getTrip"
        let data: [String: Any] = [
            "id": tripsID
        ]
        
        let parameters: [String: Any] = [
            "data": data
        ]
        let headers: HTTPHeaders = [
            "Authorization": "Bearer " + accessToken,
            "Content-Type": "application/json",
        ]
        
        AF.request(
            url,
            method: .post,
            parameters: parameters,
            encoding: JSONEncoding.default,
            headers: headers)
            .responseJSON { response in
                
                let statusCode = response.response?.statusCode ?? 0
                print("|" + url + ":", statusCode)
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    if statusCode == 200 {
                        if let model = Mapper<TripModel>().map(JSONObject: json["result"].object) {
                            completion(model,true, nil)
                        } else {
                            completion(nil, false, "error mapping")
                        }
                    }
                    
                case .failure(let error):
                    print("getOrders>error>",error)
                    completion(nil, false, error.errorDescription)
                    return
                }
            }
        
    }
}
