//
//  MonayModel.swift
//  The SilkWay
//
//  Created by Azapsh on 03.07.2021.
//  Copyright © 2021 Ахмед Фокичев. All rights reserved.
//

import ObjectMapper

struct MoneyModel: Mappable {
    var amount: Int?
    var currency: String?
    
    init?(map: Map) {

    }

    mutating func mapping(map: Map) {
        amount <- map["amount"]
        currency <- map["currency"]
    }
}
