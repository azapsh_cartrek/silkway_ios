//
//  TravelModuleViewController.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 13/02/2021.
//  Copyright © 2021 SilkWay. All rights reserved.
//

import UIKit
import GoogleMaps
import PickerPopupDialog

final class NewTravelModuleViewController: UIViewController {

    // MARK: Constants
    private enum Constants {
        static let offset: CGFloat = 20
    }
    // MARK: IBOutlets
    
    // MARK: - Subviews
    private let backButton: UIButton = {
        let button = UIButton()
        button.setImage(R.image.back(), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -20, bottom: 0, right: 0)
        button.contentMode = .scaleAspectFit
        
        return button
    }()
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = .h1TitleFont()
        label.textColor = R.color.labelTextColor()
        label.text = R.string.localizable.travelModuleTitle()
        label.numberOfLines = 0
        
        return label
    }()
    
    private let fromButtonView: ButtonView = {
        let buttonView = ButtonView()
        buttonView.configure(action: Action.from)
        buttonView.placeholder = R.string.localizable.travelModuleFromPlaceholder()
        buttonView.onRightButtonAction(action: .from, image: R.image.rightArrow())
        
        return buttonView
    }()
    
    private let toButton: ButtonView = {
        let buttonView = ButtonView()
        buttonView.configure(action: Action.to)
        buttonView.placeholder = R.string.localizable.travelModuleToPlaceholder()
        buttonView.onRightButtonAction(action: .to, image: R.image.rightArrow())
        
        return buttonView
    }()
    
    private let errorLabel: UILabel = {
        let label = UILabel()
        label.font = .captionFont()
        label.numberOfLines = 0
        label.textColor = R.color.textFieldViewBorderColorNoValid()
        return label
    }()
    
    private let stackStopOversView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .fill
        stack.distribution = .fill
        stack.spacing = 2
        
        return stack
    }()
    
    private let addStopOverButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.numberOfLines = 0
        button.setImage(R.image.addPhoto(), for: .normal)
        button.layer.cornerRadius = 18
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        
        return button
    }()
    private let addStopOverLabel: UILabel = {
        let label = UILabel()
        label.text = R.string.localizable.travelModuleStopOverButton()
        label.numberOfLines = 0
        return label
    }()
    
    private let mapsTitleLabel: UILabel = {
        let label = UILabel()
        label.font = .h4TitleFontSB()
        label.textColor = R.color.labelTextColor()
        label.text = R.string.localizable.travelModuleTitleMap()
        return label
    }()
   
    private let mapView: GMSMapView = {
        let map = GMSMapView()
        map.isUserInteractionEnabled = true
        return map
    }()
    
    private let deviationRouteButton: ButtonView = {
        let buttonView = ButtonView()
        buttonView.configure(action: Action.deviation)
        buttonView.placeholder = R.string.localizable.travelModuleRouterMapDeviation()
        buttonView.onRightButtonAction(action: .deviation, image: R.image.rightArrow())
        
        return buttonView
    }()
    private let deliveryDateButton: ButtonView = {
        let buttonView = ButtonView()
        buttonView.configure(action: Action.selectDate)
        buttonView.placeholder = R.string.localizable.newOrderModuleDeliveryDatePlaceholder()
        buttonView.onLeftButtonAction(action: .selectDate, image: R.image.calendarIcon())
        
        return buttonView
    }()
    private let deliveryTimeButtonView: ButtonView = {
        let buttonView = ButtonView()
        buttonView.configure(action: Action.selectTime)
        buttonView.placeholder = R.string.localizable.newOrderModuleTravelTimePlaceholder()
        buttonView.onLeftButtonAction(action: .selectTime, image: R.image.timeIcon())
        return buttonView
    }()
    
    private let datePicker: UITextView = {
        let picker = UITextView()
        picker.isHidden = true
        return picker
    }()
    private let deliveryPicker: UITextView = {
        let picker = UITextView()
        picker.isHidden = true
        return picker
    }()
    
    private let titleParametrsLabel: UILabel = {
        let label = UILabel()
        label.font = .h4TitleFontSB()
        label.textColor = R.color.textViewViewColorPlaceholder()
        label.text = R.string.localizable.travelModuleParametersTitle()
        label.numberOfLines = 0
        return label
    }()
    private let titleVolumeLabel: UILabel = {
        let label = UILabel()
        label.font = .h4TitleFontSB()
        label.textColor = R.color.labelTextColor()
        label.text = R.string.localizable.travelModuleVolumeTitle()
        return label
    }()
    private let volumeFromField: TextFieldView = {
        let buttonView = TextFieldView()
        buttonView.configure(id:"",
                             title: R.string.localizable.from(),
                             placeholder: R.string.localizable.from(),
                             maskString: nil)
        buttonView.setKeybord(keyboardType: .numberPad)
        
        return buttonView
    }()
    private let volumeToField: TextFieldView = {
        let buttonView = TextFieldView()
        buttonView.configure(id:"",
                             title: R.string.localizable.to(),
                             placeholder: R.string.localizable.to(),
                             maskString: nil)
        buttonView.setKeybord(keyboardType: .numberPad)
        
        return buttonView
    }()
    ///////////////
    private let weightTitleLabel: UILabel = {
        let label = UILabel()
        label.font = .h4TitleFontSB()
        label.textColor = R.color.labelTextColor()
        label.text = R.string.localizable.travelModuleWeightTitle()
        return label
    }()
    private let weightFromField: TextFieldView = {
        let buttonView = TextFieldView()
        buttonView.configure(id:"",
                             title: R.string.localizable.from(),
                             placeholder: R.string.localizable.from(),
                             maskString: nil)
        buttonView.setKeybord(keyboardType: .numberPad)
        
        return buttonView
    }()
    private let weightToField: TextFieldView = {
        let buttonView = TextFieldView()
        buttonView.configure(id:"",
                             title: R.string.localizable.to(),
                             placeholder: R.string.localizable.to(),
                             maskString: nil)
        buttonView.setKeybord(keyboardType: .numberPad)
        
        return buttonView
    }()
    /////
    private let remunerationTitleLabel: UILabel = {
        let label = UILabel()
        label.font = .h4TitleFontSB()
        label.textColor = R.color.labelTextColor()
        label.text = R.string.localizable.travelModuleRemunerationTitle()
        return label
    }()
    private let remunerationFromField: TextFieldView = {
        let buttonView = TextFieldView()
        buttonView.configure(id:"",
                             title: R.string.localizable.from(),
                             placeholder: R.string.localizable.from(),
                             maskString: nil)
        buttonView.setKeybord(keyboardType: .numberPad)
        
        return buttonView
    }()
    private let remunerationToField: TextFieldView = {
        let buttonView = TextFieldView()
        buttonView.configure(id:"",
                             title: R.string.localizable.to(),
                             placeholder: R.string.localizable.to(),
                             maskString: nil)
        buttonView.setKeybord(keyboardType: .numberPad)
        
        return buttonView
    }()
    /////
    private let commentTextViewView: TextViewView = {
        let textViewView = TextViewView()
        textViewView.configure(title: R.string.localizable.newOrderModuleCommentsPlaceholder(),
                                placeholder: R.string.localizable.newOrderModuleCommentsPlaceholder())
        
        return textViewView
    }()
    private let labelConditionsUse: UILabel = {
        let label = UILabel()
        label.font = .subtitleFont()
        label.textColor = R.color.labelTextColor()
        label.numberOfLines = 0
        label.text = R.string.localizable.newOrderModuleIAmFamiliar()
        return label
    }()
    private let checkboxConditionsUse: CheckBoxButton = {
        let button = CheckBoxButton()
        button.setImage(R.image.checkboxOff(), for: .normal)
        return button
    }()
    private let nextButton: UIButton = {
        let button = UIButton()
        button.setImage(R.image.arrowRightButton(), for: .normal)
        return button
    }()

    // MARK: Properties
    private (set) var presenter: NewTravelModulePresenterProtocol?
    var scrollView = UIScrollView()
    var box = UIView()


    // MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let styleURL = Bundle.main.url(forResource: "map_style", withExtension: "json") {
            self.mapView.mapStyle = try? GMSMapStyle(contentsOfFileURL: styleURL)
        }
    }

    // MARK: Methods
    func setup(presenter: NewTravelModulePresenterProtocol) {
        self.presenter = presenter
        mapView.animate(toLocation: LocationService.shared.location)
        mapView.animate(toZoom: 16)
    }
    
    // MARK: ConfigureLayouts
    func configureLayoutBox() {
        view.addSubview(scrollView)
        scrollView.addSubview(box)
        box.backgroundColor = .clear
        view.backgroundColor = R.color.backgroundColor()
        view.addSubview(datePicker)
        
        scrollView.showsVerticalScrollIndicator = false
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        box.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.centerX.top.bottom.equalToSuperview()
        }
        
        datePicker.snp.makeConstraints {
            $0.bottom.equalToSuperview().offset(-16)
            $0.leading.equalToSuperview()
            $0.trailing.equalToSuperview()
        }
    }
    
    override func loadView() {
        super.loadView()
        
        configureLayoutBox()
        view.backgroundColor = R.color.backgroundColor()
        box.addSubview(backButton)
        box.addSubview(titleLabel)
        box.addSubview(fromButtonView)
        box.addSubview(toButton)
        box.addSubview(errorLabel)
        box.addSubview(stackStopOversView)
        box.addSubview(addStopOverButton)
        box.addSubview(addStopOverLabel)
        box.addSubview(mapsTitleLabel)
        box.addSubview(mapView)
        box.addSubview(deviationRouteButton)
        box.addSubview(deliveryDateButton)
        box.addSubview(deliveryTimeButtonView)
        box.addSubview(titleParametrsLabel)
        box.addSubview(titleVolumeLabel)
        box.addSubview(volumeFromField)
        box.addSubview(volumeToField)
        box.addSubview(weightTitleLabel)
        box.addSubview(weightFromField)
        box.addSubview(weightToField)
        box.addSubview(remunerationTitleLabel)
        box.addSubview(remunerationFromField)
        box.addSubview(remunerationToField)
        box.addSubview(commentTextViewView)
        box.addSubview(labelConditionsUse)
        box.addSubview(checkboxConditionsUse)
        box.addSubview(nextButton)
        
        backButton.addTarget(self, action: #selector(tapBack), for: .touchUpInside)
        backButton.snp.makeConstraints {
            $0.top.equalTo(box).offset(24)
            $0.left.equalTo(box).offset(Constants.offset)
            $0.height.width.equalTo(40)
        }
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(backButton.snp.bottom).offset(16)
            $0.trailing.equalTo(box).inset(Constants.offset)
            $0.leading.equalTo(box).offset(Constants.offset)
            $0.height.equalTo(24)
        }
        
        fromButtonView.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(16)
            $0.trailing.equalTo(box).inset(Constants.offset)
            $0.leading.equalTo(box).offset(Constants.offset)
            $0.height.equalTo(56)
        }
        fromButtonView.delegate = self
        
        toButton.snp.makeConstraints {
            $0.top.equalTo(fromButtonView.snp.bottom).offset(16)
            $0.trailing.equalTo(box).inset(Constants.offset)
            $0.leading.equalTo(box).offset(Constants.offset)
            $0.height.equalTo(56)
        }
        toButton.delegate = self
        errorLabel.snp.makeConstraints {
            $0.top.equalTo(toButton.snp.bottom).offset(2)
            $0.trailing.leading.equalTo(box).inset(Constants.offset)
        }
        
        stackStopOversView.snp.makeConstraints {
            $0.top.equalTo(errorLabel.snp.bottom).offset(8)
            $0.trailing.equalTo(box).inset(Constants.offset)
            $0.leading.equalTo(box).offset(Constants.offset)
        }
        
        addStopOverButton.snp.makeConstraints {
            $0.top.equalTo(stackStopOversView.snp.bottom).offset(8)
            $0.leading.equalTo(box).offset(Constants.offset)
            $0.width.height.equalTo(36)
        }
        addStopOverButton.addTarget(self, action: #selector(tapAddNewStopOver), for: .touchUpInside)
        
        addStopOverLabel.snp.makeConstraints {
            $0.centerY.equalTo(addStopOverButton)
            $0.leading.equalTo(addStopOverButton.snp.trailing).offset(8)
            $0.trailing.equalToSuperview()
        }
        addStopOverLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapAddNewStopOver)))
        addStopOverLabel.isUserInteractionEnabled = true
        
        mapsTitleLabel.snp.makeConstraints {
            $0.top.equalTo(addStopOverButton.snp.bottom).offset(16)
            $0.trailing.equalTo(box).inset(Constants.offset)
            $0.leading.equalTo(box).offset(Constants.offset)
            $0.height.equalTo(48)
        }
        mapView.snp.makeConstraints {
            $0.top.equalTo(mapsTitleLabel.snp.bottom).offset(8)
            $0.trailing.equalTo(box)
            $0.leading.equalTo(box)
            $0.height.equalTo(300)
        }
        deviationRouteButton.snp.makeConstraints {
            $0.top.equalTo(mapView.snp.bottom).offset(16)
            $0.trailing.equalTo(box).inset(Constants.offset)
            $0.leading.equalTo(box).offset(Constants.offset)
            $0.height.equalTo(56)
        }
        deviationRouteButton.delegate = self
        
        deliveryDateButton.snp.makeConstraints {
            $0.top.equalTo(deviationRouteButton.snp.bottom).offset(16)
            $0.trailing.equalTo(box).inset(Constants.offset)
            $0.leading.equalTo(box).offset(Constants.offset)
            $0.height.equalTo(56)
        }
        deliveryTimeButtonView.snp.makeConstraints {
            $0.top.equalTo(deliveryDateButton.snp.bottom).offset(16)
            $0.trailing.equalTo(box).inset(Constants.offset)
            $0.leading.equalTo(box).offset(Constants.offset)
            $0.height.equalTo(56)
        }
        deliveryDateButton.delegate = self
        deliveryTimeButtonView.delegate = self
        
        titleParametrsLabel.snp.makeConstraints {
            $0.top.equalTo(deliveryTimeButtonView.snp.bottom).offset(16)
            $0.trailing.equalTo(box).inset(Constants.offset)
            $0.leading.equalTo(box).offset(Constants.offset)
        }
        
        titleVolumeLabel.snp.makeConstraints {
            $0.top.equalTo(titleParametrsLabel.snp.bottom).offset(16)
            $0.trailing.equalTo(box).inset(Constants.offset)
            $0.leading.equalTo(box).offset(Constants.offset)
            $0.height.equalTo(36)
        }
        volumeFromField.snp.makeConstraints {
            $0.top.equalTo(titleVolumeLabel.snp.bottom).offset(8)
            $0.width.equalToSuperview().multipliedBy(0.42)
            $0.leading.equalTo(box).offset(Constants.offset)
            $0.height.equalTo(56)
        }
        volumeToField.snp.makeConstraints {
            $0.centerY.equalTo(volumeFromField)
            $0.trailing.equalTo(box).inset(Constants.offset)
            $0.width.equalToSuperview().multipliedBy(0.42)
            $0.height.equalTo(56)
        }
        
        weightTitleLabel.snp.makeConstraints {
            $0.top.equalTo(volumeFromField.snp.bottom).offset(16)
            $0.trailing.equalTo(box).inset(Constants.offset)
            $0.leading.equalTo(box).offset(Constants.offset)
            $0.height.equalTo(36)
        }
        weightFromField.snp.makeConstraints {
            $0.top.equalTo(weightTitleLabel.snp.bottom).offset(8)
            $0.width.equalToSuperview().multipliedBy(0.42)
            $0.leading.equalTo(box).offset(Constants.offset)
            $0.height.equalTo(56)
        }
        weightToField.snp.makeConstraints {
            $0.centerY.equalTo(weightFromField)
            $0.trailing.equalTo(box).inset(Constants.offset)
            $0.width.equalToSuperview().multipliedBy(0.42)
            $0.height.equalTo(56)
        }
        
        remunerationTitleLabel.snp.makeConstraints {
            $0.top.equalTo(weightFromField.snp.bottom).offset(16)
            $0.trailing.equalTo(box).inset(Constants.offset)
            $0.leading.equalTo(box).offset(Constants.offset)
            $0.height.equalTo(36)
        }
        remunerationFromField.snp.makeConstraints {
            $0.top.equalTo(remunerationTitleLabel.snp.bottom).offset(8)
            $0.width.equalToSuperview().multipliedBy(0.42)
            $0.leading.equalTo(box).offset(Constants.offset)
            $0.height.equalTo(56)
        }
        remunerationToField.snp.makeConstraints {
            $0.centerY.equalTo(remunerationFromField)
            $0.trailing.equalTo(box).inset(Constants.offset)
            $0.width.equalToSuperview().multipliedBy(0.42)
            $0.height.equalTo(56)
        }
        commentTextViewView.snp.makeConstraints {
            $0.top.equalTo(remunerationFromField.snp.bottom).offset(16)
            $0.trailing.equalTo(box).inset(Constants.offset)
            $0.leading.equalTo(box).offset(Constants.offset)
            $0.height.equalTo(146)
        }
        
        checkboxConditionsUse.snp.makeConstraints {
            $0.top.equalTo(commentTextViewView.snp.bottom).offset(16)
            $0.width.height.equalTo(24)
            $0.leading.equalToSuperview().offset(Constants.offset)
        }
        
        labelConditionsUse.snp.makeConstraints {
            $0.top.equalTo(commentTextViewView.snp.bottom).offset(16)
            $0.leading.equalTo(checkboxConditionsUse.snp.trailing).offset(8)
            $0.trailing.equalTo(box).offset(-Constants.offset)
        }
        let conditionsUseText = R.string.localizable.newOrderModuleIAmFamiliar()
        let attributeString2: NSMutableAttributedString =  NSMutableAttributedString(string: conditionsUseText)
        attributeString2.addAttribute(.underlineStyle, value: NSUnderlineStyle.patternDot.rawValue|NSUnderlineStyle.single.rawValue, range: NSMakeRange(13, 34))
        labelConditionsUse.attributedText = attributeString2
        labelConditionsUse.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapConditionsUseText(_:))))
        labelConditionsUse.isUserInteractionEnabled = true
        
        nextButton.snp.makeConstraints {
            $0.top.equalTo(labelConditionsUse.snp.bottom).offset(16)
            $0.trailing.equalTo(box).offset(-Constants.offset)
            $0.width.height.equalTo(52)
        }
        nextButton.addTarget(self, action: #selector(tapNext), for: .touchUpInside)
        
        let last = box.subviews.last ?? box
        last.snp.makeConstraints {
            $0.bottom.equalTo(box).offset(-32)
        }
    }
    
    func checkDateValidation() -> Bool {
        var result = true
    
        fromButtonView.checkValidation(minChars: 1, result: &result)
        toButton.checkValidation(minChars: 1, result: &result)
        
        errorLabel.text = nil
        if fromButtonView.title == toButton.title {
            toButton.setNotValidBorder()
            errorLabel.text = R.string.localizable.travelModuleToError()
        }
        deliveryDateButton.checkValidation(minChars: 1, result: &result)
        deliveryTimeButtonView.checkValidation(minChars: 1, result: &result)
        checkboxConditionsUse.checkValidation(needChecked: true, result: &result)
        remunerationFromField.checkValidation(minChars: 1, result: &result)
        remunerationToField.checkValidation(minChars: 1, result: &result)
        weightFromField.checkValidation(minChars: 1, result: &result)
        weightToField.checkValidation(minChars: 1, result: &result)
        
        return result
    }
    
    
    // MARK: IBActions
    @objc func tapBack(_ sender: UIButton) {
        presenter?.close()
    }
    @objc func tapNext(_ sender: UIButton) {
        if checkDateValidation() {
            presenter?.travelModel.weightFrom = Int(weightFromField.text) ?? 0
            presenter?.travelModel.weightTo = Int(weightToField.text) ?? 0
        
            presenter?.travelModel.compensationForm = Int(remunerationFromField.text) ?? 0
            presenter?.travelModel.compensationTo = Int(remunerationToField.text) ?? 0
            
            presenter?.travelModel.volumeTo = Int(volumeToField.text) ?? 0
            presenter?.travelModel.volumeFrom = Int(volumeFromField.text) ?? 0
            
            presenter?.tapNext()
        } 
    }
    @objc func tapConditionsUseText(_ sender: UIButton) {
        presenter?.tapLabelConditionsUse()
    }
    
    @objc func tapAddNewStopOver() {
        presenter?.tapAddNewStopOver()
    }
    
    func tapSelectDateTime() {
        print(#function)
        let picker: UIDatePicker = UIDatePicker()
        picker.datePickerMode = UIDatePicker.Mode.dateAndTime
        
        if let deliveryDateTime = presenter?.travelModel.addressFrom?.deliveryDateTime {
            picker.date = deliveryDateTime
        } else {
            let calendar = Calendar.current
            var dateComponents: DateComponents? = calendar.dateComponents([.year, .month, .day, .hour, .month], from: Date())
            dateComponents!.month = dateComponents!.month!
            dateComponents?.hour = 10
            dateComponents?.minute = 0
            picker.date = calendar.date(from: dateComponents!)!
        }
        if #available(iOS 13.4, *) {
            picker.preferredDatePickerStyle = .wheels
        }

        datePicker.inputView = picker
        picker.addTarget(self, action: #selector(datePickerValueChanged), for: UIControl.Event.valueChanged)
        datePicker.becomeFirstResponder()
    }
    
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        presenter?.changeDeliveryDateTime(date: sender.date)
    }
    
    func showPicker() {
        let pickerView = PickerPopupDialog()
//        let myDataSource : [(Int, String)] = [
//            (0, "до 500 м"),
//            (1, "до 1 км"),
//            (5, "до 5 км"),
//            (10, "до 10 км"),
//            (30, "до 30 км"),
//            (50, "до 50 км"),
//            (70, "до 70 км"),
//            (100, "до 100 км"),
//        ]
//        pickerView.setDataSource(myDataSource)
//        pickerView.showDialog("Отклонение от маршрута",
//                              doneButtonTitle: "Ok",
//                              cancelButtonTitle: "cancel",
//                              titleTextColor: R.color.buttonViewTitleColor(),
//                              backColor: R.color.backgroundColor()
//        ) { [weak self] (result) -> Void in
//
//            if let value = result.0 as? Int {
//                self?.presenter?.setDeviation(value: value, text: result.1)
//            }
//            //close window
//            self?.dismiss(animated: true, completion: nil)
//        }
    }
}

extension NewTravelModuleViewController: NewTravelModuleViewProtocol {
    func updateView(travelModel: TravelModel) {
        
        if let addressFrom = travelModel.addressFrom?.addrssPoint {
            fromButtonView.title = addressFrom.streetAddress
        }
        if let addressTo = travelModel.addressTo?.addrssPoint {
            toButton.title = addressTo.streetAddress
        }
        
        if let deliveryDateTime = travelModel.addressFrom?.deliveryDateTime {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.medium
            dateFormatter.timeStyle = DateFormatter.Style.none
            deliveryDateButton.title = dateFormatter.string(from: deliveryDateTime)
            dateFormatter.dateStyle = DateFormatter.Style.none
            dateFormatter.timeStyle = DateFormatter.Style.short
            deliveryTimeButtonView.title = dateFormatter.string(from: deliveryDateTime)
        }
        
        if let deviationText = travelModel.deviationText {
            deviationRouteButton.title = deviationText
        }
    }
    
    func addStopOverView(stopoverModel: WaypointModel
) {
        let stopOverItemView = StopOverItemView()
        stopOverItemView.height(constant: 72)
        stopOverItemView.setup(stopoverModel: stopoverModel)
        stackStopOversView.addArrangedSubview(stopOverItemView)
    }
}

// MARK : ButtonViewDelegate
extension NewTravelModuleViewController: ButtonViewDelegate {
    
    func tapAction(action: Action) {
        
        switch action {
        case .from:
            presenter?.tapAddressSelect(direction: .from)

        case .to:
            presenter?.tapAddressSelect(direction: .to)
            
        case .stopOver:
            presenter?.tapAddressSelect(direction: .stopOver)
        
        case .deviation:
            showPicker()
            
        case .selectDate:
            tapSelectDateTime()
            
        case .selectTime:
            tapSelectDateTime()
        
        default: break
        }
        
    }
    
    func tapRightAction(action: Action) {
        
        switch action {
        case .product:
            break

        case .from:
            presenter?.tapAddressSelect(direction: .from)

        case .to:
            presenter?.tapAddressSelect(direction: .to)

        case .stopOver:
            presenter?.tapAddressSelect(direction: .stopOver)
            
        case .deviation:
            showPicker()
            
        case .selectDate:
            tapSelectDateTime()
            
        case .selectTime:
            tapSelectDateTime()
            
        default: break
        }
    }
}
