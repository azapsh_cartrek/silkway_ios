//
//  BankCardModulePresenter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 11/06/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation

protocol BankCardModuleCallBackProtocol {
    func closeView(success: Bool)
}

protocol BankCardModuleViewProtocol: class {

}

protocol BankCardModuleViewPresenter: class {
    func tapSave()
}

final class BankCardModulePresenter: BankCardModuleViewPresenter {

    // MARK: Constants

    // MARK: Properties

    weak var view: BankCardModuleViewProtocol?
    var router: BankCardModuleRouter!
    var callbackDelegate: BankCardModuleCallBackProtocol?
    
    init(with view: BankCardModuleViewProtocol, router: BankCardModuleRouter) {
        self.view = view
        self.router = router
    }
    
    // MARK: Methods
    func tapSave() {
        router?.close()
        
        callbackDelegate?.closeView(success: true)
    }
}
