//
//  AlertView.swift
//  The SilkWay
//
//  Created by Kirill Ryabinin on 04.11.2019.
//  Copyright © 2019 com.thesilkway. All rights reserved.
//

import UIKit
import SwiftMessages

class AlertView: MessageView {
    
    // MARK: - Builder
    
    class func instance() -> AlertView? {
        return Bundle.main.loadNibNamed("AlertView", owner: nil)?.first as? AlertView
    }
    
    // MARK: - Properties
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var blueButton: UIButton!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var iconTop: UIImageView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentView.layer.cornerRadius = 12
        contentView.layer.masksToBounds = true
        
        blueButton.layer.cornerRadius = blueButton.frame.height * 0.5
        blueButton.layer.masksToBounds = true
        
        var blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        if #available(iOS 12.0, *) , traitCollection.userInterfaceStyle == .dark {
              contentView.backgroundColor = .blackLite()
              blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
          } else {
              contentView.backgroundColor = .white
          }
        if let view = contentView.superview {
            let subview = UIView(frame: CGRect(x: 0, y: -view.frame.height/2, width: view.frame.width, height: view.frame.height))
            view.insertSubview(subview, belowSubview: contentView)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = subview.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            subview.addSubview(blurEffectView)
        }
    }
    
    @IBAction func closeAction(_ sender: Any) {
        print("closeAction>tapOk")
        SwiftMessages.hide()
    }
}
