//
//  NewOrderModulePresenter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 12/09/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation

protocol NewOrderModuleViewProtocol: AnyObject {
    func updateView(orderModel: Order)
}

protocol NewOrderModulePresenterProtocol: AnyObject {
    func tapProductSelect()
    func tapAddressSelect(direction: Direction)
    var orderModel: Order { get set }
    func updateView()
    func changeDeliveryDateTime(date: Date)
    func showAddBankCardView()
    func tapLabelConditionsUse()
}

final class NewOrderModulePresenter: NewOrderModulePresenterProtocol {

    // MARK: Constants

    // MARK: Properties

    weak var view: NewOrderModuleViewProtocol?
    private var router: NewOrderModuleRouter!
    var orderModel: Order
    let firestoreService = FirestoreService()
    init(with view: NewOrderModuleViewProtocol, router: NewOrderModuleRouter, orderModel: Order) {
        self.view = view
        self.router = router
        self.orderModel = orderModel
    }
    
    // MARK: Methods
    func tapProductSelect() {
        router?.showOrderProductEdit(newOrderPresenter: self, orderModel: orderModel)
    }
    func tapAddressSelect(direction: Direction) {
        print(#function)
        router?.showAddressSelectVC(presenterDelegate: self, direction: direction)
    }
    
    func changeDeliveryDateTime(date: Date) {
        orderModel.deliveryDateTime = date
        view?.updateView(orderModel: orderModel)
    }

    func showAddBankCardView() {
        router?.showAddBankCardView(newOrderModulePresenter: self)
    }
    
    func tapLabelConditionsUse() {
        router.showInfoScreen(title: R.string.localizable.rules_use_service_title(),
                              info: R.string.localizable.rules_use_service_info())
    }
    
    func updateView() {
        print(#function)
        view?.updateView(orderModel: orderModel)
    }
    
    func createOrder() {
        print(orderModel.documentData.description)
        guard let image = orderModel.product?.image else { return }
        guard let photoData = image.compressedData(quality: 0.2) else { return }
        let fileName = AuthService.shared.uid ?? "" + "_order_" + Date().description.toBase64()
        
        UploaderService.shared.uploadImage(name: fileName, file: photoData) { [weak self] url, error in
            guard let self = self else { return }
            if let url = url, error == nil {
                self.orderModel.product?.imageUrl = url
            }
            self.saveOrder(orderModel: self.orderModel)
        }
    }
    
    private func saveOrder(orderModel: Order) {
        
        firestoreService.newDocument(doc: orderModel) { [weak self] (success, errorText, docID) in
            print("createOrder> success>", success)
            print("createOrder> errorText>", errorText)

            if success {
                self?.router?.showOrderedSuccess()
            } else {
                Popup().showAlertView(title: "Alert",
                                      message: errorText,
                                      buttonTitle: R.string.localizable.bind(),
                                      icon: nil,
                                      onReject: {
                                        // TODO Log
                                      })
            }
        }
    }
}

extension NewOrderModulePresenter: BankCardModuleCallBackProtocol {
    func closeView(success: Bool) {
        print("BankCardModuleCallBackProtocol>success", success)
        createOrder()
    }
}

extension NewOrderModulePresenter: AddrssSelectDelegate {
    func didAddress(direction: Direction, addressPoint: AddressPoint, deliveryDateTime: Date?, comment: String?) {
        
        switch direction {
        case .from:
            orderModel.addressFrom = addressPoint
            
        case .to:
            orderModel.addressTo = addressPoint
        default:
            break
        }
        self.view?.updateView(orderModel: orderModel)
    }
}
