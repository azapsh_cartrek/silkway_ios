//
//  NewOrderModuleRouter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 12/09/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation
import UIKit

protocol NewOrderModuleRouterProtocol: RouterProtocol {
    func showOrderProductEdit(newOrderPresenter: NewOrderModulePresenterProtocol, orderModel: Order)
    func showAddressSelectVC(presenterDelegate: AddrssSelectDelegate, direction: Direction)
    func showAddBankCardView(newOrderModulePresenter: NewOrderModulePresenter)
    func showOrderedSuccess()
}

final class NewOrderModuleRouter: Router, NewOrderModuleRouterProtocol {
    // MARK: Properties
    
    func showOrderProductEdit(newOrderPresenter: NewOrderModulePresenterProtocol, orderModel: Order) {
        let vc = OrderProductEditModuleViewController()
        let router = OrderProductEditModuleRouter(view: vc)
        let presenter = OrderProductEditModulePresenter(with: vc, router: router, orderModel: orderModel)
        vc.presenter = presenter
        
        presenter.newOrderPresenter = newOrderPresenter
        
        view?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showAddressSelectVC(presenterDelegate: AddrssSelectDelegate, direction: Direction) {
        let vc = OrderAddrssSelectModuleViewController()
        let router = OrderAddrssSelectModuleRouterImplementation(view: vc)
        let addressService = AddressService()
        let presenter = OrderAddrssSelectModulePresenter(with: vc,
                                                         router: router,
                                                         addressService: addressService,
                                                         direction: direction)
        presenter.delegate = presenterDelegate
        vc.presenter = presenter
        view?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showAddBankCardView(newOrderModulePresenter: NewOrderModulePresenter) {
        
        let vc = BankCardModuleViewController()
        let router = BankCardModuleRouterImplementation(with: vc)
        let bankCardModulePresenter = BankCardModulePresenter(with: vc, router: router)
        vc.presenter = bankCardModulePresenter
        bankCardModulePresenter.callbackDelegate = newOrderModulePresenter
        
        view?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showOrderedSuccess() {
        let vc = OrderSuccessViewController()
        let router = OrderSuccessRouterImplementation(viewController: vc)
        let presenter = OrderSuccessPresenter(with: vc, router: router)
        vc.presenter = presenter
        
        view?.navigationController?.pushViewController(vc, animated: true)
    }

}

