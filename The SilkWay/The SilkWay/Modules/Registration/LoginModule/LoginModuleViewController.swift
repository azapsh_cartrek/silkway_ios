//
//  LoginModuleViewController.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 14.05.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//

import UIKit
import PhoneNumberKit
import Firebase
import SVProgressHUD
import FlagKit

class LoginModuleViewController: UIViewController {

    var presenter: LoginModulePresenter?
    
    @IBOutlet weak var flagButton: UIButton!
    @IBOutlet weak var titlePhoneViewLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var mainButton: UIButton!
    @IBOutlet weak var phoneNumberTextView: PhoneNumberTextField!
    @IBOutlet weak var phoneNumberView: UIView!
    @IBOutlet weak var acceptTextLabel1: UILabel!
    @IBOutlet weak var acceptTextLabel2: UILabel!
    @IBOutlet weak var acceptTextLabel3: UILabel!
    
    // MARK: IBOutlets
    @IBOutlet
    private weak var leadingContainerViewConstraint: NSLayoutConstraint! {
        didSet {
            leadingContainerViewConstraint.constant = 20
        }
    }
    @IBOutlet
    private weak var trailingContainerViewConstraint: NSLayoutConstraint! {
        didSet {
            trailingContainerViewConstraint.constant = 20
        }
    }
    @IBOutlet weak var widthTitlePhoneViewConstraint: NSLayoutConstraint!
    var start = true
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneNumberTextView.delegate = self
        flagButton.imageView?.contentMode = UIView.ContentMode.scaleToFill
        flagButton.layer.cornerRadius = 2
        flagButton.layer.borderWidth = 2
        var bgcolor = UIColor.greyLite()   //darkgrey()
        
        if #available(iOS 12.0, *) , traitCollection.userInterfaceStyle == .dark {
            bgcolor = .blackLite2()
        }
        
        flagButton.layer.borderColor = bgcolor.cgColor
        flagButton.backgroundColor = bgcolor
        flagButton.isHidden = true
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidLayoutSubviews() {
        mainButton.setStyle(style: .blue)
        titleLabel.font = .h1TitleFont()
        titleLabel.text = R.string.localizable.loginModule_Title()
        infoLabel.font = .subtitleFont()
        infoLabel.text = R.string.localizable.loginModule_info()
        infoLabel.backgroundColor = .clear
        titlePhoneViewLabel.font = .captionFont()
        titlePhoneViewLabel.text = " " + R.string.localizable.loginModule_titlePhoneLabel() + " "
        widthTitlePhoneViewConstraint.constant = titlePhoneViewLabel.intrinsicContentSize.width
        
        if #available(iOS 12.0, *) , traitCollection.userInterfaceStyle == .dark {
            view.backgroundColor = .theBlack()
            phoneNumberView.backgroundColor = .theBlack()
            titlePhoneViewLabel.backgroundColor = .theBlack()
        } else {
            titlePhoneViewLabel.backgroundColor = .white
            view.backgroundColor = .white
        }
        
        phoneNumberTextView.borderStyle = .none
        phoneNumberTextView.withFlag = false
        phoneNumberTextView.withPrefix = true
        
        let phoneNumberKit = PhoneNumberKit()
        print(phoneNumberKit.allCountries())
        
        mainButton.setTitle(R.string.localizable.loginModule_titleBottonPhone(), for: .normal)
    
        acceptTextLabel1.text = R.string.localizable.loginModule_acceptTerms1()

        let acceptTerms2 = NSMutableAttributedString.init(string: R.string.localizable.loginModule_acceptTerms2())
        var range = NSMakeRange(0, acceptTerms2.length)
        acceptTerms2.addAttribute(.underlineStyle, value: NSUnderlineStyle.patternDot.rawValue|NSUnderlineStyle.single.rawValue, range: range)
        acceptTextLabel2.attributedText = acceptTerms2
        
        let acceptTerms3 = NSMutableAttributedString.init(string: R.string.localizable.loginModule_acceptTerms3())
        range = NSMakeRange(2, acceptTerms3.length-2)
        acceptTerms3.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.patternDot.rawValue|NSUnderlineStyle.single.rawValue, range: range)
        acceptTextLabel3.attributedText = acceptTerms3
        phoneNumberView.setBorder()
        
        if start {
            start = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                self?.phoneNumberTextView.partialFormatter.defaultRegion = "RU"
                self?.update()
            }
        }
    }
    
    func update() {
        print("update", phoneNumberTextView.currentRegion)
        if let image = Flag(countryCode: phoneNumberTextView.currentRegion)?.image(style: .roundedRect) {
            flagButton.setImage(image, for: .normal)
            flagButton.isHidden = false
        }
    }
    
    @IBAction func tapSelectCoutry(_ sender: Any) {
        presenter?.selectCoutry()
    }
    
    @IBAction func tapMainButton(_ sender: Any) {
        
        if let phoneNumber = phoneNumberTextView.text {
            print(phoneNumber)
            SVProgressHUD.show()
            PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
                print("verificationID>",verificationID)
                print("error>",error)
                
                SVProgressHUD.dismiss()
                if let error = error {
                    let popup = Popup()
                    popup.showAlertView(title: R.string.localizable.loginModule_error(),
                                        message: error.localizedDescription,
                                        buttonTitle: R.string.localizable.loginModule_buttonOk())
                    return
                }
                ProfileService.shared.myPhoneNumber = phoneNumber
                UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                self.presenter?.showSingSms(verificationID: verificationID ?? "")
            }
        }
        
    }
    

}

extension LoginModuleViewController: LoginModuleViewProtocol {
    func changeCountry(country: Country) {
        print(country.phoneCode, "  ", country.name)
        phoneNumberTextView.partialFormatter.defaultRegion = country.regionCode
        phoneNumberTextView.text = "+" + String(country.phoneCode)
        
        if let image = Flag(countryCode: country.regionCode)?.image(style: .roundedRect) {
            flagButton.setImage(image, for: .normal)
            flagButton.isHidden = false
        }
    }
}

extension LoginModuleViewController: UITextFieldDelegate {
    // UITextField Delegates
       func textFieldDidBeginEditing(_ textField: UITextField) {
        print("textFieldDidBeginEditing")
            update()
        }
        func textFieldDidEndEditing(_ textField: UITextField) {
             print("textFieldDidEndEditing")
            update()
        }
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
             print("shouldChangeCharactersIn")
            update()
            return true
        }
}
