//
//  BankCardModuleViewController.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 11/06/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import UIKit

final class BankCardModuleViewController: UIViewController {
    // MARK: Constants

    // MARK: IBOutlets
    @IBOutlet
    private weak var leadingContainerViewConstraint: NSLayoutConstraint! {
        didSet {
            leadingContainerViewConstraint.constant = 20
        }
    }
    @IBOutlet
    private weak var trailingContainerViewConstraint: NSLayoutConstraint! {
        didSet {
            trailingContainerViewConstraint.constant = 20
        }
    }
    @IBOutlet weak var containerView: UIView! {
        didSet {
            containerView.backgroundColor = .clear
        }
    }
    @IBOutlet private weak var topMainButtonConstraint: NSLayoutConstraint!
    @IBOutlet private weak var topSecondoryConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var numberTextFieldView: TextFieldView!
    @IBOutlet weak var nameTextFiledView: TextFieldView!
    @IBOutlet weak var extTextFiledView: TextFieldView!
    @IBOutlet weak var cvvTextFIeldView: TextFieldView!
    
    @IBOutlet weak var mainButton: UIButton!
    @IBOutlet weak var secondoryButton: UIButton!
    
    // MARK: Properties

    var presenter: BankCardModulePresenter!
    
    // MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewDidLayoutSubviews() {
        if #available(iOS 12.0, *) , traitCollection.userInterfaceStyle == .dark {
            view.backgroundColor = .theBlack()
        } else {
            view.backgroundColor = .white
        }
        
        titleLabel.text = R.string.localizable.bankCardModuleTitle()
        infoLabel.text = R.string.localizable.bankCardModuleInfo()
        if UIDevice.modelSize() != .small {
            topMainButtonConstraint.constant = 20
            topSecondoryConstraint.constant = 20
            titleLabel.font = .h1TitleFont()
            infoLabel.font = .subtitleFont()
        }
        
        numberTextFieldView.configure(id: R.string.localizable.bankCardModuleCardNumberTitle(),
                                      title: R.string.localizable.bankCardModuleCardNumberTitle(),
                                      placeholder: R.string.localizable.bankCardModuleCardNumberTitle(),
                                      maskString: "0000 0000 0000 0000 00")
        numberTextFieldView.setFont(font: .subtitleFont())
        numberTextFieldView.setKeybord(keyboardType: .numberPad)
        numberTextFieldView.setPrimaryKey(returnKeyType: .next)
        
        nameTextFiledView.configure(id: R.string.localizable.bankCardModuleCardNameTitle(),
                                    title: R.string.localizable.bankCardModuleCardNameTitle(),
                                       placeholder: R.string.localizable.bankCardModuleCardNameTitle(),
                                       maskString: nil)

        nameTextFiledView.setFont(font: .subtitleFont())
        nameTextFiledView.setPrimaryKey(returnKeyType: .next)
        nameTextFiledView.upFirstChar = true
        nameTextFiledView.delegate = self
        
        extTextFiledView.configure(id: R.string.localizable.bankCardModuleCardNumberTitle(),
                                   title: R.string.localizable.bankCardModuleCardExtTitle(),
                                       placeholder: R.string.localizable.bankCardModuleCardExtTitle(),
                                       maskString: "00/00")
        extTextFiledView.setFont(font: .subtitleFont())
        extTextFiledView.setKeybord(keyboardType: .numberPad)
        extTextFiledView.setPrimaryKey(returnKeyType: .next)
        
        cvvTextFIeldView.configure(id: R.string.localizable.bankCardModuleCardNumberTitle(),
                                   title: R.string.localizable.bankCardModuleCardCcvTitle(),
                                       placeholder: R.string.localizable.bankCardModuleCardCcvTitle(),
                                       maskString: "000")
        cvvTextFIeldView.setFont(font: .subtitleFont())
        cvvTextFIeldView.setKeybord(keyboardType: .numberPad)
        cvvTextFIeldView.setPrimaryKey(returnKeyType: .done)
        cvvTextFIeldView.setSecureText(true)
        
        mainButton.setTitle(R.string.localizable.bankCardModuleMainButtonTitle(), for: .normal)
        mainButton.setStyle(style: .blue)
        secondoryButton.setTitle(R.string.localizable.bankCardModuleSecondoryButtonTitle(), for: .normal)
        secondoryButton.setStyle(style: .standart)
    }
    // MARK: Methods
    func checkDateValidation() -> Bool {
        var result = true
        numberTextFieldView.checkValidation(minChars: 19, result: &result)
        nameTextFiledView.checkValidation(minChars: 1, result: &result)
        cvvTextFIeldView.checkValidation([3], result: &result)
        extTextFiledView.checkValidation([5], result: &result)
         
        return result
    }
    
    // MARK: IBActions
    @IBAction func tapBack(_ sender: Any) {
        navigationController?.popViewController(animated: false)
        
    }
    @IBAction func tapMainButton(_ sender: Any) {
        if checkDateValidation() {
            presenter?.tapSave()
        }
    }
    
    @IBAction func tapSeconderyButton(_ sender: Any) {
        presenter?.tapSave()
    }
}


extension BankCardModuleViewController: BankCardModuleViewProtocol {
    
}

extension BankCardModuleViewController: TextFieldViewDelegate {
    func tapLeftButtonAction(Action: Action) {
    }
    
    func tapRightButtonAction(Action: Action) {
    }
    
    func tapPrimaryAction(id: String, text: String) {
        print(#function)
        print("id>", id, "  text>", text)
        if id == R.string.localizable.bankCardModuleCardNameTitle() {
            extTextFiledView.showKeybord()
        }
    }
}
