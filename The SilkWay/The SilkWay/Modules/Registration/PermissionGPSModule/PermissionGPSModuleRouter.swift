//
//  PermissionGPSModuleRouter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 16/05/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation
import UIKit

protocol PermissionGPSModuleRouter: AnyObject {
    func showPermissionPushView()
}

final class PermissionGPSModuleRouterImplementation: PermissionGPSModuleRouter {
    
    // MARK: Properties
    weak var view: PermissionGPSModuleViewController?
    weak var presenter: PermissionGPSModulePresenter? 
    
    init(with viewController: PermissionGPSModuleViewController) {
        self.view = viewController
    }
    // MARK: Internal helpers
    
    func showPermissionPushView() {
        let vc = ModuleBulder.permissionPush()
        view?.navigationController?.pushViewController(vc, animated: true)
    }
}

