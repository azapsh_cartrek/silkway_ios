//
//  UIImage+Extensions.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 28.05.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//

import Foundation
import UIKit

public extension UIImage {
    func compressed(quality: CGFloat = 0.5) -> UIImage? {
         guard let data = compressedData(quality: quality) else { return nil }
         return UIImage(data: data)
     }
     func compressedData(quality: CGFloat = 0.5) -> Data? {
         return jpegData(compressionQuality: quality)
     }
    
    func addText(_ text: String) -> UIImage {
        UIGraphicsBeginImageContext(self.size)
        self.draw(in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
        let font=UIFont(name: "Helvetica-Bold", size: 14)!
        let text_style=NSMutableParagraphStyle()
        text_style.alignment = NSTextAlignment.center
        let text_color=UIColor.white
        let attributes=[NSAttributedString.Key.font:font, NSAttributedString.Key.paragraphStyle:text_style, NSAttributedString.Key.foregroundColor:text_color]
        let text_h=font.lineHeight
        let text_y=(self.size.height-text_h)/2
        let text_rect=CGRect(x: 0, y: text_y, width: self.size.width, height: text_h)
        text.draw(in: text_rect.integral, withAttributes: attributes)
        let result=UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result!
    }
}


