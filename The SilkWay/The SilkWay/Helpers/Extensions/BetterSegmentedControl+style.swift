//
//  BetterSegmentedControl+style.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 28.06.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//

import Foundation
import BetterSegmentedControl

enum BetterSegmentedControlStyle {
    case standart
    case blue
}


extension BetterSegmentedControl {
    private enum Constants {
        static let cornerRadius: CGFloat = 6
        static let borderWidth: CGFloat = 1
        
    }
    
    func  setStyle(style: BetterSegmentedControlStyle,defaultIndex: Int, withTitles: [String]) {
        
        if style == .blue {
            if #available(iOS 12.0, *) , traitCollection.userInterfaceStyle == .dark {
                
                segments = LabelSegment.segments(
                    withTitles: withTitles,
                    normalFont: UIFont.typeFont(),
                    normalTextColor: R.color.tabDefaultTextColor(),
                    selectedFont: UIFont.typeFont(),
                    selectedTextColor: R.color.tabSelectTextColor())
                setIndex(defaultIndex)
                options = [
                    .backgroundColor(R.color.tabSelectTextColor() ?? UIColor.gray),
                    .indicatorViewBackgroundColor(R.color.tabSelectBackgraundColor() ?? UIColor.orange),
                    .cornerRadius(Constants.cornerRadius),
                    .indicatorViewBorderWidth(Constants.borderWidth),
                    .indicatorViewBorderColor(R.color.tabSelectBackgraundColor() ?? UIColor.orange),
                    .indicatorViewInset(0),
                    .cornerRadius(0)
                ]
                self.layer.cornerRadius = Constants.cornerRadius
                self.layer.borderColor = R.color.tabSelectBorderColor()?.cgColor
                self.layer.borderWidth = Constants.borderWidth
                
            } else {
                segments = LabelSegment.segments (
                    withTitles: withTitles,
                    normalFont: UIFont.typeFont(),
                    normalTextColor: R.color.tabDefaultTextColor(),
                    selectedFont: UIFont.typeFont(),
                    selectedTextColor: R.color.tabSelectTextColor())
                setIndex(defaultIndex)
                options = [
                    .backgroundColor(R.color.tabSelectTextColor() ?? UIColor.gray),
                    .indicatorViewBackgroundColor(R.color.tabSelectBackgraundColor() ?? UIColor.orange),
                    .cornerRadius(Constants.cornerRadius),
                    .indicatorViewBorderWidth(Constants.borderWidth),
                    .indicatorViewBorderColor(R.color.tabSelectBackgraundColor() ?? UIColor.orange),
                    .indicatorViewInset(0),
                    .cornerRadius(0)
                ]
                self.layer.cornerRadius = Constants.cornerRadius
                self.layer.borderColor = R.color.tabSelectBorderColor()?.cgColor
                self.layer.borderWidth = Constants.borderWidth
                
            }
        }
        
    }
}
