//
//  StopOverItemView.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 15.02.2021.
//  Copyright © 2021 Ахмед Фокичев. All rights reserved.
//

import UIKit
import SnapKit

final class StopOverItemView: UIView {
    
    // MARK: Subview
    let buttonView: ButtonView = {
        let buttonView = ButtonView()
        buttonView.configure(action: Action.none)
        buttonView.placeholder = R.string.localizable.travelModuleStopOverTitle()
        buttonView.onRightButtonAction(action: .deviation, image: R.image.closeGray())
        
        return buttonView
    }()
    
    // MARK: LifeCycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func commonInit() {
        self.addSubview(buttonView)
        buttonView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        buttonView.delegate = self
    }
    
    func setup(stopoverModel: WaypointModel
) {
        
        var description = ""
        if let date = stopoverModel.deliveryDateTime {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.none
            dateFormatter.timeStyle = DateFormatter.Style.short
            description = dateFormatter.string(from: date)
            
            dateFormatter.dateStyle = DateFormatter.Style.medium
            dateFormatter.timeStyle = DateFormatter.Style.none
            description = description + ", " + dateFormatter.string(from: date)
        }
        
        buttonView.title = stopoverModel.addrssPoint?.streetAddress
        buttonView.setDescription(text: description)
    }
    
}

extension StopOverItemView: ButtonViewDelegate {
    func tapAction(action: Action) {
        
    }
    
    func tapRightAction(action: Action) {
        self.removeFromSuperview()
    }
}
