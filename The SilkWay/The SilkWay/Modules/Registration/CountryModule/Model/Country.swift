//
//  Country.swift
//  urent-ios
//
//  Created by Denis Borodavchenko on 27/12/2019.
//  Copyright © 2019 Denis Borodavchenko. All rights reserved.
//

import Foundation

final class Country: Comparable, Equatable {
    var name: String
    var phoneCode: Int
    var regionCode: String
    
    init(name: String, phoneCode: Int, regionCode: String) {
        self.name = name
        self.phoneCode = phoneCode
        self.regionCode = regionCode
    }
    
    static func < (lhs: Country, rhs: Country) -> Bool {
        return lhs.name < rhs.name
    }

    static func == (lhs: Country, rhs: Country) -> Bool {
        return lhs.name == rhs.name
    }
}
