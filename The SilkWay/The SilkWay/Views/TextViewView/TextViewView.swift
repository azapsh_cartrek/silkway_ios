//
//  TextFieldView.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 14.06.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//
import UIKit
import Foundation

protocol TextViewViewDelegate: AnyObject {
    func tapPrimaryAction(id: String, text: String)
    func tapLeftTextViewAction(action: Action)
    func tapRightTextViewAction(action: Action)
    func didTextChanged(text: String)
}

extension TextViewViewDelegate {
    func tapLeftTextViewAction(action: Action) {}
    func tapRightTextViewAction(action: Action) {}
    func didTextChanged(text: String) {}
}

class TextViewView: UIView, UITextViewDelegate {
    weak var delegate: TextViewViewDelegate?
    
    @IBOutlet weak var widthLeftButton: NSLayoutConstraint!
    @IBOutlet weak var widthrightButton: NSLayoutConstraint!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet weak var widthTitleLabelCostraint: NSLayoutConstraint!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    private var validCounts = [Int]()
    var validIfNotNil = false
    
    var noValidColor = UIColor.red
    var text: String {
        get {
            if let text = textView.text {
                return text
            } else {
                return ""
            }
        }
        set (val) {
            textView.text = val
        }
    }

    private var placeholder = ""
    var leftButtonAction: Action?
    var rigthButtonAction: Action?
    
    var upFirstChar = true
    // MARK: Methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        guard let nib = R.nib.textViewView.firstView(owner: self) else {
            return
        }
        addSubview(nib)
        nib.pin(to: self)
        textView.delegate = self
    }
    
    @IBAction func didTextChanged(_ sender: Any) {
        update()
    }
    
    func update() {
        borderView.layer.borderColor = R.color.textFieldViewBorderColor()?.cgColor
        borderView.layer.borderWidth = 1
        borderView.layer.cornerRadius = 5
        borderView.backgroundColor = R.color.backgroundColor()
        
        if let text = textView.text {
            print(text)
            delegate?.didTextChanged(text: text)
            if upFirstChar {
                textView.text = text.capitalizingFirstLetter()
            }
        }
    }
    
    func checkValidation (minChars: Int? = 0, _ validCounts: [Int]? = nil, result: inout Bool ) {
        
        var validation = true
        
        if let min = minChars,  min > text.count {
            validation = false
            result = false
        }
        
        if let counts = validCounts, counts.count > 0, !counts.contains(text.count) {
            validation = false
            result = false
        }
        
        borderView.layer.borderWidth = 1
        borderView.layer.cornerRadius = 5
        borderView.layer.borderColor = validation ? R.color.textFieldViewBorderColor()?.cgColor : R.color.textFieldViewBorderColorNoValid()?.cgColor
    }
    
    private func setTitle(title: String) {
        titleLabel.text = "  " + title + "  "
        widthTitleLabelCostraint.constant = titleLabel.intrinsicContentSize.width
    }
    
    func onLeftButtonAction(action: Action, image: UIImage?) {
        leftButtonAction = action
        leftButton.setImage(image, for: .normal)
        widthLeftButton.constant = 32
        leftButton.isHidden = false
    }
    func onRightButtonAction(action: Action, image: UIImage?) {
        print(#function,action.rawValue)
        rigthButtonAction = action
        rightButton.setImage(image, for: .normal)
        widthrightButton.constant = 32
        rightButton.isHidden = false
    }
    
    func configure(title: String, placeholder: String) {
        
        if leftButtonAction == nil {
            widthLeftButton.constant = 0
            leftButton.isHidden = true
        }
        if rigthButtonAction == nil {
            widthrightButton.constant = 0
            rightButton.isHidden = true
        }
        
        self.placeholder = placeholder
        setTitle(title: title)
        
        titleLabel.backgroundColor = R.color.backgroundColor()
        titleLabel.isHidden = true
        
        textView.text = placeholder
        textView.textColor = R.color.textViewViewColorPlaceholder()
        titleLabel.textColor = R.color.textViewViewColorPlaceholder()
        textView.backgroundColor = R.color.backgroundColor()
        
        update()
    }
    func setFont(font: UIFont) {
        textView.font = font
    }
    func setKeybord(keyboardType: UIKeyboardType){
        textView.keyboardType = keyboardType
    }
    func setPrimaryKey(returnKeyType: UIReturnKeyType){
        textView.returnKeyType = returnKeyType
    }
    func setSecureText(_ secure: Bool){
        textView.isSecureTextEntry = secure
    }
    func showKeybord() {
        textView.becomeFirstResponder()
    }
    func setValidateCounts(_ values: [Int]) {
        validCounts = values
    }
    @IBAction func tapRightButton(_ sender: Any) {
        guard let action = rigthButtonAction else { return }
        delegate?.tapRightTextViewAction(action: action)
    }
    @IBAction func tapLeftButton(_ sender: Any) {
        guard let action = leftButtonAction else { return }
        delegate?.tapLeftTextViewAction(action: action)
    }
}

//UITextViewDelegate
extension TextViewView {
    
    func textViewDidChange(_ textView: UITextView) {
        update()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if (textView.text == placeholder && textView.textColor == R.color.textViewViewColorPlaceholder()) {
            textView.text = ""
            textView.textColor = R.color.textViewViewColor()
            self.titleLabel.isHidden = false
        }
        textView.becomeFirstResponder() //Optional
    }

    func textViewDidEndEditing(_ textView: UITextView){
        if (textView.text == "") {
            self.titleLabel.isHidden = true
            textView.text = placeholder
            textView.textColor = R.color.textViewViewColorPlaceholder()
        }
        textView.resignFirstResponder()
    }
    
}
