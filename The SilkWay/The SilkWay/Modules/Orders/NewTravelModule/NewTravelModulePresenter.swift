//
//  TravelModulePresenter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 13/02/2021.
//  Copyright © 2021 SilkWay. All rights reserved.
//

import Foundation

protocol NewTravelDelegate {
    func didNewTravel(travelModel: TravelModel)
}


protocol NewTravelModuleViewProtocol: AnyObject {
    func updateView(travelModel: TravelModel)
    func addStopOverView(stopoverModel: WaypointModel
)
}

protocol NewTravelModulePresenterProtocol: AnyObject {
    func close()
    func tapAddressSelect(direction: Direction)
    func tapLabelConditionsUse()
    func changeDeliveryDateTime(date: Date)
    var travelModel: TravelModel { get set }
    func setDeviation(value: Int, text: String)
    func tapAddNewStopOver()
    func tapNext()
}

final class NewTravelModulePresenter: NewTravelModulePresenterProtocol {

    // MARK: Constants
    // MARK: Properties

    weak var view: NewTravelModuleViewProtocol?
    var router: NewTravelModuleRouter?
    var travelModel: TravelModel
    var delegate: NewTravelDelegate?
    private let firestoreService = FirestoreService()
    
    init(with view: NewTravelModuleViewProtocol, router: NewTravelModuleRouter, delegate: NewTravelDelegate?, travelModel: TravelModel) {
        self.view = view
        self.router = router
        self.travelModel = travelModel
        self.delegate = delegate
    }
    
    // MARK: Methods
    private func createTrip() {
        print(travelModel.documentData)
        firestoreService.newDocument(doc: travelModel) { [weak self] (success, errorText, docID) in
            guard let travelModel = self?.travelModel else { return }

            print("createTrip> success>", success)
            print("createTrip> errorText>", errorText)

            if success {
                self?.router?.popOver(animated: false)
                self?.delegate?.didNewTravel(travelModel: travelModel)
            } else {
                Popup().showAlertView(title: "Alert",
                                      message: errorText,
                                      buttonTitle: R.string.localizable.bind(),
                                      icon: nil,
                                      onReject: {
                                        // TODO Logs
                                      })
            }
        }
    }
    
    func close() {
        router?.popOver()
    }
    
    func tapAddressSelect(direction: Direction) {
        router?.showAddressSelectVC(presenterDelegate: self, direction: direction)
        
    }
    
    func tapLabelConditionsUse() {
        router?.showInfoScreen(title: R.string.localizable.rules_use_service_title(),
                              info: R.string.localizable.rules_use_service_info())
    }
    
    func changeDeliveryDateTime(date: Date) {
        travelModel.addressFrom?.deliveryDateTime = date
        view?.updateView(travelModel: travelModel)
    }
    
    func setDeviation(value: Int, text: String) {
        print(#function, value , text)
        travelModel.deviation = value
        travelModel.deviationText = text
        view?.updateView(travelModel: travelModel)
    }
    func tapAddNewStopOver() {
        router?.showNewTravelStopOver(delegate: self, tripId: travelModel.id)
    }
    
    func tapNext() {
        createTrip()
    }
    
}

extension NewTravelModulePresenter: AddrssSelectDelegate {
    func didAddress(direction: Direction, addressPoint: AddressPoint, deliveryDateTime: Date? = nil, comment: String?) {
        let uuid = UUID().uuidString
        let waypointModel = WaypointModel(id: uuid, addrssPoint: addressPoint, deliveryDateTime: deliveryDateTime, sort: 0, tripId: travelModel.id)
        
        switch direction {
        case .from:
            travelModel.addressFrom = waypointModel
            
        case .to:
            travelModel.addressTo = waypointModel
            
        case .stopOver:
            travelModel.waypoints.append(waypointModel)
            
        }
        self.view?.updateView(travelModel: travelModel)
    }
}

extension NewTravelModulePresenter: StopOverModuleDelegate {
    func addStopover(waypointModel: WaypointModel) -> Bool {
        if travelModel.waypoints.filter { $0 == waypointModel}.first != nil {
            return false
        } else {
            travelModel.waypoints.append(waypointModel)
            view?.addStopOverView(stopoverModel: waypointModel)
            return true
        }
    }
}
