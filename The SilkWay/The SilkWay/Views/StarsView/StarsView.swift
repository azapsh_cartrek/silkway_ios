//
//  StarsView.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 03.07.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//

import Foundation
// MARK: IBOutlets


import UIKit
import Foundation
import SnapKit

protocol StarsViewDelegate: class {
}

class StarsView: UIView {
    weak var delegate: StarsViewDelegate?
    
    var box = UIView()
    var starts: Double = 4
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        guard let nib = R.nib.starsView.firstView(owner: self) else {
            return
        }
        addSubview(nib)
        nib.pin(to: self)
        configureLayoutBox()
        //        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(fotoBordViewTap(sender:)))
        //        subView.isUserInteractionEnabled = true
        //        subView.addGestureRecognizer(tapGesture)
    }
    
    func setStars(value: Double) {
        starts = value
        configureLayoutContent()
    }
    func configureLayoutBox() {
        if #available(iOS 12.0, *) , traitCollection.userInterfaceStyle == .dark {
            box.backgroundColor = .theBlack()
        } else {
            box.backgroundColor = .white
        }
        self.addSubview(box)
        box.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.centerX.top.bottom.equalToSuperview()
        }
    }
    
    func configureLayoutContent() {
        for obj in box.subviews {
            obj.removeFromSuperview()
        }
        for i in 1...5 {
            let starImageView = UIImageView()
            box.addSubview(starImageView)
            if starts >= Double(i) {
                starImageView.image = R.image.starOn()
            } else {
                starImageView.image = R.image.starOff()
            }
            starImageView.snp.makeConstraints { (make) in
                make.top.equalToSuperview()
                make.height.width.equalTo(20)
                make.left.equalToSuperview().offset((i-1) * 24)
            }
        }
    }
    
    
    
}
