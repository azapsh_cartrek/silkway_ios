//
//  AddressCacheService.swift
//  The SilkWay
//
//  Created by Azapsh on 06.06.2021.
//  Copyright © 2021 Ахмед Фокичев. All rights reserved.
//

import Foundation

final class AddressCacheService {
    
    private var cacheList = [AddressPoint]()
    
    func saveAddressPoint(value: AddressPoint) {
        
        if let list = try? getCacheAddressPointList() {
            cacheList = list
        }
        
        var isNew = true
        cacheList.forEach {
            if $0.address1 == value.address1 && $0.address2 == value.address2 {
                isNew = false
            }
        }
        
        if isNew {
            cacheList.append(value)
            try? saveCacheAddressPointList(value: cacheList)
        }
    }
    
    private func saveCacheAddressPointList(value: [AddressPoint]) throws {
        let placesData = NSKeyedArchiver.archivedData(withRootObject: value)
        UserDefaults.standard.set(placesData, forKey: "addressPointCacheList")
    }
    
    func getCacheAddressPointList() throws -> [AddressPoint] {
        guard let orderData = UserDefaults.standard.data(forKey: "addressPointCacheList"),
              let list = NSKeyedUnarchiver.unarchiveObject(with: orderData) as? [AddressPoint] else { return [] }
        list.forEach {
            $0.isCache = true
        }
        return list
    }
}
