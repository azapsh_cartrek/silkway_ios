//
//  Review.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 18.07.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//

import Foundation

class Review {
    var id: String
    var userId = ""
    var description: String
    var rate: Double = 0
    var sort = 0
    
    init(id:String, userId: String, description: String, rate: Double, sort: Int = 0) {
        self.id = id
        self.userId = id
        self.description = description
        self.rate = rate
        self.sort = sort
    }
}
