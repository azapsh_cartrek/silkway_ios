//
//  TravelModuleRouter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 14/02/2021.
//  Copyright © 2021 SilkWay. All rights reserved.
//

import Foundation
import UIKit

protocol TravelModuleRouterProtocol: RouterProtocol {

}

final class TravelModuleRouter: Router, TravelModuleRouterProtocol {
    
    // MARK: Properties
    weak var presenter: TravelModulePresenter? 
    
    // MARK: Internal helpers

}

