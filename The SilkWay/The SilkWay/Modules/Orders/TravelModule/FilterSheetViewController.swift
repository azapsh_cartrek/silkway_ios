//
//  FilterSheetViewController.swift
//  The SilkWay
//
//  Created by Azapsh on 17.07.2021.
//  Copyright © 2021 Ахмед Фокичев. All rights reserved.
//

import UIKit

protocol FilterSheetDelegate: AnyObject {
    func didSelect(sort: SortOrders)
}

enum SortOrders {
    case price
    case location
}

class FilterSheetViewController: UIViewController {
    
    // MARK: Constants
    private enum Constants {
        static let offset: CGFloat = 20
    }
    
    private let checkinImageView: UIImageView = {
        let checkinImage = UIImageView()
        checkinImage.image = R.image.galka()
        checkinImage.contentMode = .scaleAspectFit
        return checkinImage
    }()
    
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = R.string.localizable.filterSheetViewControllerTitle()
        label.font = .h3TitleFont()
        label.textAlignment = .left
        return label
    }()
    
    private let sortPriceButton: UIButton = {
        let button = UIButton()
        button.setTitle("  " + R.string.localizable.filterSheetViewControllerSortedPrice() , for: .normal)
        button.titleLabel?.font = .typeFont()
        button.contentHorizontalAlignment = .left
        return button
    }()
    
    private let sortLocationButton: UIButton = {
        let button = UIButton()
        button.setTitle("  " + R.string.localizable.filterSheetViewControllerSortedLocation() , for: .normal)
        button.titleLabel?.font = .typeFont()
        button.contentHorizontalAlignment = .left
        return button
    }()
    
    private let mainButton: UIButton = {
        let button = UIButton()
        button.setTitle(R.string.localizable.filterSheetViewControllerMakeButtonTitle() , for: .normal)
        button.setStyle(style: .blue)
        return button
    }()
    
    var deleagte: FilterSheetDelegate?
    var sort = SortOrders.price
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 12.0, *) , traitCollection.userInterfaceStyle == .dark {
            view.layer.cornerRadius = 10
            view.layer.borderWidth = 2
            view.layer.borderColor = UIColor.gray.withAlphaComponent(0.5).cgColor
        }
        
        sortPriceButton.addTarget(self, action: #selector(tapSortPrice), for: .touchUpInside)
        sortLocationButton.addTarget(self, action: #selector(tapSortLocation), for: .touchUpInside)
        mainButton.addTarget(self, action: #selector(tapMake), for: .touchUpInside)
        
        switch sort {
        case .price:
            sortPriceButton.setStyle(style: .checkIn)
            sortLocationButton.setStyle(style: .checkOut)
            setChikedIcon(button: sortPriceButton)
            
        case .location:
            sortPriceButton.setStyle(style: .checkOut)
            sortLocationButton.setStyle(style: .checkIn)
            setChikedIcon(button: sortLocationButton)
        }
    }
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = R.color.backgroundColor()
        view.addSubview(titleLabel)
        view.addSubview(sortPriceButton)
        view.addSubview(sortLocationButton)
        view.addSubview(mainButton)
        
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(32)
            $0.trailing.equalToSuperview()
            $0.leading.equalToSuperview().inset(Constants.offset)
        }
        sortPriceButton.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(16)
            $0.trailing.equalToSuperview().inset(Constants.offset)
            $0.leading.equalToSuperview().inset(Constants.offset)
            $0.height.equalTo(46)
        }
        
        sortLocationButton.snp.makeConstraints {
            $0.top.equalTo(sortPriceButton.snp.bottom).offset(16)
            $0.trailing.equalToSuperview().inset(Constants.offset)
            $0.leading.equalToSuperview().inset(Constants.offset)
            $0.height.equalTo(46)
        }
        
        mainButton.snp.makeConstraints {
            $0.top.equalTo(sortLocationButton.snp.bottom).offset(24)
            $0.trailing.equalToSuperview().inset(Constants.offset)
            $0.leading.equalToSuperview().inset(Constants.offset)
            $0.height.equalTo(50)
        }
    }
    
    private func setChikedIcon(button: UIButton) {
        checkinImageView.removeFromSuperview()
        button.addSubview(checkinImageView)
        checkinImageView.snp.makeConstraints {
            $0.trailing.equalToSuperview().inset(8)
            $0.centerY.equalToSuperview()
            $0.height.width.equalTo(24)
        }
    }
    
    @objc func tapSortPrice(sender: UIButton!) {
        sort = .price
        sortPriceButton.setStyle(style: .checkIn)
        sortLocationButton.setStyle(style: .checkOut)
        setChikedIcon(button: sortPriceButton)
    }
    @objc func tapSortLocation(sender: UIButton!) {
        sort = .location
        sortPriceButton.setStyle(style: .checkOut)
        sortLocationButton.setStyle(style: .checkIn)
        setChikedIcon(button: sortLocationButton)
    }
    
    @objc func tapMake(sender: UIButton!) {
        deleagte?.didSelect(sort: sort)
        self.dismiss(animated: true, completion: nil)
    }
}
