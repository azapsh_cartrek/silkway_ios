//
//  ChatsModuleRouter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 13/06/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation
import UIKit

protocol ChatsModuleRouter: class {

}

final class ChatsModuleRouterImplementation: ChatsModuleRouter {
    
    // MARK: Properties
    weak var view: ChatsModuleViewController?
    weak var presenter: ChatsModulePresenter? 
    
    init(with viewController: ChatsModuleViewController) {
        self.view = viewController
    }
    // MARK: Internal helpers

}

