//
//  OrderAddrssSelectModuleViewController.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 19/09/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import UIKit
import SnapKit

final class OrderAddrssSelectModuleViewController: UIViewController {
    
    // MARK: Constants
    private enum Constants {
        static let offset: CGFloat = 20
    }
    // MARK: IBOutlets
    
    // MARK: - Subviews
    private let backButton: UIButton = {
        let button = UIButton()
        button.setImage(R.image.back(), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -20, bottom: 0, right: 0)
        button.contentMode = .scaleAspectFit
        
        return button
    }()
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = R.string.localizable.newOrderModuleTitle()
        label.font = .h2TitleFont()
        label.textAlignment = .left
        
        return label
    }()
    private let searchTextView: TextViewView = {
        let textView = TextViewView()
        textView.configure(title: R.string.localizable.orderAddrssSelectSearchTextViewPlaceholder(),
                                placeholder: R.string.localizable.orderAddrssSelectSearchTextViewPlaceholder())
        
        return textView
    }()
    private let myLocationButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(.black, for: .normal)
        button.setTitle(R.string.localizable.orderAddrssSelectButtonMylocationTitle(), for: .normal)
        button.titleLabel?.font = .typeFont()
        button.titleLabel?.numberOfLines = 0
        button.contentHorizontalAlignment = .left
        button.setTitleColor(R.color.labelTextColor(), for: .normal)
        return button
    }()
    private let myLocationButtonIcon: UIImageView = {
        let icon = UIImageView()
        icon.image = R.image.location_icon()
        
        return icon
    }()
        
        //location_icon
    
    private let listTableView: UITableView = {
        let tableview = UITableView()
       // button.setTitle(R.string.localizable.OrderAdres, for: T##UIControl.State)
        return tableview
    }()
    // MARK: Properties

    var presenter: OrderAddrssSelectModulePresenter?

    // MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = R.color.backgroundColor()
        listTableView.backgroundColor = R.color.backgroundColor()
        
    }

    override func loadView() {
        super.loadView()
        
        view.addSubview(backButton)
        view.addSubview(titleLabel)
        view.addSubview(searchTextView)
        view.addSubview(myLocationButton)
        view.addSubview(listTableView)
        
        backButton.addTarget(self, action: #selector(tapBack), for: .touchUpInside)
        backButton.snp.makeConstraints {
            $0.top.equalToSuperview().offset(44)
            $0.left.equalToSuperview().offset(Constants.offset)
            $0.height.width.equalTo(44)
        }
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(backButton.snp.bottom).offset(8)
            $0.trailing.equalToSuperview().inset(Constants.offset)
            $0.leading.equalToSuperview().offset(Constants.offset)
            $0.height.equalTo(24)
        }
        
        if presenter?.direction == .from {
            titleLabel.text = R.string.localizable.newOrderModuleFromPlaceholder()
        }
        if presenter?.direction == .to {
            titleLabel.text = R.string.localizable.newOrderModuleToPlaceholder()
        }

        searchTextView.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(8)
            $0.trailing.equalToSuperview().inset(Constants.offset)
            $0.leading.equalToSuperview().offset(Constants.offset)
            $0.height.equalTo(48)
        }
        searchTextView.delegate = self
        
        myLocationButton.addTarget(self, action: #selector(tapMyLocation), for: .touchUpInside)
        myLocationButton.snp.makeConstraints {
            $0.top.equalTo(searchTextView.snp.bottom).offset(8)
            $0.trailing.equalToSuperview().inset(Constants.offset)
            $0.leading.equalToSuperview().offset(54)
            $0.height.equalTo(36)
        }

        view.addSubview(myLocationButtonIcon)
        myLocationButtonIcon.snp.makeConstraints {
            $0.centerY.equalTo(myLocationButton)
            $0.leading.equalToSuperview().offset(Constants.offset)
            $0.height.width.equalTo(30)
        }
        
        listTableView.snp.makeConstraints {
            $0.top.equalTo(myLocationButton.snp.bottom).offset(8)
            $0.trailing.equalToSuperview().inset(Constants.offset)
            $0.leading.equalToSuperview().offset(Constants.offset)
            $0.bottom.equalToSuperview()
        }
        listTableView.delegate = self
        listTableView.dataSource = self
        
    }
    // MARK: Methods

    // MARK: IBActions
    @objc func tapBack(_ sender: UIButton) {
        print(#function)
        navigationController?.popViewController(animated: true)
    }
    @objc func tapMyLocation(_ sender: UIButton) {
        presenter?.tapMyLocation()
        print(#function)
    }
    
}

extension OrderAddrssSelectModuleViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.addresList.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = AddressTableViewCell()
        if let address = presenter?.addresList[indexPath.row] {
            cell.configure(address: address)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let addres = presenter?.addresList[indexPath.row] {
            presenter?.selectAdderss(addressPoint: addres)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 82
    }
}

extension OrderAddrssSelectModuleViewController: TextViewViewDelegate {
    func tapPrimaryAction(id: String, text: String) {
    }
    
    func didTextChanged(text : String) {
        presenter?.didsearchTextChanged(text: text)
    }
}

extension OrderAddrssSelectModuleViewController: OrderAddrssSelectModuleViewProtocol {
    func tableUpdate() {
        listTableView.reloadData()
    }
}
