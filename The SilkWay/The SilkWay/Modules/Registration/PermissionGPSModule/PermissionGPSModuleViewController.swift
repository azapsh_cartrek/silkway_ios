//
//  PermissionGPSModuleViewController.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 16/05/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import UIKit

final class PermissionGPSModuleViewController: UIViewController {
    // MARK: Constants

    // MARK: IBOutlets
    @IBOutlet
    private weak var leadingContainerViewConstraint: NSLayoutConstraint! {
        didSet {
            leadingContainerViewConstraint.constant = 20
        }
    }
    @IBOutlet
    private weak var trailingContainerViewConstraint: NSLayoutConstraint! {
        didSet {
            trailingContainerViewConstraint.constant = 20
        }
    }
    @IBOutlet private weak var containerView: UIView! {
        didSet {
            containerView.backgroundColor = .clear
        }
    }
    @IBOutlet weak var mainButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var infoTextview: UITextView! {
        didSet {
                   if UIDevice.modelSize() == .small {
                       infoTextview.font = .typeFont()
                   }
               }
    }
    // MARK: Properties
    
    var presenter: PermissionGPSModulePresenter!

    // MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewDidLayoutSubviews() {
        if #available(iOS 12.0, *) , traitCollection.userInterfaceStyle == .dark {
            view.backgroundColor = .theBlack()
        } else {
            view.backgroundColor = .white
        }
        titleLabel.text = R.string.localizable.permissionGPSModule_Title()
        infoTextview.text = R.string.localizable.permissionGPSModule_info()
        mainButton.setTitle(R.string.localizable.permissionGPSModule_titleMainBotton(), for: .normal)
        mainButton.setStyle(style: .blue)
    }
    
    // MARK: Methods

    // MARK: IBActions
    @IBAction func tapMainButton(_ sender: Any) {
        presenter?.tapMainButton()
    }
    
}

extension PermissionGPSModuleViewController: PermissionGPSModuleViewProtocol {
    
}
