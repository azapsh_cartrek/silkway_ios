//
//  PermissionGPSModulePresenter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 16/05/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation
import CoreLocation

protocol PermissionGPSModuleViewProtocol: class {

}

protocol PermissionGPSModuleViewPresenter: class {
    init(with view: PermissionGPSModuleViewProtocol, router: PermissionGPSModuleRouter)
     func tapMainButton ()
}

final class PermissionGPSModulePresenter: PermissionGPSModuleViewPresenter {

    // MARK: Constants

    // MARK: Properties

    weak var view: PermissionGPSModuleViewProtocol?
    var router: PermissionGPSModuleRouter!
    private var tapedStart = false
    
    init(with view: PermissionGPSModuleViewProtocol, router: PermissionGPSModuleRouter) {
        self.view = view
        self.router = router
        LocationService.shared.delegate = self
    }
    
    // MARK: Methods
    func startLocationService() {
        tapedStart = true
        print("startLocationService")
        let state = LocationService.shared.start()
        if state == .notDetermined {
            LocationService.shared.showPermission()
        } else if state == .denied {
            self.router?.showPermissionPushView()
        } else {
            self.router?.showPermissionPushView()
        }
    }
    
    
    func tapMainButton () {
        startLocationService()
    }
}

extension PermissionGPSModulePresenter: LocationServiceDelegate {
    func locationManager(didChangeAuthorization status: CLAuthorizationStatus) {
        print("locationManager>status>",status.rawValue)
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            if tapedStart {
                self.router?.showPermissionPushView()
            }
        default:
            break
        }
    }
}
