//
//  MainModulePresenter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 14/05/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth


protocol MainModuleViewProtocol: AnyObject {

}

protocol MainModulePresenterProtocol: AnyObject {
    func tapNewOrder()
    func tapNewTravel()
}

final class MainModulePresenter: MainModulePresenterProtocol {

    // MARK: Constants

    // MARK: Properties
    weak var view: MainModuleViewProtocol?
    private let router: MainModuleRouterProtocol
    
    init(with view: MainModuleViewProtocol, router: MainModuleRouterProtocol) {
        self.view = view
        self.router = router
    }
    
    // MARK: Methods
    func tapNewOrder() {
        router.showTravel(travelModelId: "V1X4rwHyCpelIQurUf5j")
        //router.showNewOrder()
    }
    func tapNewTravel() {
        let owner = AuthService.shared.uid ?? ""
        let firestoreService = FirestoreService()
        let docId = firestoreService.getNewDocId(DocumentType.travel)
        
        router.showNewTravel(delegate: self, docId: docId, owner: owner)
    }
}

extension MainModulePresenter: NewTravelDelegate {
    func didNewTravel(travelModel: TravelModel) {
        router.showTravel(travelModelId: travelModel.id)
    }
}
