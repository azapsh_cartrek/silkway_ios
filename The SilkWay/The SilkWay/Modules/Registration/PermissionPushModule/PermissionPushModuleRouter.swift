//
//  PermissionPushModuleRouter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 16/05/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation
import UIKit

protocol PermissionPushModuleRouter: class {
    func showMain()
}

final class PermissionPushModuleRouterImplementation: PermissionPushModuleRouter {
    
    // MARK: Properties
    weak var view: PermissionPushModuleViewController?
    weak var presenter: PermissionPushModulePresenter? 
    
    init(with viewController: PermissionPushModuleViewController) {
        self.view = viewController
    }
    // MARK: Internal helpers
    func showMain() {
        let vc = ModuleBulder.core()
        view?.navigationController?.pushViewController(vc, animated: true)
    }
}

