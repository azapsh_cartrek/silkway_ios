//
//  Money.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 23.02.2021.
//  Copyright © 2021 Ахмед Фокичев. All rights reserved.
//

import Foundation

class Money {
    var amount: Int
    var currency: String?
    
    init(amount: Int, currency: String?) {
        self.amount = amount
        self.currency = currency
    }
    
    init(model: MoneyModel) {
        self.amount = model.amount ?? 0
        self.currency = model.currency
    }
    
    var toJSON: [String : Any] {
        return [
            "amount": amount,
            "currency": "RUB"
        ]
    }
}
