//
//  LaunchModuleRouter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 14/05/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation
import UIKit

protocol LaunchModuleRouter: class {
    func showLoginModule()
    func showPermissionGPSModule()
    func showSMSModule()
    func showCore()
    func showFullProfileEdit()
    func profileEdit()
}

final class LaunchModuleRouterImplementation: LaunchModuleRouter {
    
    // MARK: Properties
    weak var view: LaunchModuleViewController?
    weak var presenter: LaunchModulePresenter? 
    
    init(with viewController: LaunchModuleViewController) {
        self.view = viewController
    }
    // MARK: Internal helpers
    
    func showLoginModule() {
        print("showLoginModule>")
        let vc = ModuleBulder.login()
        view?.navigationController?.pushViewController(vc, animated: true)
    }
    func showMainModule() {
        let vc = ModuleBulder.main()
        view?.navigationController?.pushViewController(vc, animated: true)
    }
    func showPermissionGPSModule() {
        let vc = ModuleBulder.permissionGPS()
        view?.navigationController?.pushViewController(vc, animated: true)
    }
    func showSMSModule() {
        let vc = ModuleBulder.smsSign(verificationID: "4444")
        view?.navigationController?.pushViewController(vc, animated: true)
    }
    func showCore() {
        let vc = ModuleBulder.core()
        view?.navigationController?.pushViewController(vc, animated: true)
    }

    func profileEdit() {
        let vc = ModuleBulder.profileEdit()
        view?.navigationController?.pushViewController(vc, animated: true)
    }
    func showFullProfileEdit() {
        let vc = ModuleBulder.profileEdit()
        ProfileService.shared.myProfile = UserProfile()
        ProfileService.shared.myProfile?.type = .individual
        vc.presenter?.userProfile = ProfileService.shared.myProfile
        vc.presenter?.newProfileMode = true
        view?.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}

