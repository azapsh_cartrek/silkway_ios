//
//  InfoPresenter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 13/02/2021.
//  Copyright © 2021 SilkWay. All rights reserved.
//

import Foundation

protocol InfoViewProtocol: class {

}

protocol InfoViewPresenter: class {
    func close()
}

final class InfoPresenter: InfoViewPresenter {

    // MARK: Constants

    // MARK: Properties

    weak var view: InfoViewProtocol?
    private let router: InfoRouter
    let title: String
    let info: String
    
    init(with view: InfoViewProtocol, router: InfoRouter, title: String, info: String) {
        self.view = view
        self.router = router
        self.title = title
        self.info = info
    }
    
    // MARK: Methods
    func close() {
        router.close()
    }
}
