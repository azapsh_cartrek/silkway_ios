//
//  MainModuleRouter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 14/05/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation
import UIKit

protocol MainModuleRouterProtocol: AnyObject {
    func showLoginModule()
    func showNewOrder()
    func showTravel(travelModelId: String)

    func showNewTravel(delegate: NewTravelDelegate?, docId: String, owner: String)
}

final class MainModuleRouter: Router, MainModuleRouterProtocol {
    
    // MARK: Properties
    weak var presenter: MainModulePresenter? 
    
    // MARK: Internal helpers
    
    func showLoginModule() {
        let vc = ModuleBulder.login()
        view?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showNewOrder() {
        let vc = ModuleBulder.newOrder()
        view?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showNewTravel(delegate: NewTravelDelegate?, docId: String, owner: String) {
        let vc = NewTravelModuleViewController()
        let router = NewTravelModuleRouter(viewController: vc)
        let travelModel = TravelModel(id: docId, owner: owner)
        let presenter = NewTravelModulePresenter(with: vc, router: router, delegate: delegate, travelModel: travelModel)
        vc.setup(presenter: presenter)
        view?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showTravel(travelModelId: String) {
        let vc = TravelModuleViewController()
        let router = TravelModuleRouter(viewController: vc)
        let presenter = TravelModulePresenter(with: vc, router: router, travelModelId: travelModelId)
        vc.setup(presenter: presenter)
        view?.navigationController?.pushViewController(vc, animated: true)
    }
}

