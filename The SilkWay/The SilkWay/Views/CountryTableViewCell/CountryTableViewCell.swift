//
//  CountryTableViewCell.swift
//  urent-ios
//
//  Created by Denis Borodavchenko on 27/12/2019.
//  Copyright © 2019 Denis Borodavchenko. All rights reserved.
//

import UIKit
import FlagKit

class CountryTableViewCell: UITableViewCell {
    // MARK: Outlets
    @IBOutlet private var countryNameLabel: UILabel! {
        didSet {
            countryNameLabel.font = UIFont.h4TitleFont()
        }
    }
    @IBOutlet private var phoneNumberLabel: UILabel! {
        didSet {
            phoneNumberLabel.font = UIFont.h4TitleFont()
        }
    }
    @IBOutlet private var countryIconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: Methods
    func configure(with country: Country) {
        countryNameLabel.text = country.name
        phoneNumberLabel.text = "+\(country.phoneCode)"
        if let image = Flag(countryCode: country.regionCode)?.image(style: .roundedRect) {
            countryIconImageView.image = image
        }
    }
}
