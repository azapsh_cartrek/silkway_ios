//
//  ReviewsView.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 01.07.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

protocol ReviewsViewViewDelegate: class {
  
}

class ReviewsView: UIView {
    weak var delegate: SelectFotoViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        guard let nib = R.nib.reviewsView.firstView(owner: self) else {
            return
        }
        addSubview(nib)
        nib.pin(to: self)
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(fotoBordViewTap(sender:)))
//        subView.isUserInteractionEnabled = true
//        subView.addGestureRecognizer(tapGesture)
    }

}
