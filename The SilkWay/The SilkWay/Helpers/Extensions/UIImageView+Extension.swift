//
//  UIImageView+Extension.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 03.07.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    func asCircle(cornerRadius: CGFloat){
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
        self.clipsToBounds = true
    }
}
