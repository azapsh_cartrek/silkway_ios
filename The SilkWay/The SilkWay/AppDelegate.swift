//
//  AppDelegate.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 14.05.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 100
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        
       // IQKeyboardManager.shared. shouldShowTextFieldPlaceholder = false
      //  IQKeyboardManager.shared.nex shouldHidePreviousNext = false
        
        FirebaseApp.configure()
        GMSServices.provideAPIKey(Config.googleMapsApiKey)
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        return true
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Registration(google firebase) failed!")
    }
}
