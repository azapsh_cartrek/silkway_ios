//
//  AddressPointModel.swift
//  The SilkWay
//
//  Created by Azapsh on 03.07.2021.
//  Copyright © 2021 Ахмед Фокичев. All rights reserved.
//

import ObjectMapper
import CoreLocation

struct PointModel: Mappable {
    
    var longitude: Double?
    var latitude: Double?
    
    init?(map: Map) {

    }
    
    mutating func mapping(map: Map) {
        longitude <- map["_longitude"]
        latitude <- map["_latitude"]
    }
}

struct LocationPointModel: Mappable {
    
    var address: String?
    var point: PointModel?
    
    init?(map: Map) {

    }

    var location: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: point?.latitude ?? 0, longitude: point?.longitude ?? 0)
    }
    
    mutating func mapping(map: Map) {
        address <- map["address"]
        point <- map["point"]
    }
}
