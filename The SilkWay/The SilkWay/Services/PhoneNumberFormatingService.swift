//
//  PhoneNumberFormatingService.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 06.06.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//
import Foundation
import PhoneNumberKit

class PhoneNumberFormatingService {
    private init() {}
    static let shared = PhoneNumberFormatingService()

    let partialFormatter = PartialFormatter()
    let phoneNumberKit = PhoneNumberKit()
    var countryCode: UInt64?
    var formattingError: Error?
    
    func setPhoneNumberFormat(phoneNumber: inout String) {
        if phoneNumber.first != "+" {
            if phoneNumber.first == "8" {
                phoneNumber.removeFirst()
                phoneNumber = "+7"
            } else {
                phoneNumber = "+" + phoneNumber
            }
        }
        do {
            let phoneFormattedString = partialFormatter.formatPartial(phoneNumber)
            phoneNumber = phoneFormattedString
            countryCode = phoneNumberKit.countryCode(for: partialFormatter.currentRegion)
            let phone = try phoneNumberKit.parse(phoneFormattedString, ignoreType: false)
            countryCode = phone.countryCode
            if phone.type == .mobile {
                formattingError = nil
            }
        } catch let ex {
            formattingError = ex
        }
    }
    
    func getStringMaskFor(region: String) -> String? {
        let phone = phoneNumberKit.getFormattedExampleNumber(forCountry: region, ofType: .mobile, withFormat: .international, withPrefix: true)
        guard let phoneInitialMask = phone?.map({ (char) -> Character in
            if char.isNumber {
                return "d"
            }
            return char
        }) else {
            return nil
        }
        let countryCode = Int(phoneNumberKit.countryCode(for: region) ?? 0)
        var phoneMask = String(phoneInitialMask).replacingOccurrences(of: " d", with: " {d").replacingOccurrences(of: "d ", with: "d} ").replacingOccurrences(of: "+d}", with: "+d").replacingOccurrences(of: "d-", with: "d}-").replacingOccurrences(of: "-d", with: "-{d")
        phoneMask.append("}")
        let countryCodeString = String("+\(countryCode)")
        let countryInMaskRange = phoneMask.range(of: "[^.[a-z]*.]", options: .regularExpression, range: nil, locale: nil)
        phoneMask.removeSubrange(countryInMaskRange!)
        phoneMask.replaceSubrange(countryInMaskRange!, with: countryCodeString)
        phoneMask = phoneMask.replacingOccurrences(of: "\(countryCodeString)dd}", with: "\(countryCodeString)").replacingOccurrences(of: "\(countryCodeString)d}", with: "\(countryCodeString)")
        return phoneMask
    }
    
    func getExampleNumberFor(region: String) -> String? {
        let phone = phoneNumberKit.getFormattedExampleNumber(forCountry: region, ofType: .mobile, withFormat: .international, withPrefix: true)
        return phone
    }
    
    func getCountryCode() -> Int {
        return Int(phoneNumberKit.countryCode(for: partialFormatter.currentRegion) ?? 0)
    }
    
    func sanitizePhoneString(returnString: String, withPlus: Bool = true) -> String {
        if withPlus {
            return returnString.replacingOccurrences(of: " ", with: "")
                .replacingOccurrences(of: "-", with: "")
                .replacingOccurrences(of: "(", with: "")
                .replacingOccurrences(of: ")", with: "")
                .replacingOccurrences(of: " ", with: "")
        }
        return returnString.replacingOccurrences(of: " ", with: "")
            .replacingOccurrences(of: "-", with: "")
            .replacingOccurrences(of: "(", with: "")
            .replacingOccurrences(of: ")", with: "")
            .replacingOccurrences(of: " ", with: "")
            .replacingOccurrences(of: "+", with: "")
    }

}
