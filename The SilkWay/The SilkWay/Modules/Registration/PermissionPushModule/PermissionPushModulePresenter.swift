//
//  PermissionPushModulePresenter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 16/05/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation
import UserNotifications

protocol PermissionPushModuleViewProtocol: class {
    
}

protocol PermissionPushModuleViewPresenter: class {
    init(with view: PermissionPushModuleViewProtocol, router: PermissionPushModuleRouter)
    func goMain()
}

final class PermissionPushModulePresenter: PermissionPushModuleViewPresenter {
    
    // MARK: Constants
    
    // MARK: Properties
    weak var view: PermissionPushModuleViewProtocol?
    var router: PermissionPushModuleRouter!
    
    init(with view: PermissionPushModuleViewProtocol, router: PermissionPushModuleRouter) {
        self.view = view
        self.router = router
    }
    
    // MARK: Methods
    func goMain() {
        self.router?.showMain()
    }
  
}
