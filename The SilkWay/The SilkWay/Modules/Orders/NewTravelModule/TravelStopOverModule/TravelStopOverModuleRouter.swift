//
//  TravelStopOverModuleRouter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 14/02/2021.
//  Copyright © 2021 SilkWay. All rights reserved.
//

import Foundation
import UIKit

protocol TravelStopOverModuleRouterProtocol: RouterProtocol {
    func showAddressSelectVC(presenterDelegate: AddrssSelectDelegate, direction: Direction)
}

final class TravelStopOverModuleRouter: Router, TravelStopOverModuleRouterProtocol {
    
    // MARK: Properties
    weak var presenter: TravelStopOverModulePresenter? 
    
    // MARK: Internal helpers
    func showAddressSelectVC(presenterDelegate: AddrssSelectDelegate, direction: Direction) {
        let vc = OrderAddrssSelectModuleViewController()
        let router = OrderAddrssSelectModuleRouterImplementation(view: vc)
        let addressService = AddressService()
        let presenter = OrderAddrssSelectModulePresenter(with: vc,
                                                         router: router,
                                                         addressService: addressService,
                                                         direction: direction)
        presenter.delegate = presenterDelegate
        vc.presenter = presenter
        view?.navigationController?.pushViewController(vc, animated: true)
    }
}

