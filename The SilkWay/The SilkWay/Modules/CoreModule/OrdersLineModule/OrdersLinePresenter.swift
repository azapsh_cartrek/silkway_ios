//
//  OrdersLinePresenter.swift
//  The SilkWay
//
//  Created by Azapsh on 06.06.2021.
//  Copyright © 2021 Ахмед Фокичев. All rights reserved.
//

import Foundation
import Firebase

protocol OrdersLineViewProtocol: AnyObject {

}

protocol OrdersLinePresenterProtocol: AnyObject {

}

final class OrdersLinePresenter: OrdersLinePresenterProtocol {

    // MARK: Constants

    // MARK: Properties
    weak var view: OrdersLineViewProtocol?
    private let router: OrdersLineRouterProtocol
    private let orderService = OrderService()
    
    init(with view: OrdersLineViewProtocol, router: OrdersLineRouterProtocol) {
        self.view = view
        self.router = router
        
//        let tripsID = "97fd0cbfa8284b98b55e323919bc853e"
//        Auth.auth().currentUser?.getIDToken(completion: { [weak self] token, error in
//            guard let token = token else { return }
//            self?.orderService.getOrders(accessToken: token, tripsID: tripsID) { success, error in
//                
//            }
//        })
//       
        
    }
    
    // MARK: Methods
}
