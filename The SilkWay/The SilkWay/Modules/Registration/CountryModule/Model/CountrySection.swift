//
//  CountrySection.swift
//  urent-ios
//
//  Created by Denis Borodavchenko on 27/12/2019.
//  Copyright © 2019 Denis Borodavchenko. All rights reserved.
//

import Foundation

struct CountrySection {
    var alphabetIndex: String
    var countries: [Country]
}
