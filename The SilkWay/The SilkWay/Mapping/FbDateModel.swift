//
//  FbDateModel.swift
//  The SilkWay
//
//  Created by Azapsh on 17.07.2021.
//  Copyright © 2021 Ахмед Фокичев. All rights reserved.
//



import ObjectMapper
import Firebase

struct FbDateModel: Mappable {
    var seconds: Int64 = 0
    var nanoseconds: Int32 = 0

    init?(map: Map) { }

    mutating func mapping(map: Map) {
        seconds <- map["_seconds"]
        nanoseconds <- map["_nanoseconds"]
    }
    
    func toDate() -> Date {
        let timestamp = Timestamp(seconds: seconds, nanoseconds: nanoseconds)
        return timestamp.dateValue()
    }
}
