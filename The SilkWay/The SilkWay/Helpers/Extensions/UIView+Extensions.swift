//
//  UIView+Extensions.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 20.05.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//
import UIKit

public extension UIView {
    
    func roundCorners(radius: CGFloat) {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = radius
    }
    
    func circularCorners() {
        self.roundCorners(radius: self.bounds.height * 0.5)
    }
    
    func pin(to view: UIView, leftOffset: CGFloat = 0, rightOffset: CGFloat = 0, topOffset: CGFloat = 0, bottomOffset: CGFloat = 0) {
        self.translatesAutoresizingMaskIntoConstraints = false
        let constraints = [
            self.leftAnchor.constraint(equalTo: view.leftAnchor, constant: leftOffset),
            self.rightAnchor.constraint(equalTo: view.rightAnchor, constant: rightOffset),
            self.topAnchor.constraint(equalTo: view.topAnchor, constant: topOffset),
            self.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: bottomOffset)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    func setBorder() {
        var borderColor =  UIColor.greyLite().cgColor
        print("setBorder")
        
        if #available(iOS 12.0, *) , traitCollection.userInterfaceStyle == .dark {
            print("userInterfaceStyle == .dark")
            borderColor = R.color.textFieldViewBorderColor()!.cgColor
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            let externalBorder = CALayer()
            let frame = self?.frame ?? CGRect()
            externalBorder.frame = CGRect(x: 0 , y: 0, width: frame.size.width , height: frame.size.height )
            
            externalBorder.borderColor = borderColor
            externalBorder.borderWidth = 1
            externalBorder.cornerRadius = 5
            self?.layer.insertSublayer(externalBorder, at: 0)
            self?.layer.masksToBounds = false
        }
    }
    
    func addLine(color: UIColor?, height:Int = 1, offset:Int = 0) {
        guard let color = color else { return }
        let line = UIView()
        let last = self.subviews.last ?? self
        self.addSubview(line)
        line.backgroundColor = color
        line.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(last.snp.bottom).offset(offset)
            make.trailing.leading.equalToSuperview()
            make.height.equalTo(height)
        }
    }
    
    func addLabel(text: String, textColor: UIColor?, font: UIFont, topOffset:Int = 0,leftOffset:Int = 0, align: NSTextAlignment = .left) -> UILabel {
        let label = UILabel()
        let last = self.subviews.last ?? self
        self.addSubview(label)
        label.textColor = textColor ?? UIColor.black
        label.font = font
        label.textAlignment = align
        label.text = text
        label.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(last.snp.bottom).offset(topOffset)
            make.trailing.equalToSuperview().offset(-leftOffset)
            make.leading.equalToSuperview().offset(leftOffset)
        }
        return label
    }
    
    func addButton(text: String, textColor: UIColor?, font: UIFont, topOffset:Int = 0,leftOffset:Int = 0, align: UIControl.ContentHorizontalAlignment = .left) -> UIButton {
        let button = UIButton()
        let last = self.subviews.last ?? self
        self.addSubview(button)
        button.setTitleColor(textColor ?? UIColor.black, for: .normal)
        button.titleLabel?.font = font
        button.contentHorizontalAlignment = align
        button.setTitle(text, for: .normal)
        button.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(last.snp.bottom).offset(topOffset)
            make.trailing.equalToSuperview().offset(-leftOffset)
            make.leading.equalToSuperview().offset(leftOffset)
        }
        return button
    }
    
    
}

//For UIStackView
extension UIView {
  func height(constant: CGFloat) {
    setConstraint(value: constant, attribute: .height)
  }
  
  func width(constant: CGFloat) {
    setConstraint(value: constant, attribute: .width)
  }
  
    private func removeConstraint(attribute: NSLayoutConstraint.Attribute) {
    constraints.forEach {
      if $0.firstAttribute == attribute {
        removeConstraint($0)
      }
    }
  }
  
    private func setConstraint(value: CGFloat, attribute: NSLayoutConstraint.Attribute) {
    removeConstraint(attribute: attribute)
    let constraint =
      NSLayoutConstraint(item: self,
                         attribute: attribute,
                         relatedBy: NSLayoutConstraint.Relation.equal,
                         toItem: nil,
                         attribute: NSLayoutConstraint.Attribute.notAnAttribute,
                         multiplier: 1,
                         constant: value)
    self.addConstraint(constraint)
  }
}
