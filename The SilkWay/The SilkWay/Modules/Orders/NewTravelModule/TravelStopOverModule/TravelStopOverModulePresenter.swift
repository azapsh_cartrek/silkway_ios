//
//  TravelStopOverModulePresenter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 14/02/2021.
//  Copyright © 2021 SilkWay. All rights reserved.
//

import Foundation

protocol StopOverModuleDelegate {
    func addStopover(waypointModel: WaypointModel) -> Bool
}

protocol TravelStopOverModuleViewProtocol: AnyObject {
    func updateView(waypointModel: WaypointModel)
}

protocol TravelStopOverModulePresenterProtocol: AnyObject {
    func close()
    func tapAddressSelect(direction: Direction)
    func changeDeliveryDateTime(date: Date)
}

final class TravelStopOverModulePresenter: TravelStopOverModulePresenterProtocol {

    // MARK: Constants

    // MARK: Properties
    weak var view: TravelStopOverModuleViewProtocol?
    let router: TravelStopOverModuleRouter
    var waypointModel: WaypointModel

    var delegate: StopOverModuleDelegate?
    
    init(with view: TravelStopOverModuleViewProtocol, router: TravelStopOverModuleRouter, delegate: StopOverModuleDelegate?, waypointModel: WaypointModel) {
        self.view = view
        self.router = router
        self.waypointModel = waypointModel
        self.delegate = delegate
    }
    
    // MARK: Methods
    func changeDeliveryDateTime(date: Date) {
        waypointModel.deliveryDateTime = date
        view?.updateView(waypointModel: waypointModel)
    }
    func tapAddressSelect(direction: Direction) {
        router.showAddressSelectVC(presenterDelegate: self, direction: direction)
    }
    
    func close() {
        router.popOver(animated: true)
    }
    
    func save() {
        guard let delegate = delegate else { return }
        if delegate.addStopover(waypointModel: waypointModel) {
            router.popOver(animated: true)
        } else {
            //TravelModule.stopOver.title
            Popup().showAlertView(title: R.string.localizable.travelModuleStopOverTitle(),
                                  message: R.string.localizable.travelModuleStopOverErrorText(),
                                  buttonTitle: R.string.localizable.ok(),
                                  icon: R.image.info())
        }
    }
}

extension TravelStopOverModulePresenter: AddrssSelectDelegate {
    func didAddress(direction: Direction, addressPoint: AddressPoint, deliveryDateTime: Date? = nil, comment: String?) {
        waypointModel.addrssPoint = addressPoint
        self.view?.updateView(waypointModel: waypointModel)
    }
}
