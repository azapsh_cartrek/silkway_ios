//
//  MyProfileModuleRouter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 13/06/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation
import UIKit

protocol MyProfileModuleRouter: class {
    func showProfileSettings(userProfile: UserProfile)
    func showPubilcProfileVC(userProfile: UserProfile)
}

final class MyProfileModuleRouterImplementation: MyProfileModuleRouter {
    
    // MARK: Properties
    weak var view: MyProfileModuleViewController?
    weak var presenter: MyProfileModulePresenter? 
    
    init(with viewController: MyProfileModuleViewController) {
        self.view = viewController
    }
    // MARK: Internal helpers
    func showPubilcProfileVC(userProfile: UserProfile) {
        let vc = ModuleBulder.publicProfileView()
        vc.publicMode = true
        vc.presenter?.userProfile = userProfile
        vc.navigationController?.setNavigationBarHidden(true, animated: true)
        view?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showProfileSettings(userProfile: UserProfile) {
        let vc = ModuleBulder.profileSettings()
        vc.presenter?.userProfile = userProfile
        view?.navigationController?.pushViewController(vc, animated: true)
        vc.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    
    
}

