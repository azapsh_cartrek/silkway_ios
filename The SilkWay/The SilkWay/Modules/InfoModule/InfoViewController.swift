//
//  InfoViewController.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 13/02/2021.
//  Copyright © 2021 SilkWay. All rights reserved.
//

import UIKit

final class InfoViewController: UIViewController {
    
    // MARK: Constants
    private enum Constants {
        static let offset: CGFloat = 20
    }
    
    // MARK: - Subviews
    private var scrollView = UIScrollView()
    private var box = UIView()
    
    private let backButton: UIButton = {
        let button = UIButton()
        button.setImage(R.image.back(), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -20, bottom: 0, right: 0)
        button.contentMode = .scaleAspectFit
        
        return button
    }()
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = .h1TitleFont()
        label.textColor = R.color.labelTextColor()
        label.numberOfLines = 0
        
        return label
    }()
    private let infoLabel: UILabel = {
        let label = UILabel()
        label.font = .subtitleFont()
        label.textColor = R.color.infoLabelTextColor()
        label.numberOfLines = 0
        
        return label
    }()
    // MARK: IBOutlets

    // MARK: Properties

    var presenter: InfoPresenter?

    // MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = presenter?.title ?? ""
        infoLabel.text = presenter?.info ?? ""
    }

    // MARK: ConfigureLayouts
    func configureLayoutBox() {
        view.addSubview(scrollView)
        scrollView.addSubview(box)
        
        box.backgroundColor = .clear
        view.backgroundColor = R.color.backgroundColor()
        
        scrollView.showsVerticalScrollIndicator = false
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        box.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.centerX.top.bottom.equalToSuperview()
        }
    }
    
    override func loadView() {
        super.loadView()
        configureLayoutBox()
        
        view.backgroundColor = R.color.backgroundColor()
        view.addSubview(backButton)
        view.addSubview(titleLabel)
        view.addSubview(infoLabel)
        
        backButton.snp.makeConstraints {
            $0.top.equalTo(box).offset(36)
            $0.width.height.equalTo(30)
            $0.left.equalTo(Constants.offset)
        }
        backButton.addTarget(self, action: #selector(tapBack), for: .touchUpInside)
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(backButton.snp.bottom).offset(16)
            $0.leading.equalTo(Constants.offset)
            $0.trailing.equalTo(-Constants.offset)
        }
        infoLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(16)
            $0.leading.equalTo(Constants.offset)
            $0.trailing.equalTo(-Constants.offset)
            $0.bottom.equalTo(box).offset(-16)
        }
        
    }
    // MARK: Methods

    // MARK: IBActions
    @objc func tapBack(_ sender: UIButton) {
        print(#function)
        presenter?.close()
    }
}

extension InfoViewController: InfoViewProtocol {
    
}
