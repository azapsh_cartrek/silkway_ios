//
//  LoginModuleRouter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 14/05/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation
import UIKit

protocol LoginModuleRouter: class {
    func showSMSSingModule(verificationID: String)
    func selectCountries()
}

final class LoginModuleRouterImplementation: LoginModuleRouter {
    
    // MARK: Properties
    weak var view: LoginModuleViewController?
    weak var presenter: LoginModulePresenter? 
    
    init(with viewController: LoginModuleViewController) {
        self.view = viewController
    }
    // MARK: Internal helpers
    func showSMSSingModule(verificationID: String) {
        print("showLoginModule>")
        let vc = ModuleBulder.smsSign(verificationID: verificationID)
        view?.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    // MARK: Internal helpers
     func selectCountries() {
        print("selectCountries>")
        let vc = ModuleBulder.countrys()
         vc.delegate = view?.presenter
         let nc = UINavigationController(rootViewController: vc)
         nc.modalPresentationStyle = .fullScreen
         view?.present(nc, animated: true, completion: nil)
     }
    
}

