//
//  UIColor.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 20.05.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    
    static func blue() -> UIColor {
        return  UIColor(rgb: 0x40A4C4)
    }
    static func blueLite() -> UIColor {
        return  UIColor(rgb: 0x8BE2FE)
    }
    static func greyLite() -> UIColor {
        return  UIColor(rgb: 0xFDDDDE6)
    }
    static func greyLite2() -> UIColor {
        return  UIColor(rgb: 0xB6BABB)
    }
    static func darkgrey() -> UIColor {
        return  UIColor(rgb: 0x787878)
    }
    static func theBlack() -> UIColor {
        return  UIColor(rgb: 0x101113)
    }
    static func whileBorder() -> UIColor {
        return  UIColor(rgb: 0x5D5E65)
    }
    static func brown() -> UIColor {
        return  UIColor(rgb: 0xAC8656)
    }
    static func blackLite() -> UIColor {
        return  UIColor(rgb: 0x17181C)
    }
    static func blackLite2() -> UIColor {
        return  UIColor(rgb: 0x313134)
    }
}

