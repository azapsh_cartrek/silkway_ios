//
//  ParcelTableCell.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 19.02.2021.
//  Copyright © 2021 Ахмед Фокичев. All rights reserved.
//

import UIKit
import SnapKit
import SDWebImage

class ParcelTableCell: UITableViewCell {

    // MARK: Constants
    private enum Constants {
        static let offset: CGFloat = 20
    }
    
    // MARK: - Subviews
    private let borderView: UIView = {
        let border = UIView()
        border.layer.cornerRadius = 10
        border.layer.borderWidth = 1
        border.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        
        return border
    }()
    
    private let nameLabel: UILabel = {
        let label = UILabel()
        label.font = .subtitleFont()
        label.textColor = R.color.labelTextColor()
        label.numberOfLines = 0
        return label
    }()
    private let priceLabel: UILabel = {
        let label = UILabel()
        label.font = .h3TitleFont()
        label.textColor = R.color.labelTextColor()
        label.numberOfLines = 0
        label.textAlignment = .right
        return label
    }()
    private let routerIcon: UIImageView = {
        let imageview = UIImageView()
        imageview.contentMode = .scaleAspectFill
        imageview.image = R.image.routeIcon()
        
        return imageview
    }()
    
    private let fromLabel: UILabel = {
        let label = UILabel()
        label.font = .typeFont()
        label.textColor = R.color.labelTextColor()
        label.numberOfLines = 0
        return label
    }()
    private let toLabel: UILabel = {
        let label = UILabel()
        label.font = .typeFont()
        label.textColor = R.color.labelTextColor()
        label.numberOfLines = 0
        return label
    }()
    
    private let fotoImageView: UIImageView = {
        let imageview = UIImageView()
        imageview.contentMode = .scaleAspectFill
        imageview.layer.cornerRadius = 10
        imageview.clipsToBounds = true
        
        return imageview
    }()
    
    private let dateTimeIcon: UIImageView = {
        let imageview = UIImageView()
        imageview.contentMode = .scaleAspectFill
        imageview.image = R.image.calendarIcon()
        
        return imageview
    }()
    
    private let dateTimeLabel: UILabel = {
        let label = UILabel()
        label.font = .typeFont()
        label.textColor = R.color.labelTextColor()
        label.numberOfLines = 0
        return label
    }()
    
    private let safeDeliveryIcon: UIImageView = {
        let imageview = UIImageView()
        imageview.contentMode = .scaleAspectFill
        imageview.image = R.image.shield()
        
        return imageview
    }()
    
    private let safeDeliveryLabel: UILabel = {
        let label = UILabel()
        label.font = .typeFont()
        label.textColor = R.color.labelTextColor()
        label.text = R.string.localizable.safeDelivery()
        label.numberOfLines = 0
        return label
    }()
    
    // MARK: Properties
    var orderModel: Order?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func setData(orderModel: Order) {
        self.backgroundColor = R.color.backgroundColor()
        loadView()
        self.orderModel = orderModel
        let price = orderModel.compensation == nil ? "" : String(orderModel.compensation!.amount) + "₽"
        priceLabel.text = price
        nameLabel.text = orderModel.product?.name
        fromLabel.text = orderModel.addressFrom?.streetAddress
        toLabel.text = orderModel.addressTo?.streetAddress
        fotoImageView.sd_setImage(with: URL(string: orderModel.product?.imageUrl ?? ""))
        
        safeDeliveryLabel.isHidden = !orderModel.isSafeDelivery
        safeDeliveryIcon.isHidden = !orderModel.isSafeDelivery
        
        print(orderModel.deliveryDateTime)
        
        if let deliveryDateTime = orderModel.deliveryDateTime {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.medium
            dateFormatter.timeStyle = DateFormatter.Style.short
            dateTimeLabel.text = dateFormatter.string(from: deliveryDateTime)
        }
        
        if let imageUrl = orderModel.product?.imageUrl {
            fotoImageView.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage())
        }
    }
    
    func loadView() {
        
        self.addSubview(borderView)
        self.addSubview(nameLabel)
        self.addSubview(priceLabel)
        self.addSubview(routerIcon)
        self.addSubview(fromLabel)
        self.addSubview(toLabel)
        self.addSubview(fotoImageView)
        self.addSubview(dateTimeIcon)
        self.addSubview(safeDeliveryIcon)
        self.addSubview(dateTimeLabel)
        self.addSubview(safeDeliveryLabel)
        
        borderView.snp.makeConstraints {
            $0.right.left.top.equalToSuperview()
            $0.bottom.equalToSuperview().offset(-Constants.offset)
        }
        
        priceLabel.snp.makeConstraints {
            $0.right.equalToSuperview().offset(-Constants.offset)
            $0.height.equalTo(24)
            $0.top.equalToSuperview().offset(Constants.offset)
        }
        nameLabel.snp.makeConstraints {
            $0.left.equalToSuperview().offset(Constants.offset)
            $0.right.equalTo(priceLabel.snp.left).offset(-4)
            $0.centerY.equalTo(priceLabel)
            $0.height.equalTo(24)
        }
     
        routerIcon.snp.makeConstraints {
            $0.left.equalToSuperview().offset(Constants.offset)
            $0.top.equalTo(nameLabel.snp.bottom).offset(12)
            $0.height.equalTo(42)
            $0.width.equalTo(8)
        }
        fromLabel.snp.makeConstraints {
            $0.top.equalTo(routerIcon.snp.top).offset(-4)
            $0.left.equalTo(routerIcon.snp.right).offset(8)
            $0.right.equalToSuperview().offset(-Constants.offset)
            $0.height.equalTo(16)
        }
        toLabel.snp.makeConstraints {
            $0.left.equalTo(routerIcon.snp.right).offset(8)
            $0.right.equalToSuperview().offset(-Constants.offset)
            $0.bottom.equalTo(routerIcon.snp.bottom).inset(-2)
            $0.height.equalTo(16)
        }
        fotoImageView.snp.makeConstraints {
            $0.top.equalTo(routerIcon.snp.bottom).offset(Constants.offset)
            $0.left.equalToSuperview().offset(Constants.offset)
            $0.height.width.equalTo(96)
        }
        dateTimeIcon.snp.makeConstraints {
            $0.left.equalTo(fotoImageView.snp.right).offset(8)
            $0.top.equalTo(fotoImageView.snp.top).offset(8)
            $0.height.width.equalTo(22)
        }
        safeDeliveryIcon.snp.makeConstraints {
            $0.left.equalTo(fotoImageView.snp.right).offset(8)
            $0.top.equalTo(dateTimeIcon.snp.bottom).offset(Constants.offset)
            $0.height.width.equalTo(22)
        }
        
        dateTimeLabel.snp.makeConstraints {
            $0.left.equalTo(dateTimeIcon.snp.right).offset(8)
            $0.right.equalToSuperview()
            $0.centerY.equalTo(dateTimeIcon)
            $0.height.equalTo(22)
        }
        safeDeliveryLabel.snp.makeConstraints {
            $0.left.equalTo(safeDeliveryIcon.snp.right).offset(8)
            $0.right.equalToSuperview()
            $0.centerY.equalTo(safeDeliveryIcon)
            $0.height.equalTo(22)
        }
    }
    
}
