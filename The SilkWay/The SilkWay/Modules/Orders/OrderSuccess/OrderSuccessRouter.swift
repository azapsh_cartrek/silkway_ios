//
//  OrderSuccessRouter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 05/01/2021.
//  Copyright © 2021 SilkWay. All rights reserved.
//

import Foundation
import UIKit

protocol OrderSuccessRouter: class {
    func goToRootView()
}

final class OrderSuccessRouterImplementation: OrderSuccessRouter {
    
    // MARK: Properties
    weak var viewController: UIViewController?
    
    init(viewController: UIViewController) {
        self.viewController = viewController
    }
    // MARK: Internal helpers

    func goToRootView() {
        viewController?.navigationController?.popToRootViewController(animated: true)
    }
}

