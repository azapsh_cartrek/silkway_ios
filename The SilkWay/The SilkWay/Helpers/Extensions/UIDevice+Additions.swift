//
//  UIDevice+Additions.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 20.05.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//

import Foundation
import UIKit

public extension UIDevice {
    enum ModelsSize {
        case small
        case medium
        case large
    }

    enum Models {
        case iPhoneSE
        case iPhone6
        case iPhone6Plus
        case iPhone6s
        case iPhone6sPlus
        case iPhone7
        case iPhone7Plus
        case iPhone8
        case iPhone8Plus
        case iPhoneX
        case iPhoneXR
        case iPhoneXS
        case iPhoneXSMax
        case iPhone11
        case iPhone11Pro
        case iPhone11ProMax
        case nonSupported
        case simulator
    }
    static let identifier: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else {
                return identifier
            }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        return identifier
    }()
    
    static let modelName: Models = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else {
                return identifier
            }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }

        func mapToDevice(identifier: String) -> Models { // swiftlint:disable:this cyclomatic_complexity
            switch identifier {
            case "iPhone5,1", "iPhone5,2":                  return .iPhoneSE
            case "iPhone5,3", "iPhone5,4":                  return .iPhoneSE
            case "iPhone6,1", "iPhone6,2":                  return .iPhoneSE
            case "iPhone7,2":                               return .iPhone6
            case "iPhone7,1":                               return .iPhone6Plus
            case "iPhone8,1":                               return .iPhone6s
            case "iPhone8,2":                               return .iPhone6sPlus
            case "iPhone9,1", "iPhone9,3":                  return .iPhone7
            case "iPhone9,2", "iPhone9,4":                  return .iPhone7Plus
            case "iPhone8,4":                               return .iPhoneSE
            case "iPhone10,1", "iPhone10,4":                return .iPhone8
            case "iPhone10,2", "iPhone10,5":                return .iPhone8Plus
            case "iPhone10,3", "iPhone10,6":                return .iPhoneX
            case "iPhone11,2":                              return .iPhoneXS
            case "iPhone11,4", "iPhone11,6":                return .iPhoneXSMax
            case "iPhone11,8":                              return .iPhoneXR
            case "iPhone12,1":                              return .iPhone11
            case "iPhone12,3":                              return .iPhone11Pro
            case "iPhone12,5":                              return .iPhone11ProMax
            case "i386", "x86_64":                          return .simulator
            default:                                        return .nonSupported
            }
        }
        return mapToDevice(identifier: identifier)
    }()
    
    var hasTapticEngine: Bool {
        switch UIDevice.modelName {
        case .iPhone7, .iPhone7Plus, .iPhone8, .iPhone8Plus,
             .iPhoneXR, .iPhone11, .iPhone11Pro, .iPhone11ProMax:
            return true
        default:
            return false
        }
    }
    
    static func modelSize() -> ModelsSize {
        switch modelName {
        case .iPhoneSE: return .small
        case .iPhone6, .iPhone6Plus, .iPhone6s, .iPhone6sPlus, .iPhone7, .iPhone7Plus, .iPhone8, .iPhone8Plus: return .medium
        case .iPhoneX, .iPhoneXR, .iPhoneXS, .iPhoneXSMax, .iPhone11, .iPhone11Pro, .iPhone11ProMax: return .large
        case .simulator, .nonSupported: return .large
        }
    }
    
}
