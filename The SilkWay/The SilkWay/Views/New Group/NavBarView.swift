//
//  NavBarView.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 30.08.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

protocol NavBarViewViewDelegate : class {
    func tapLeftButton()
    func tapRightButton()
}

final class NavBarView: UIView {
    weak var delegate: NavBarViewViewDelegate?
    // MARK: - Constants
    private enum Constants {
        static let checkmarkSize = CGSize(width: 24, height: 24)
    }
    
    // MARK: - Subviews
    let titleLabel = UILabel()
    let rightButton = UIButton()
    let leftButton = UIButton()
    
    // MARK: - Private properties
    
    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        layer.backgroundColor = UIColor.clear.cgColor
        
        addSubview(leftButton)
        addSubview(rightButton)
        addSubview(titleLabel)
    }
    
    // MARK: - Setup
    func setTitle (title: String) {
        titleLabel.text = title
    }
    func setup( leftButtonTitle: String?, rightButtonTitle: String?, leftButtonImage: UIImage?, rightButtonImage: UIImage? ) {
        
        if let leftButtonTitle = leftButtonTitle {
            leftButton.setTitle(leftButtonTitle, for: .normal)
        }
        if let leftButtonImage = leftButtonImage {
            leftButton.setImage(leftButtonImage, for: .normal)
        }
        leftButton.setTitleColor(.red, for: .normal)
        leftButton.contentHorizontalAlignment = .left
        leftButton.addTarget(self, action:#selector(self.tapLeftButton), for: .touchUpInside)
            
        leftButton.snp.makeConstraints {
            $0.left.equalToSuperview().offset(20)
            $0.top.equalToSuperview()
            $0.bottom.equalToSuperview()
            $0.width.equalTo(50)
            if leftButtonTitle == nil && leftButtonImage == nil {
                $0.width.equalTo(0)
            }
        }
        
        if let rightButtonTitle = rightButtonTitle {
            rightButton.setTitle(rightButtonTitle, for: .normal)
        }
        if let rightButtonImage = rightButtonImage {
            rightButton.setImage(rightButtonImage, for: .normal)
        }
        
        rightButton.setTitleColor(.red, for: .normal)
        rightButton.contentHorizontalAlignment = .right
        rightButton.addTarget(self, action:#selector(self.tapRightButton), for: .touchUpInside)
        
        rightButton.snp.makeConstraints {
            $0.right.equalToSuperview().offset(-20)
            $0.top.equalToSuperview()
            $0.bottom.equalToSuperview()
            $0.width.equalTo(50)
            if rightButtonTitle == nil && rightButtonImage == nil {
                $0.width.equalTo(0)
            }
        }
        
        titleLabel.snp.makeConstraints {
            $0.leading.equalTo(leftButton.snp.trailing)
            $0.right.equalTo(rightButton.snp.left)
            $0.top.bottom.equalToSuperview()
        }
        
    }
    
    
    @objc func tapLeftButton(sender: UIButton!) {
        delegate?.tapLeftButton()
    }
    @objc func tapRightButton(sender: UIButton!) {
        delegate?.tapRightButton()
    }
    
    // MARK: - Private methods
}
