//
//  MyProfileModulePresenter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 13/06/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation

protocol MyProfileModuleViewProtocol: class {
 func setUserProfile(userProfile: UserProfile, reviews: [Review])
}

protocol MyProfileModuleViewPresenter: class {
    init(with view: MyProfileModuleViewProtocol, router: MyProfileModuleRouter)
    func tapSettings()
    func tapReview( review: Review?)
}

final class MyProfileModulePresenter: MyProfileModuleViewPresenter {

    // MARK: Constants

    // MARK: Properties

    weak var view: MyProfileModuleViewProtocol?
    var router: MyProfileModuleRouter?
    var userProfile: UserProfile?
    
    init(with view: MyProfileModuleViewProtocol, router: MyProfileModuleRouter) {
        self.view = view
        self.router = router
    }
    
    // MARK: Methods
    func start() {
        if userProfile == nil {
            userProfile = ProfileService.shared.myProfile
        }
        guard let userProfile = userProfile else { return }
        
        let reviews = ReviewsService.shared.getReviews(userid: userProfile.id)
        view?.setUserProfile(userProfile: userProfile, reviews: reviews)
    }
    
    func tapSettings() {
        guard let userProfile = userProfile else { return }
        router?.showProfileSettings(userProfile: userProfile)
    }
    
    func tapReview( review: Review?) {
        if let review = review, let _userProfile = ProfileService.shared.getPublicProfile(id: review.userId) {
            router?.showPubilcProfileVC(userProfile: _userProfile)
        }
    }

}
