//
//  Parcel.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 05.01.2021.
//  Copyright © 2021 Ахмед Фокичев. All rights reserved.
//

import Foundation
import UIKit

struct Product {
    var name: String
    var image: UIImage?
    var imageUrl: String?
    var width: Double?
    var length: Double?
    var depth: Double?
    var weight: Double?
}
