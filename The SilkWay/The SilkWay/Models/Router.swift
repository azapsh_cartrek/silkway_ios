//
//  Router.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 13.02.2021.
//  Copyright © 2021 Ахмед Фокичев. All rights reserved.
//

import UIKit

protocol RouterProtocol: AnyObject {
    func dismiss(animated: Bool)
    func popOver(animated: Bool)
    func showInfoScreen(title: String, info: String)
}

class Router {
    
    weak var view: UIViewController?
    
    init(viewController: UIViewController) {
        self.view = viewController
    }
    
    func dismiss(animated: Bool = true) {
        view?.dismiss(animated: animated, completion: nil)
    }
    
    func popOver(animated: Bool = true) {
        view?.navigationController?.popViewController(animated: animated)
    }
    
    func showInfoScreen(title: String, info: String) {
        let vc = InfoViewController()
        let infoRouter = InfoRouter(viewController: vc)
        let infoPresenter = InfoPresenter(with: vc, router: infoRouter,title: title, info: info)
        vc.presenter = infoPresenter
        
        view?.navigationController?.pushViewController(vc, animated: true)
    }
}
