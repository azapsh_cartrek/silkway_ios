//
//  Order.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 01.01.2021.
//  Copyright © 2021 Ахмед Фокичев. All rights reserved.
//

import Foundation
import SwiftyJSON
import Firebase

struct Order {
    var id: String?
    var addressFrom: AddressPoint?
    var addressTo: AddressPoint?
    var deliveryDateTime: Date?
    var reward: String?
    var comment: String?
    var product: Product?
    var compensation: Money?
    var cost: Money?
    var isSafeDelivery = false
    let owner: String
}

extension Order: FirestoreDocumentProtocol {
    
    var documentType: DocumentType {
        .order
    }
    
    var documentData: [String : Any] {
        let dimensions: [Double] = [
            product?.width ?? 0.0,
            product?.length ?? 0.0,
            product?.depth ?? 0.0
        ]
        
        return [
            "id": id ?? "",
            "comment": comment ?? "",
            "compensation": compensation?.toJSON,
            "cost": cost?.toJSON,
            "date": Timestamp(date: deliveryDateTime ?? Date()),
            "dimensions": dimensions,
            "fileUrl": product?.imageUrl ?? "",
            "from": addressFrom?.toJSON ?? [],
            "to": addressTo?.toJSON ?? [],
            "name": product?.name ?? "",
            "weight": product?.weight ?? 0,
            "safeDelivery": isSafeDelivery,
            "owner": owner
        ]
    }
    
    var documentId: String? {
        id
    }
}
