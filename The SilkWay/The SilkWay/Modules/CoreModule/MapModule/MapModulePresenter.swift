//
//  MapModulePresenter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 13/06/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation

protocol MapModuleViewProtocol: class {

}

protocol MapModuleViewPresenter: class {
    init(with view: MapModuleViewProtocol, router: MapModuleRouter)
}

final class MapModulePresenter: MapModuleViewPresenter {

    // MARK: Constants

    // MARK: Properties

    weak var view: MapModuleViewProtocol?
    var router: MapModuleRouter!

    init(with view: MapModuleViewProtocol, router: MapModuleRouter) {
        self.view = view
        self.router = router
    }
    
    // MARK: Methods

}
