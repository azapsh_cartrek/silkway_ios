//
//  String+Additions.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 29.05.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//

import Foundation

extension String {
    
    static func toChar(num: Int) -> String {
        let list = ["A","B", "C","D", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "V", "W","X","Y","Z"]
        return list[num]
    }
    
    var numbers: String {
        return String(describing: filter { String($0).rangeOfCharacter(from: CharacterSet(charactersIn: "0123456789")) != nil })
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
        
        return String(data: data, encoding: .utf8)
    }
    
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
    

}

