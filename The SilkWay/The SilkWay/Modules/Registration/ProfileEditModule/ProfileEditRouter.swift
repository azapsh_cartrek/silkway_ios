//
//  RegistrationModuleRouter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 22/06/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation
import UIKit

protocol ProfileEditRouter: class {
    func showPermissionGPSView()
    func back()
    func showAddBankCard()
}

final class ProfileEditRouterImplementation: ProfileEditRouter {
    
    // MARK: Properties
    weak var view: UIViewController?
    
    init(with viewController: UIViewController) {
        self.view = viewController
    }
    // MARK: Internal helpers
    func showPermissionGPSView() {
        let vc = ModuleBulder.permissionGPS()
        view?.navigationController?.pushViewController(vc, animated: true)
    }
    func back() {
        view?.navigationController?.popViewController(animated: true)
    }
    
    func showAddBankCard() {
        
        let vc = BankCardModuleViewController()
        let router = BankCardModuleRouterImplementation(with: vc)
        let bankCardModulePresenter = BankCardModulePresenter(with: vc, router: router)
        vc.presenter = bankCardModulePresenter
        
        view?.navigationController?.pushViewController(vc, animated: true)
    }

}

