//
//  UserProfileModel.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 19.07.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//

import Foundation

class UserProfileModel {
    var id: String
    var firstName: String
    var lastName: String
    var phoneNumber: String
    var position: String
    var organizationName: String
    var organizationInnNumber: String
    var organizationOgrnNumber: String
    var userType : String
    var avatarUrl: String
    var verified: Int
    var pushToken: String
    
    init(data: [String: Any]) {
        print("init>data>", data)
         id = data["id"] as? String ?? ""
         firstName = data["firstName"] as? String ?? ""
         lastName = data["lastName"] as? String ?? ""
         phoneNumber = data["phoneNumber"] as? String ?? ""
         userType = data["userType"] as? String ?? ""
        
         position = data["position"] as? String ?? ""
         organizationName = data["organizationName"] as? String ?? ""
         organizationInnNumber = data["organizationInnNumber"] as? String ?? ""
         organizationOgrnNumber = data["organizationOgrnNumber"] as? String ?? ""
        
         pushToken = data["pushToken"] as? String ?? ""
         avatarUrl = data["avatarUrl"] as? String ?? ""
         verified = data["verified"] as? Int ?? 0
    }

}
