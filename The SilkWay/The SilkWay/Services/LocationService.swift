//
//  LocationService.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 28.05.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//

import Foundation
import CoreLocation

protocol LocationServiceDelegate: AnyObject {
    func locationManager(didChangeAuthorization status: CLAuthorizationStatus)
}

class LocationService: NSObject, CLLocationManagerDelegate {

    static let shared = LocationService()
    lazy var llcmanager: CLLocationManager = {
        let llcmanager = CLLocationManager()
        llcmanager.desiredAccuracy = kCLLocationAccuracyBest
        llcmanager.delegate = self
        return llcmanager
    }()

    var location = CLLocationCoordinate2D()
    weak var delegate: LocationServiceDelegate?
    
    override init() {
        super.init()
        self.llcmanager.startUpdatingLocation()
    }
    
    func start() -> CLAuthorizationStatus {
        print("LocationService>start")
        llcmanager.requestAlwaysAuthorization()
        llcmanager.requestWhenInUseAuthorization()
        return CLLocationManager.authorizationStatus()
    }
    
    func showPermission() {
        llcmanager.requestWhenInUseAuthorization()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let locValue = manager.location {
            if locValue.coordinate.latitude != 0 {
                location = locValue.coordinate
                NotificationCenter.default.post(name: Notification.Name("nfLocation"), object: nil, userInfo: ["location": location])
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case CLAuthorizationStatus.restricted:
            print("GPS Restricted Access to location")
        case CLAuthorizationStatus.denied:
            print("GPS User denied access to location")
        case CLAuthorizationStatus.notDetermined:
            print("GPS Status not determined")
        default:
            print("GPS Allowed to location Access")
        }
        delegate?.locationManager(didChangeAuthorization: status)
    }
    
}
