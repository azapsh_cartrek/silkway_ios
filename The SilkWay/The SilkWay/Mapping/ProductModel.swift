//
//  ProductModel.swift
//  The SilkWay
//
//  Created by Azapsh on 03.07.2021.
//  Copyright © 2021 Ахмед Фокичев. All rights reserved.
//

import Foundation

import ObjectMapper

//struct ProductModel: Mappable {
//    
//    var name: String?
//    var imageUrl: String?
//    var width: Int?
//    var length: Int?
//    var depth: Int?
//    var weight: Int?
//    
//    init?(map: Map) {
//
//    }
//
//    mutating func mapping(map: Map) {
//        name <- map["name"]
//        imageUrl <- map["imageUrl"]
//        width <- map["width"]
//        length <- map["length"]
//        depth <- map["depth"]
//        weight <- map["weight"]
//    }
//}
