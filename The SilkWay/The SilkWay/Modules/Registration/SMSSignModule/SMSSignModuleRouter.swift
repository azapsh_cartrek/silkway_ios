//
//  SMSSignModuleRouter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 24/05/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation
import UIKit

protocol SMSSignModuleRouter: class {
    func showFullRegistrationView()
    func showPermissionGPSView()
    func back()
}

final class SMSSignModuleRouterImplementation: SMSSignModuleRouter {
    
    // MARK: Properties
    weak var view: SMSSignModuleViewController?
    weak var presenter: SMSSignModulePresenter? 
    
    init(with viewController: SMSSignModuleViewController) {
        self.view = viewController
    }
    // MARK: Internal helpers
    func showFullRegistrationView(userProfile: UserProfile) {
        let vc = ModuleBulder.profileEdit()
        
        view?.navigationController?.pushViewController(vc, animated: true)
    }
    func back() {
        view?.navigationController?.popViewController(animated: true)
    }
    func showFullRegistrationView() {
        let vc = ModuleBulder.profileEdit()
        ProfileService.shared.myProfile = UserProfile()
        ProfileService.shared.myProfile?.type = .individual
        vc.presenter?.userProfile = ProfileService.shared.myProfile
        vc.presenter?.newProfileMode = true
        view?.navigationController?.pushViewController(vc, animated: true)
    }
     func showPermissionGPSView() {
         let vc = ModuleBulder.permissionGPS()
         view?.navigationController?.pushViewController(vc, animated: true)
     }
}

