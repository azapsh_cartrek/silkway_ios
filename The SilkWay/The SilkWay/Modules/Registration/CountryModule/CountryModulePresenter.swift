//
//  CountryModulePresenter.swift
//  urent-ios
//
//  Created by Denis Borodavchenko on 27/12/2019.
//  Copyright © 2019 Urent. All rights reserved.
//

import Foundation
import PhoneNumberKit
import FlagKit

protocol CountryModuleViewProtocol: AnyObject {
    func setList(list: [Country])
}

protocol CountryModuleViewPresenter: AnyObject {
    init(with view: CountryModuleViewProtocol, router: CountryModuleRouter)
    func filterCountries(searchString: String)
}

final class CountryModulePresenter: CountryModuleViewPresenter {
    // MARK: Properties
    weak var view: CountryModuleViewProtocol?
    var router: CountryModuleRouter!
    var phoneNumberFormatingService = PhoneNumberFormatingService.shared
    var countriesList: [Country] = []
    private var filterCountries: [Country] = []
    
    init(with view: CountryModuleViewProtocol, router: CountryModuleRouter) {
        self.view = view
        self.router = router
    }
    
    // MARK: Methods
    func filterCountries(searchString: String) {
        if searchString.isEmpty {
            filterCountries = countriesList
        } else {
            filterCountries = countriesList.filter {
                $0.name.range(of: searchString) != nil
            }
        }
        view?.setList(list: filterCountries)
    }
    
    func start() {
        let countries = Locale.isoRegionCodes.compactMap({ (regionCode) -> Country? in
            guard let phoneCode = self.phoneNumberFormatingService.phoneNumberKit.countryCode(for: regionCode),
                let name = Locale.current.localizedString(forRegionCode: regionCode) else {
                    return nil
            }
            return Country(name: name, phoneCode: Int(phoneCode), regionCode: regionCode)
        })
        
        for country in countries {
            if let image = Flag(countryCode: country.regionCode)?.image(style: .roundedRect) {
             countriesList.append(country)
            }
        }
        
        filterCountries = countriesList
        view?.setList(list: filterCountries)
    }
}
