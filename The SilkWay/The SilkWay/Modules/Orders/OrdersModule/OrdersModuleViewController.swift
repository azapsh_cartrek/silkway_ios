//
//  OrdersModuleViewController.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 13/06/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import UIKit

final class OrdersModuleViewController: UIViewController {
    // MARK: Constants

    // MARK: IBOutlets

    // MARK: Properties

    var presenter: OrdersModulePresenter!

    // MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewDidLayoutSubviews() {
        if #available(iOS 12.0, *) , traitCollection.userInterfaceStyle == .dark {
            view.backgroundColor = .theBlack()
        } else {
            view.backgroundColor = .white
        }
    }
    
    // MARK: Methods

    // MARK: IBActions

}

extension OrdersModuleViewController: OrdersModuleViewProtocol{
    
}
