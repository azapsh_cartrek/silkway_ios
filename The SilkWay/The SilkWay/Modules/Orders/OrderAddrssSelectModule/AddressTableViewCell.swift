//
//  AddressTableViewCell.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 01.01.2021.
//  Copyright © 2021 Ахмед Фокичев. All rights reserved.
//

import UIKit

class AddressTableViewCell: UITableViewCell {

    private let iconView: UIImageView = {
        let icon = UIImageView()
        icon.image = R.image.timeIcon()
        icon.contentMode = .scaleAspectFit
        
        return icon
    }()
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = R.string.localizable.newOrderModuleTitle()
        label.font = .subtitleFont()
        label.textAlignment = .left
        label.textColor = R.color.labelTextColor()
        return label
    }()
    private let subTitleLabel: UILabel = {
        let label = UILabel()
        label.font = .typeFont()
        label.textAlignment = .left
        label.textColor = R.color.labelTextColor()?.withAlphaComponent(0.8)
        return label
    }()
    
    var address: AddressPoint?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configure(address: AddressPoint) {
        self.backgroundColor = R.color.backgroundColor()
        self.address = address
        
        self.addSubview(titleLabel)
        self.addSubview(subTitleLabel)
        self.addSubview(iconView)
        titleLabel.text = address.streetAddress
        subTitleLabel.text = address.stateAddress
        
        iconView.snp.makeConstraints {
            if address.isCache {
              $0.width.equalTo(30)
            } else {
              $0.width.equalTo(0)
            }
            $0.height.equalToSuperview().offset(-2)
            $0.centerY.equalToSuperview()
            $0.leading.equalToSuperview()
        }
        titleLabel.snp.makeConstraints {
            $0.top.equalToSuperview().offset(16)
            $0.leading.equalTo(iconView.snp.trailing).offset(16)
            $0.height.equalTo(20)
            $0.right.equalToSuperview()
        }
        subTitleLabel.snp.makeConstraints {
            $0.bottom.equalToSuperview().offset(-16)
            $0.leading.equalTo(iconView.snp.trailing).offset(16)
            $0.height.equalTo(16)
            $0.right.equalToSuperview()
        }
        
    }
}
