//
//  TextFieldView.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 14.06.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//
import UIKit
import Foundation
import JMMaskTextField_Swift

protocol TextFieldViewDelegate: AnyObject {
    func tapPrimaryAction(id: String, text: String)
    func tapLeftButtonAction(action: Action)
    func tapRightButtonAction(action: Action)
}

extension TextFieldViewDelegate {
    func tapLeftButtonAction(action: Action) {}
    func tapRightButtonAction(action: Action) {}
}

class TextFieldView: UIView {
    weak var delegate: TextFieldViewDelegate?
    
    @IBOutlet weak var widthLeftButton: NSLayoutConstraint!
    @IBOutlet weak var widthrightButton: NSLayoutConstraint!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet weak var widthTitleLabelCostraint: NSLayoutConstraint!
    @IBOutlet weak var maskTextfield: JMMaskTextField!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    
    var noValidColor = UIColor.red
    var text: String {
        get {
            let result = maskTextfield.text ?? ""
            return result
        }
        set (value){
            if maskTextfield != nil {
                maskTextfield.text = value
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
                    self?.update()
                }
            }
        }
    }
    var isEnable: Bool {
        get {
            let state = maskTextfield == nil ? false : maskTextfield.isEnabled
            return state
        }
        set (value){
            if maskTextfield != nil {
                maskTextfield.isEnabled = value
            }
        }
    }
    var id = ""
    private var placeholder = ""
    var leftButtonAction: Action?
    var rigthButtonAction: Action?
    
    var upFirstChar = true
    // MARK: Methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        guard let nib = R.nib.textFieldView.firstView(owner: self) else {
            return
        }
        addSubview(nib)
        nib.pin(to: self)
        maskTextfield.delegate = self
        
    }
    
    @IBAction func didTextChanged(_ sender: Any) {
        if let text = maskTextfield.text {
            self.text = text
        }
    }
    
    func update() {
        print(#function)
        borderView.layer.borderColor = R.color.textFieldViewBorderColor()?.cgColor
        borderView.layer.borderWidth = 1
        borderView.layer.cornerRadius = 5
        borderView.backgroundColor = R.color.backgroundColor()
        titleLabel.text = placeholder
        
        if text.isEmpty {
            titleLabel.isHidden = true
            maskTextfield.placeholder = self.self.placeholder
            if maskTextfield.isEditing {
                titleLabel.isHidden = false
                maskTextfield.placeholder = ""
            }
        } else {
            titleLabel.isHidden = false
        }
        
        if upFirstChar {
            maskTextfield.text = text.capitalizingFirstLetter()
        }
    }
    
    func checkValidation (minChars: Int? = 0, _ validCounts: [Int]? = nil, result: inout Bool ) {
        
        var validation = true
        
        if let min = minChars,  min > text.count {
            validation = false
            result = false
        }
        
        if let counts = validCounts, counts.count > 0, !counts.contains(text.count) {
            validation = false
            result = false
        }
        
        borderView.layer.borderWidth = 1
        borderView.layer.cornerRadius = 5
        borderView.layer.borderColor = validation ? R.color.textFieldViewBorderColor()?.cgColor : R.color.textFieldViewBorderColorNoValid()?.cgColor
    }
    
    private func setTitle(title: String) {
        titleLabel.text = "  " + title + "  "
        titleLabel.textColor = R.color.textViewViewColorPlaceholder()
        widthTitleLabelCostraint.constant = titleLabel.intrinsicContentSize.width
    }
    
    func onLeftButtonAction(action: Action, image: UIImage?) {
        leftButtonAction = action
        leftButton.setImage(image, for: .normal)
        widthLeftButton.constant = 32
        leftButton.isHidden = false
    }
    func onRightButtonAction(action: Action, image: UIImage?) {
        print(#function,action.rawValue)
        rigthButtonAction = action
        rightButton.setImage(image, for: .normal)
        widthrightButton.constant = 32
        rightButton.isHidden = false
    }
    
    func configure(id: String, title: String, placeholder: String, maskString: String? = nil) {
        
        if leftButtonAction == nil {
            widthLeftButton.constant = 0
            leftButton.isHidden = true
        }
        if rigthButtonAction == nil {
            widthrightButton.constant = 0
            rightButton.isHidden = true
        }
        
        self.id = id
        //maskTextfield.placeholder = placeholder
        self.placeholder = placeholder
        if let mask = maskString {
          maskTextfield.maskString = mask
        }
        setTitle(title: title)
        
        titleLabel.backgroundColor = R.color.backgroundColor()
        maskTextfield.backgroundColor = R.color.backgroundColor()

        update()
    }
    func setFont(font: UIFont) {
        maskTextfield.font = font
    }
    func setKeybord(keyboardType: UIKeyboardType){
        maskTextfield.keyboardType = keyboardType
    }
    func setPrimaryKey(returnKeyType: UIReturnKeyType){
        maskTextfield.returnKeyType = returnKeyType
    }
    func setSecureText(_ secure: Bool){
        maskTextfield.isSecureTextEntry = secure
    }
    func showKeybord() {
        maskTextfield.becomeFirstResponder()
    }
    @IBAction func tapRightButton(_ sender: Any) {
        guard let action = rigthButtonAction else { return }
        delegate?.tapRightButtonAction(action: action)
    }
    @IBAction func tapLeftButton(_ sender: Any) {
        guard let action = leftButtonAction else { return }
        delegate?.tapLeftButtonAction(action: action)
    }
}

extension TextFieldView : UITextFieldDelegate {
    
    // UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        update()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        update()
    }
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        textField.resignFirstResponder()
        update()
        delegate?.tapPrimaryAction(id: id, text: text)

        return true
    }
    
}
