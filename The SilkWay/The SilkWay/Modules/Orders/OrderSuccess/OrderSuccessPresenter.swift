//
//  OrderSuccessPresenter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 05/01/2021.
//  Copyright © 2021 SilkWay. All rights reserved.
//

import Foundation

protocol OrderSuccessViewProtocol: class {

}

protocol OrderSuccessViewPresenter: class {
    func close()
}

final class OrderSuccessPresenter: OrderSuccessViewPresenter {

    // MARK: Constants

    // MARK: Properties

    weak var view: OrderSuccessViewProtocol?
    var router: OrderSuccessRouter!

    init(with view: OrderSuccessViewProtocol, router: OrderSuccessRouter) {
        self.view = view
        self.router = router
    }
    
    // MARK: Methods
    func close() {
        router?.goToRootView()
    }
}
