//
//  CheckBoxButton.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 20.09.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//

import Foundation
import UIKit

protocol CheckBoxButtonDelegate {
    func isChange(state: Bool)
}

class CheckBoxButton: UIButton {
    private var isOn = false
    var delegate: CheckBoxButtonDelegate?
    
    func isChecked() -> Bool {
        return isOn
    }
    
    func setChecked( _ value: Bool) {
        isOn = value
        delegate?.isChange(state: isOn)
        if isOn {
            self.setImage(R.image.checkboxOn(), for: .normal)
        } else {
            self.setImage(R.image.checkboxOff(), for: .normal)
        }
    }
    
    func tap() {
        setChecked(!isOn)
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        tap()
    }
    
    func checkValidation(needChecked: Bool, result: inout Bool) {
        if needChecked, isChecked() == false {
            result = false
        }
    }
}
