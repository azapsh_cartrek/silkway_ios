//
//  Popup.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 28.05.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//

import Foundation
import SwiftMessages

class Popup {
    
    func showAlertView(title: String, message: String, buttonTitle: String, icon: UIImage? = nil, _ isHidden: Bool = true, onReject: (()->())? = nil) {
        
        if let view = AlertView.instance() {
            
            if let image = icon {
                view.topConstraint.constant = 77
                view.iconTop.image = image
                view.closeButton.isHidden = isHidden
            } else {
                view.topConstraint.constant = 25
                view.closeButton.isHidden = true
            }
            
            view.headerLabel.text = title
            view.subtitleLabel.text = message
            view.blueButton.setTitle(buttonTitle, for: .normal)
            view.blueButton.addTarget(self, action: #selector(Popup.close(_:)), for: .touchUpInside)
            
            var config = SwiftMessages.defaultConfig
            
            config.duration = .forever
            config.presentationStyle = .center
            config.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
            config.dimMode = .color(color: .clear, interactive: true)
            config.interactiveHide = false
            config.eventListeners.append() { event in
                if case .didHide = event {
                    onReject?()
                }
            }
            
            view.blueButton.setStyle(style: .blue)
            
            SwiftMessages.show(config: config, view: view)
        }
    }
    
    @objc class func close(_ sender : Any) {
        SwiftMessages.hide()
    }
}
