//
//  InfoRouter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 13/02/2021.
//  Copyright © 2021 SilkWay. All rights reserved.
//

import Foundation
import UIKit

protocol InfoRouterProtocol: class {
    func close()
}

final class InfoRouter: InfoRouterProtocol {
    
    // MARK: Properties
    weak var view: UIViewController?
    weak var presenter: InfoPresenter? 
    
    init(viewController: UIViewController) {
        self.view = viewController
    }
    // MARK: Internal helpers
    func close() {
        view?.navigationController?.popViewController(animated: true)
    }
}

