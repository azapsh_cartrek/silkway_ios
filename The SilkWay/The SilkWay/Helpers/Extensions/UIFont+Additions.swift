//
//  UIFont+Additions.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 20.05.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//

import Foundation
import UIKit
extension UIFont {
    static func h1TitleFont() -> UIFont {
        switch UIDevice.modelName {
        case .iPhone11, .iPhone11Pro, .iPhone11ProMax, .iPhoneXR, .iPhoneXS, .iPhoneXSMax, .iPhoneX, .iPhone8Plus, .iPhone7Plus, .iPhone6sPlus, .iPhone6Plus:
            return systemFont(ofSize: 32, weight: .bold)
        case  .iPhoneSE:
            return systemFont(ofSize: 26, weight: .bold)
        case .simulator:
            return systemFont(ofSize: 32, weight: .bold)
        default:
            return systemFont(ofSize: 26, weight: .bold)
        }
    }
    static func h2TitleFont() -> UIFont {
        switch UIDevice.modelName {
        case .iPhone11, .iPhone11Pro, .iPhone11ProMax, .iPhoneXR, .iPhoneXS, .iPhoneXSMax, .iPhoneX, .iPhone8Plus, .iPhone7Plus, .iPhone6sPlus, .iPhone6Plus:
            return systemFont(ofSize: 22, weight: .bold)
        case  .iPhoneSE:
            return systemFont(ofSize: 20, weight: .bold)
        case .simulator:
            return systemFont(ofSize: 24, weight: .bold)
        default:
            return systemFont(ofSize: 22, weight: .bold)
        }
    }
    
    static func h3TitleFont() -> UIFont {
        switch UIDevice.modelName {
        case .iPhone11, .iPhone11Pro, .iPhone11ProMax, .iPhoneXR, .iPhoneXS, .iPhoneXSMax, .iPhoneX, .iPhone8Plus, .iPhone7Plus, .iPhone6sPlus, .iPhone6Plus:
            return systemFont(ofSize: 20, weight: .bold)
        case .iPhoneSE:
            return systemFont(ofSize: 18, weight: .bold)
        case .simulator:
            return systemFont(ofSize: 20, weight: .bold)
        default:
            return systemFont(ofSize: 18, weight: .bold)
        }
    }
    
    static func h4TitleFont() -> UIFont {
        switch UIDevice.modelName {
        case .iPhone11, .iPhone11Pro, .iPhone11ProMax, .iPhoneXR, .iPhoneXS, .iPhoneXSMax, .iPhoneX, .iPhone8Plus, .iPhone7Plus, .iPhone6sPlus, .iPhone6Plus:
            return systemFont(ofSize: 18, weight: .bold)
        case .iPhoneSE:
            return systemFont(ofSize: 16, weight: .bold)
        case .simulator:
            return systemFont(ofSize: 18, weight: .bold)
        default:
            return systemFont(ofSize: 16, weight: .bold)
        }
    }
    static func h4TitleFontSB() -> UIFont {
        switch UIDevice.modelName {
        case .iPhone11, .iPhone11Pro, .iPhone11ProMax, .iPhoneXR, .iPhoneXS, .iPhoneXSMax, .iPhoneX, .iPhone8Plus, .iPhone7Plus, .iPhone6sPlus, .iPhone6Plus:
            return systemFont(ofSize: 17, weight: .semibold)
        case .iPhoneSE:
            return systemFont(ofSize: 16, weight: .semibold)
        case .simulator:
            return systemFont(ofSize: 17, weight: .semibold)
        default:
            return systemFont(ofSize: 16, weight: .semibold)
        }
    }
    
    static func subtitleFont() -> UIFont {
        switch UIDevice.modelName {
        case .iPhone11, .iPhone11Pro, .iPhone11ProMax, .iPhoneXR, .iPhoneXS, .iPhoneXSMax, .iPhoneX, .iPhone8Plus, .iPhone7Plus, .iPhone6Plus, .iPhone6sPlus:
            return systemFont(ofSize: 17, weight: .regular)
        case .iPhoneSE:
            return systemFont(ofSize: 16, weight: .regular)
        default:
            return systemFont(ofSize: 18, weight: .regular)
        }
    }
    
    static func typeFont() -> UIFont {
        switch UIDevice.modelName {
        case .iPhone11, .iPhone11Pro, .iPhone11ProMax, .iPhoneXR, .iPhoneXS, .iPhoneXSMax, .iPhoneX, .iPhone8Plus, .iPhone7Plus, .iPhone6sPlus, .iPhone6Plus:
            return systemFont(ofSize: 16)
        case .iPhoneSE:
            return systemFont(ofSize: 14)
        case .simulator:
            return systemFont(ofSize: 16)
        default:
            return systemFont(ofSize: 15)
        }
    }
    
    static func captionFont() -> UIFont {
        switch UIDevice.modelName {
        case .iPhone11, .iPhone11Pro, .iPhone11ProMax, .iPhoneXR, .iPhoneXS, .iPhoneXSMax, .iPhoneX, .iPhone8Plus, .iPhone7Plus, .iPhone6sPlus, .iPhone6Plus:
            return systemFont(ofSize: 12)
        case .iPhoneSE:
            return systemFont(ofSize: 11)
        case .simulator:
            return systemFont(ofSize: 12)
        default:
            return systemFont(ofSize: 12)
        }
    }

}
