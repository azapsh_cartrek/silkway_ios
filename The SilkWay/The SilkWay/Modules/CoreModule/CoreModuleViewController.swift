//
//  CoreModuleViewController.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 13/06/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import UIKit

final class CoreModuleViewController: UITabBarController {
    // MARK: Constants

    // MARK: IBOutlets

    // MARK: Properties

    var presenter: CoreModulePresenter!

    // MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 12.0, *) , traitCollection.userInterfaceStyle == .dark {
            tabBar.barTintColor = .blackLite()
        } else {
            tabBar.barTintColor = .white
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    // MARK: Methods

    // MARK: IBActions

}

extension CoreModuleViewController: CoreModuleViewProtocol {
    
}
