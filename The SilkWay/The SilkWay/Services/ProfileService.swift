//
//  ProfileService.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 28.05.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//

import Foundation
import Firebase

class ProfileService {
    static let shared = ProfileService()
    var myProfile: UserProfile?
    let firestoreService = FirestoreService()
    
    var myPhoneNumber: String? {
        get {
            return UserDefaults.standard.string(forKey: #function)
        }
        set(number) {
            UserDefaults.standard.set(number, forKey: #function)
        }
    }
    
    func getMyProfle(completion: @escaping (_ success: Bool, _ error: String, _ userProfile: UserProfile?) -> Void) {
        let uid = AuthService.shared.uid ?? ""
        firestoreService.getDocument(type: .users, id: uid) { (success, error, data) in
            print("FirestoreService>getMyProfle>success", success)
            print("FirestoreService>getMyProfle>error", error)
            
            if let data = data {
            let userProfileModel = UserProfileModel(data: data)
            let userProfile = UserProfile(userProfileModel: userProfileModel)
                completion(true,"", userProfile)
            } else {
                completion(true,"no data", nil)
            }
        }
    }
    
    
    func getPublicProfile(id: String) -> UserProfile? {
        var result: UserProfile?
        
        //MOK
        if id == "2" {
            result = UserProfile()
            result?.firstName = "Серхио"
            result?.lastName = "Перес"
            result?.type = .individual
            result?.isVerified = true
            result?.avatarUrl = "https://avatars.mds.yandex.net/get-zen_doc/1368767/pub_5ba6a5204e9adf00abea0760_5ba6b3998ad01000a9323608/scale_1200"
            result?.phoneNumber = "9999999"
        }
        
        
        if id == "11" {
            result = UserProfile()
            result?.firstName = "Роман"
            result?.lastName = "Грожан"
            result?.type = .individual
            result?.isVerified = false
            result?.phoneNumber = "333333"
            result?.avatarUrl = "https://clubs1.bg/wp-content/uploads/2019/03/romain-grosjean1-1024x733.jpg"
        }
        
        if id == "16" {
            result = UserProfile()
            result?.firstName = "Леклерк"
            result?.lastName = "Шарль"
            result?.type = .individual
            result?.isVerified = false
            result?.avatarUrl = "https://auto.vercity.ru/img/formula/pilots/charles_leclerc.jpg"
            result?.phoneNumber = "16161616161"
        }
        if id == "07" {
            result = UserProfile()
            result?.firstName = "Кими"
            result?.lastName = "Райкконен"
            result?.type = .individual
            result?.isVerified = true
            result?.avatarUrl = "https://thef1.files.wordpress.com/2017/03/1490268227661-e1490296424464.jpg"
            result?.phoneNumber = "77777777"
        }
        
        if id == "22" {
            result = UserProfile()
            result?.firstName = "Дмитрий"
            result?.lastName = "Андреев"
            result?.type = .organization
            result?.organizationInnNumber = "445454545454"
            result?.organizationName = "Лондон Экспересс"
            result?.organizationOgrnNumber = "0101"
            result?.isVerified = false
            result?.type = .organization
            result?.phoneNumber = "88004005656"
        }
        
        if id == "23" {
            result = UserProfile()
            result?.organizationRepresentativeName = "Дмитрий"
            result?.organizationRepresentativeSurName = "Андреев"
            result?.type = .organization
            result?.organizationInnNumber = "101010101"
            result?.organizationName = " Экспересс"
            result?.organizationRepresentativePosition = "Шеф повор"
            result?.organizationOgrnNumber = "0101"
            result?.isVerified = true
            result?.type = .organization
            result?.phoneNumber = "88004005656"
        }
        
        if result == nil {
            result = UserProfile()
            result?.firstName = "МОК"
            result?.lastName = "МОК"
            result?.type = .organization
            result?.organizationInnNumber = "101010101"
            result?.organizationName = " МОК"
            result?.organizationOgrnNumber = "0101"
            result?.organizationRepresentativePosition = "Шеф повор"
            result?.organizationRepresentativeName = ""
            result?.isVerified = true
            result?.type = .organization
            result?.phoneNumber = "88004005656"
        }
        
      return result
    }
    
    func saveProfile(userProfile: UserProfile, imageUser: UIImage, newProfileMode: Bool, completion: @escaping (_ success: Bool, _ error: String) -> Void) {
        
        if let photoData = imageUser.compressedData(quality: 0.2) {
            let uid = AuthService.shared.uid ?? ""
            UploaderService.shared.uploadImage(name: uid, file: photoData) { [weak self] (photoUrl, error) in
                if let error = error {
                    completion(false, error.localizedDescription)
                    return
                }
                
//                let refName = "users"
//                var data = [String: Any]()
//
                
                if !newProfileMode {
                    let uid = AuthService.shared.uid ?? ""
                    self?.firestoreService.updateDocument(doc: userProfile) { (success, text) in
                        print("FirestoreService>updateDocument>AuthService.shared.uid>", uid)
                        if success {
                            self?.myProfile = userProfile
                            if let ingUrl = photoUrl {
                                self?.myProfile?.avatarUrl = ingUrl
                            }
                            completion(true,"")
                        } else {
                            completion(false, text)
                        }
                    }
                    
                }
                if newProfileMode {
                    let uid = AuthService.shared.uid
                    self?.firestoreService.newDocument(doc: userProfile) { (success, text, docID) in
                        print("FirestoreService>newDocument>newProfileMode>success>",success)
                        print("FirestoreService>newDocument>newProfileMode>text>",text)
                        print("FirestoreService>newDocument>newProfileMode>docID>",docID)
                        if success {
                            self?.myProfile = userProfile
                            if let ingUrl = photoUrl {
                                self?.myProfile?.avatarUrl = ingUrl
                            }
                            completion(true,"")
                        } else {
                            completion(false, text)
                        }
                    }
                }
                
            }
        }
    }
    
}



//
//  if let photoData = imageUser.compressedData(quality: 0.2) {
//      
//      HUDProgress.show()
//      
//      Uploader.shared.uploadImage(name: profile?.id ?? "", file: photoData) { [weak self] (photoUrl, error) in
//          guard let strongSelf = self else { return }
//          
//          if let error = error {
//              Popup.showAlertView(title: "Ошибка", message: error.localizedDescription, buttonTitle: "ОК")
//              HUDProgress.hide()
//              return
//          }
//          
//          guard let photoUrl = photoUrl else { return }
//          
//          strongSelf.profile?.avatarUrl = photoUrl
//          strongSelf.profile?.firstName = strongSelf.firstName
//          strongSelf.profile?.lastName = strongSelf.lastName
//          
//          strongSelf.updateUser()
//      }
//  }
