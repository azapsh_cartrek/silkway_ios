//
//  SelectFotoView.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 23.06.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//

import UIKit
import Foundation

protocol SelectFotoViewDelegate: class {
    func showAlert(alert: UIAlertController)
    func showCamera(imagePicker: UIImagePickerController)
    func dismiss()
    func selectImage(image: UIImage?)
}

class SelectFotoView: UIView {
    weak var delegate: SelectFotoViewDelegate?
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var fotoCameraView: UIImageView!
    @IBOutlet weak var subView: UIView!
    var selectedImage: UIImage?
    var notValidImage: UIImage?
    var imagePicker = UIImagePickerController()
    var cornerRadius: CGFloat?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        guard let nib = R.nib.selectFotoView.firstView(owner: self) else {
            return
        }
        addSubview(nib)
        nib.pin(to: self)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(fotoBordViewTap(sender:)))
        subView.isUserInteractionEnabled = true
        subView.addGestureRecognizer(tapGesture)
    }

    @objc func fotoBordViewTap(sender: UITapGestureRecognizer) {
        print(#function)
         showDialog()
    }
    func setPlaceHolderImages(img: UIImage?, mini: UIImage?){
        imageView.image = img
        fotoCameraView.image = mini
    }
    func checkValidation (result: inout Bool ) {
        if selectedImage == nil {
            result = false
            let image = notValidImage ?? R.image.addPhotoError()
            imageView.image = image
        }
    }
    
    func setImage(img: UIImage?){
        if let image = img {
            selectedImage = image.compressed(quality: 0.2)
            imageView.image = selectedImage
            imageView.layer.cornerRadius = self.cornerRadius != nil ? self.cornerRadius! : self.bounds.width/2
            imageView.clipsToBounds = true
            fotoCameraView.isHidden = false
        }
    }
    func setImageUrl(ImageUrl: String){
        imageView.sd_setImage(with: URL(string: ImageUrl), placeholderImage: R.image.addPhoto())
        imageView.layer.cornerRadius = self.cornerRadius != nil ? self.cornerRadius! : self.bounds.width/2
        imageView.clipsToBounds = true
        fotoCameraView.isHidden = false
    }
    
    func showDialog(){
        let alert = UIAlertController()
        let OKAction = UIAlertAction(title: "Выбрать из галереи", style: .default) { (result : UIAlertAction) -> Void in
            self.goGalereya()
        }
        alert.addAction(OKAction)
        
        let OKCamera = UIAlertAction(title: NSLocalizedString("Сделать фото", comment: ""), style: .default) { (result : UIAlertAction) -> Void in
            self.goCamera()
        }
        alert.addAction(OKCamera)
        
        let CancleAction = UIAlertAction(title: NSLocalizedString("Отмена", comment: ""), style: .cancel) { (result : UIAlertAction) -> Void in
        }
        
        alert.addAction(CancleAction)
        delegate?.showAlert(alert: alert)
    }
    
    func goGalereya(){
        imagePicker.delegate = self // 2
        imagePicker.sourceType = .photoLibrary
        delegate?.showCamera(imagePicker: imagePicker)
    }
    func goCamera(){
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = true
        delegate?.showCamera(imagePicker: imagePicker)
    }
}

extension SelectFotoView: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let imageFromPC = info[.originalImage] as? UIImage{
            setImage(img: imageFromPC)
            delegate?.selectImage(image: imageFromPC)
        }
        delegate?.dismiss()
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("imagePickerControllerDidCancel")
        delegate?.dismiss()
    }
}
//
//"Амир"
//id
//"XA76y0E2RYS55vJ7FlUZl6v5h0V2"
//lastName
//"К"
//organizationInnNumber
//null
//organizationName
//null
//organizationOgrnNumber
//null
//phoneNumber
//"+79289160325"
//position
//null
//pushToken
//"c646aAxpRqGS0b5mPSOJA4:APA91bHj0bX6jr4w6S32cuAH1lSRUdA2EV75hUQaCZl1XDeOz_7KDWOwbDeyT36kiuGoF1hyTfwSvgjfaQR4aKmZprbBOCcGzcNo3M9LdApzjjKKougos1DxWFWvBZwbZQqXCBk6lac1"
//userType
//"individual"
//verified
//false

