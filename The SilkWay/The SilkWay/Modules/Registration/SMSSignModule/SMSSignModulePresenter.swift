//
//  SMSSignModulePresenter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 24/05/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation
import SVProgressHUD
import Firebase

protocol SMSSignModuleViewProtocol: class {

}

protocol SMSSignModuleViewPresenter: class {
    init(with view: SMSSignModuleViewProtocol, router: SMSSignModuleRouter)
    func showFullRegistrationView()
    func tapBack()
}

final class SMSSignModulePresenter: SMSSignModuleViewPresenter {
    // MARK: Constants

    // MARK: Properties

    weak var view: SMSSignModuleViewProtocol?
    var router: SMSSignModuleRouter!
    var verificationID = ""
    
    init(with view: SMSSignModuleViewProtocol, router: SMSSignModuleRouter) {
        self.view = view
        self.router = router
    }
    
    // MARK: Methods
    func sign(verificationCode: String) {
        SVProgressHUD.show()

        AuthService.shared.singIn(verificationID: verificationID, verificationCode: verificationCode) { [weak self] (success, error) in
            print("AuthService.shared.singIn>success", success)
            print("AuthService.shared.singIn>error", error)
            self?.getUserProfile()
        }
    }
    
    func getUserProfile() {
        ProfileService.shared.getMyProfle { [weak self] (success, error, userProfile) in
            if success {
                if let _userProfile = userProfile {
                    print("userProfile= NOT nil")
                    ProfileService.shared.myProfile = _userProfile
                    SVProgressHUD.dismiss()
                    self?.router.showPermissionGPSView()
                } else {
                    print("userProfile = nil")
                    SVProgressHUD.dismiss()
                    self?.showFullRegistrationView()
                }
            } else {
                SVProgressHUD.dismiss()
                let popup = Popup()
                popup.showAlertView(title: R.string.localizable.smsSingModuleMsgError(), message: error, buttonTitle: R.string.localizable.ok())
            }
        }
    }

    func tapBack() {
        router?.back()
    }
    
    func showFullRegistrationView() {
        self.router?.showFullRegistrationView()
    }
}
