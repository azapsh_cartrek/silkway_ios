//
//  TripModel.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 28.07.2021.
//  Copyright © 2021 Ахмед Фокичев. All rights reserved.
//

import ObjectMapper
    
struct TripModel: Mappable {
    
    var id: String?
    var comment: String?
    var state: String?
    var compensation: MoneyModel?
    var volume: String?
    var createdAt: String?
    var weight: Double?
    var waypoints: [WaypointModel]?
    var owner: String?
    var deviation: Int?
    var orders = [Order]()
    
    init?(map: Map) {}

    mutating func mapping(map: Map) {
        id <- map["id"]
        comment <- map["comment"]
        state <- map["state"]
        compensation <- map["compensation"]
        volume <- map["volume"]
        createdAt <- map["createdAt"]
        weight <- map["weight"]
        waypoints <- map["waypoints"]
        deviation <- map["deviation"]
        owner <- map["owner"]
    }
}
