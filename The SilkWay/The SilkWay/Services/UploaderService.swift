//
//  UploaderService.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 28.05.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//

import Foundation
import Firebase
import FirebaseStorage
import UIKit

class UploaderService {
    
    public static let shared = UploaderService()
    
    func uploadImage(name: String, file: Data, _ completion: ((_ photoUrl: String?, _ error: Error?) -> Void)? = nil) {
        let storageRef = Storage.storage().reference().child("images/\(name).jpg")
        
        storageRef.putData(file, metadata: nil) { (metadata, error) in
            if error != nil {
                completion?(nil, error)
            }
            storageRef.downloadURL(completion: { (url, error) in
                completion?(url?.absoluteString, error)
            })
        }
    }
}
