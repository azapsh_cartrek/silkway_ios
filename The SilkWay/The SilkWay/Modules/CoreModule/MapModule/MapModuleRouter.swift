//
//  MapModuleRouter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 13/06/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation
import UIKit

protocol MapModuleRouter: class {

}

final class MapModuleRouterImplementation: MapModuleRouter {
    
    // MARK: Properties
    weak var view: MapModuleViewProtocol?
    weak var presenter: MapModulePresenter? 
    
    init(view: MapModuleViewProtocol) {
        self.view = view
    }
    // MARK: Internal helpers

}

