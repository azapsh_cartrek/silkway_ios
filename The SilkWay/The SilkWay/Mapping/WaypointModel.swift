//
//  WaypointModel.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 28.07.2021.
//  Copyright © 2021 Ахмед Фокичев. All rights reserved.
//

import Foundation
import ObjectMapper
import Firebase

struct WaypointModel: Mappable {
    var id: String = ""
    var addrssPoint: AddressPoint?
    var deliveryDateTime: Date?
    var time: String?
    var tripId: String?
    var locationPointModel: LocationPointModel?
    var sort = 0
    var trip = ""
    
    init?(map: Map) {}
    
    init() {
    }
    
    init(id: String, addrssPoint: AddressPoint, deliveryDateTime: Date?, sort: Int, tripId: String) {
    self.id = id
    self.addrssPoint = addrssPoint
    self.deliveryDateTime = deliveryDateTime
    self.sort = sort
    self.tripId = tripId
    }
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        locationPointModel <- map["location"]
        sort <- map["sort"]
        trip <- map["trip"]
        
        var _fbDate: FbDateModel?
        _fbDate <- map["date"]
        deliveryDateTime = _fbDate?.toDate()
    }
    
    var toJson: [String : Any] {
        return [
            "id": id,
            "date": Timestamp(date: deliveryDateTime ?? Date()),
            "location": addrssPoint?.toJSON,
            "sort": sort,
            "time": time,
            "trip": tripId,
        ]
    }
}

extension WaypointModel : Equatable
{
    static func == (lhs: WaypointModel, rhs: WaypointModel) -> Bool
    {
        return lhs.addrssPoint?.fullAddress == rhs.addrssPoint?.fullAddress && lhs.deliveryDateTime == rhs.deliveryDateTime
    }
}
