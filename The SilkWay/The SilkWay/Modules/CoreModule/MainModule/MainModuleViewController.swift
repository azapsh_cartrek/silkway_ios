//
//  MainModuleViewController.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 14.05.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SnapKit

class MainModuleViewController: UIViewController {

    private let logoView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = R.image.logo()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    private let mainButton: UIButton = {
        let button = UIButton()
        button.setTitle(R.string.localizable.mainModule_titleMainBotton() , for: .normal)
        button.setStyle(style: .blue)
        return button
    }()
    private let seconderyButton: UIButton = {
        let button = UIButton()
        button.setTitle(R.string.localizable.mainModule_titleSecondoryBotton() , for: .normal)
        button.setStyle(style: .standart)
        return button
    }()
    
    var presenter: MainModulePresenter?
    
    override
    func viewDidLoad() {
        super.viewDidLoad()
        
        mainButton.addTarget(self, action: #selector(mainTapped(_:)), for: .touchUpInside)
        seconderyButton.addTarget(self, action: #selector(seconderyTapped(_:)), for: .touchUpInside)
    }
    
    override
    func loadView() {
        super.loadView()
        
        if #available(iOS 12.0, *) , traitCollection.userInterfaceStyle == .dark {
            view.backgroundColor = .theBlack()
        } else {
            view.backgroundColor = .white
        }
        
        view.addSubview(logoView)
        view.addSubview(mainButton)
        view.addSubview(seconderyButton)
        
        logoView.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.width.equalTo(150)
            $0.height.equalTo(116)
            $0.bottom.equalTo(mainButton.snp.top).inset(-36)
        }
        
        mainButton.snp.makeConstraints {
            $0.centerX.centerY.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(24)
            $0.height.equalTo(50)
        }
        seconderyButton.snp.makeConstraints {
            $0.top.equalTo(mainButton.snp.bottom).offset(24)
            $0.centerX.equalToSuperview()
            $0.leading.trailing.equalToSuperview().inset(24)
            $0.height.equalTo(50)
        }
    }
    
    @IBAction func mainTapped(_ sender: UIButton) {
        presenter?.tapNewOrder()
    }
    
    @IBAction func seconderyTapped(_ sender: UIButton) {
        presenter?.tapNewTravel()
    }
    
}

extension MainModuleViewController: MainModuleViewProtocol {
    
}
