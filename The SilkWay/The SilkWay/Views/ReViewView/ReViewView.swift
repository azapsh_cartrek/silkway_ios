//
//  ProfilePreView.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 18.07.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
import SnapKit

protocol ReViewViewDelegate: class {
    func tapReview(review: Review?)
}

class ReViewView: UIView {
    
    // MARK: Constants
    private enum Constants {
        static let offset: CGFloat = 20
    }
    
    weak var delegate: ReViewViewDelegate?
    var box = UIView()
    var review: Review?
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        guard let nib = R.nib.reviewsView.firstView(owner: self) else {
            return
        }
        addSubview(nib)
        nib.pin(to: self)
        configureLayoutBox()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(viewTap(sender:)))
        box.isUserInteractionEnabled = true
        box.addGestureRecognizer(tapGesture)
    }
    
    @objc func viewTap(sender: Any) {
        delegate?.tapReview(review: review)
    }
    
    func setReview(review: Review) {
        self.review = review
        if let userProfile = ProfileService.shared.getPublicProfile(id: review.userId) {
            configureLayoutContent(review: review, userProfile: userProfile)
        }
    }
    
    func configureLayoutBox() {
        if #available(iOS 12.0, *) , traitCollection.userInterfaceStyle == .dark {
            box.backgroundColor = .theBlack()
        } else {
            box.backgroundColor = .white
        }
        self.addSubview(box)
        box.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.centerX.top.bottom.equalToSuperview()
        }
    }
    
    func configureLayoutContent(review: Review, userProfile: UserProfile) {
        for obj in box.subviews {
            obj.removeFromSuperview()
        }
        
        let avatarImageView = UIImageView()
        box.addSubview(avatarImageView)
        avatarImageView.sd_setImage(with: URL(string: userProfile.avatarUrl), placeholderImage: R.image.addPhoto())
        avatarImageView.snp.makeConstraints { (make) in
            make.top.equalTo(box)
            make.height.width.equalTo(60)
            make.left.equalTo(box)
        }
        avatarImageView.contentMode = .scaleAspectFill
        avatarImageView.asCircle(cornerRadius: 30)
        
        let nameLabel = UILabel()
        box.addSubview(nameLabel)
        nameLabel.text = userProfile.firstName + " " + userProfile.lastName
        nameLabel.font = .h4TitleFont()
        nameLabel.textAlignment = .left
        nameLabel.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(box)
            make.left.equalTo(avatarImageView.snp.right).offset(Constants.offset)
            make.height.equalTo(24)
            make.width.equalTo(box).offset(-Constants.offset * 2)
        }
        
        let descriptionLabel = UILabel()
        box.addSubview(descriptionLabel)
        descriptionLabel.text = review.description
        descriptionLabel.font = .typeFont()
        descriptionLabel.numberOfLines = 0
        descriptionLabel.textAlignment = .left
        descriptionLabel.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(nameLabel.snp.bottom).offset(8)
            make.left.equalTo(avatarImageView.snp.right).offset(Constants.offset)
            make.right.equalTo(box).offset(-Constants.offset*2)
        }
        
        let starsView = StarsView()
        box.addSubview(starsView)
        starsView.setStars(value: review.rate)
        starsView.snp.makeConstraints{ (make) -> Void in
             make.top.equalTo(descriptionLabel.snp.bottom).offset(8)
            make.left.equalTo(avatarImageView.snp.right).offset(Constants.offset)
            make.width.equalTo(box).offset(-Constants.offset * 2)
            make.bottom.equalTo(box.snp.bottom).offset(0)
        }
        
    }
    
    
    
}
