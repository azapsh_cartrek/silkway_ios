//
//  NetService.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 27.06.2021.
//  Copyright © 2021 Ахмед Фокичев. All rights reserved.
//

import Alamofire
import SwiftyJSON
import ObjectMapper

struct NetService {
    // MARK: - Singleton
    func request(url: String, method: HTTPMethod, _headers: HTTPHeaders, parameters: Parameters, encoding: ParameterEncoding, completion: @escaping (JSON?, _ status: Int, _ error: String?) -> ()) {
        
        AF.request(url, method: .post, parameters: parameters, encoding: encoding, headers: _headers)
            .responseJSON { response in
                let statusCode = response.response?.statusCode ?? 0
                let responseError = response.error?.localizedDescription
                
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    completion(json, statusCode, responseError)
                    
                case .failure(let error):
                    print("error>",error)
                    print("response>body>",response.result)
                    completion(nil, statusCode, responseError)
                }
        }
    }
}
