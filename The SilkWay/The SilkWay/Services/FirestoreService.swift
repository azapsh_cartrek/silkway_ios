//
//  FirestoreService.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 01.07.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//

import Foundation
import Firebase

protocol FirestoreDocumentProtocol {
    var documentType: DocumentType { get }
    var documentData: [String: Any] { get }
    var documentId: String? { get }
}

enum DocumentType: String {
    case users = "users"
    case order = "orders"
    case travel = "trips"
}

class FirestoreService {

    private var db = Firestore.firestore()
    
    func newDocument(doc: FirestoreDocumentProtocol, completion: @escaping (_ success: Bool, _ error: String, _ documentID: String) -> Void) {
        let ref = db.collection(doc.documentType.rawValue)
        let opportunityDocument = ref.document()
        let id = doc.documentId ?? opportunityDocument.documentID
        var documentData = doc.documentData
        documentData.updateValue(id, forKey: "id")
        
        print("newDocument>newID>",id)
        print("newDocument>newID>",documentData.description)
        
        ref.document(id).setData(documentData) { (error) in
            if let error = error {
                 completion(false, error.localizedDescription, "")
            } else {
                 completion(true,"", id )
            }
        }
    }
    
    func updateDocument(doc: FirestoreDocumentProtocol, completion: @escaping (_ success: Bool, _ error: String) -> Void) {
        print("updateDocument>documentID>", doc.documentId)
        let ref = db.collection(doc.documentType.rawValue)
        let opportunityDocument = ref.document(doc.documentId ?? "0")
        ref.addDocument(data: doc.documentData)
        opportunityDocument.setData(doc.documentData) { (error) in
            if let error = error {
                completion(false, error.localizedDescription)
            } else {
                completion(true, "")
            }
        }
    }
    
    
    func getDocument(type: DocumentType, id: String, completion: @escaping (_ success: Bool, _ error: String, _ data: [String: Any]? ) -> Void) {
        let docRef = db.collection(type.rawValue).document(id)
        print("getDocument>path>", docRef.path ,"  docRef.documentID>", docRef.documentID)
        docRef.getDocument { (document, error) in
            if let document = document, document.exists, let data = document.data() {
                completion(true, "", data)
                let dataDescription = document.data().map(String.init(describing:)) ?? "nil"
                print("Document data: \(dataDescription)")
            } else {
                completion(false, "Document does not exist", nil)
            }
        }
    }
    
    func getNewDocId(_ type: DocumentType) -> String {
        let ref = db.collection(type.rawValue)
        let opportunityDocument = ref.document()
        return opportunityDocument.documentID
    }
}
