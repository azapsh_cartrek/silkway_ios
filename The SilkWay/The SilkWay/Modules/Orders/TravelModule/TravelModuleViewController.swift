//
//  TravelModuleViewController.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 14/02/2021.
//  Copyright © 2021 SilkWay. All rights reserved.
//

import UIKit
import BetterSegmentedControl
import GoogleMaps
import FittedSheets
import SwiftyJSON

final class TravelModuleViewController: UIViewController {
    // MARK: Constants
    private enum Constants {
        static let offset: CGFloat = 20
    }
    
    // MARK: - Subviews
    private let backButton: UIButton = {
        let button = UIButton()
        button.setImage(R.image.closeIcon(), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -20, bottom: 0, right: 0)
        button.contentMode = .scaleAspectFit
        
        return button
    }()

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = .h1TitleFont()
        label.textColor = R.color.labelTextColor()
        label.text = R.string.localizable.travelTitle()
        label.numberOfLines = 0
        
        return label
    }()
    
    private let infoLabel: UILabel = {
        let label = UILabel()
        label.font = .captionFont()
        label.textColor = R.color.labelTextColor()
        label.text = R.string.localizable.travelInfo()
        label.numberOfLines = 0
        
        return label
    }()
    
    private let segmentedControl: BetterSegmentedControl = {
        let segmentedControl = BetterSegmentedControl()
        segmentedControl.setStyle(
            style: .blue,
            defaultIndex: 0,
            withTitles: [R.string.localizable.travelSegmentListTitle(),
                         R.string.localizable.travelSegmentMapTitle()])
        segmentedControl.addTarget(self, action: #selector(segmentControl(_:)), for: .valueChanged)
    
        return segmentedControl
    }()
    private let mapView: GMSMapView = {
        let map = GMSMapView()
        map.isUserInteractionEnabled = true
        map.isHidden = true
        return map
    }()
    private let tableView: UITableView = {
        let table = UITableView()
        table.showsVerticalScrollIndicator = false
        table.backgroundColor = R.color.backgroundColor()
        return table
    }()
    
    private let filterButton: UIButton = {
        let button = UIButton()
        button.setImage(R.image.filterButton(), for: .normal)
        button.backgroundColor = R.color.buttonBackgraund()
        button.layer.cornerRadius = 26
        
        return button
    }()
    // MARK: Properties
    private (set) var presenter: TravelModulePresenterProtocol?
    private var sort = SortOrders.price
    
    // MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let styleURL = Bundle.main.url(forResource: "map_style", withExtension: "json") {
            self.mapView.mapStyle = try? GMSMapStyle(contentsOfFileURL: styleURL)
        }
    }

    func setup(presenter: TravelModulePresenterProtocol) {
        self.presenter = presenter
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorColor = .clear
        filterButton.addTarget(self, action: #selector(tapFilter), for: .touchUpInside)
        backButton.addTarget(self, action: #selector(tapBack), for: .touchUpInside)
    }
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = R.color.backgroundColor()
        view.addSubview(backButton)
        view.addSubview(titleLabel)
        view.addSubview(infoLabel)
        view.addSubview(segmentedControl)
        view.addSubview(mapView)
        view.addSubview(tableView)
        view.addSubview(filterButton)
        
        backButton.snp.makeConstraints {
            $0.top.equalTo(view).offset(36)
            $0.right.equalTo(view).offset(-Constants.offset)
            $0.height.width.equalTo(40)
        }
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(backButton.snp.bottom).offset(2)
            $0.trailing.equalToSuperview().inset(Constants.offset)
            $0.leading.equalToSuperview().offset(Constants.offset)
        }
        infoLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(16)
            $0.trailing.equalToSuperview().inset(Constants.offset)
            $0.leading.equalToSuperview().offset(Constants.offset)
        }
        segmentedControl.snp.makeConstraints {
            $0.top.equalTo(infoLabel.snp.bottom).offset(16)
            $0.trailing.equalToSuperview().inset(Constants.offset)
            $0.leading.equalToSuperview().offset(Constants.offset)
            $0.height.equalTo(38)
        }
        mapView.snp.makeConstraints {
            $0.top.equalTo(segmentedControl.snp.bottom).offset(16)
            $0.trailing.equalToSuperview()
            $0.leading.equalToSuperview()
            $0.bottom.equalToSuperview().offset(-60)
        }
        tableView.snp.makeConstraints {
            $0.top.equalTo(segmentedControl.snp.bottom).offset(16)
            $0.trailing.equalToSuperview().offset(-Constants.offset)
            $0.leading.equalToSuperview().offset(Constants.offset)
            $0.bottom.equalToSuperview().offset(-60)
        }
        
        filterButton.snp.makeConstraints {
            $0.bottom.equalToSuperview().inset(86)
            $0.trailing.equalToSuperview().inset(Constants.offset)
            $0.height.width.equalTo(50)
        }
    }
    
    // MARK: Methods
  
    
    // MARK: IBActions
    @objc func tapBack(_ sender: UIButton) {
        presenter?.close()
    }
    
    @objc func tapFilter(sender: UIButton!) {
        showBottomSheet()
    }
    
    @objc func segmentControl(_ segmentedControl: BetterSegmentedControl) {
        if segmentedControl.index == 0 {
            mapView.isHidden = true
            tableView.isHidden = false
        } else {
            mapView.isHidden = false
            tableView.isHidden = true
        }
        filterButton.isHidden = tableView.isHidden
    }
    
    
    private func showBottomSheet() {
        let filterSheet = FilterSheetViewController()
        filterSheet.sort = sort
        filterSheet.deleagte = self
        let sheet = SheetViewController(controller: filterSheet, sizes: [.fixed(320)])
        sheet.dismissOnPull = true
        sheet.didDismiss = { _ in
            print("didDismiss")
        }
        
        self.present(sheet, animated: false, completion: nil)
    }
    
  
}

extension TravelModuleViewController: TravelModuleViewProtocol {
    func drowLine(linePoints: [GMSPolyline]) {
        linePoints.forEach {
            $0.map = self.mapView
        }
    }
    
    func updataTable() {
        tableView.reloadData()
    }
}

extension TravelModuleViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.tripModel?.orders.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ParcelTableCell()
        if let orderModel = presenter?.tripModel?.orders[indexPath.row] {
            cell.setData(orderModel: orderModel)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 254
    }
}

extension TravelModuleViewController: FilterSheetDelegate {
    func didSelect(sort: SortOrders) {
        self.sort = sort
        tableView.reloadData()
    }
    
    func updateMaps(markers: [MapMarkerModel]) {
        mapView.clear()
        
        markers.forEach {
            switch $0.type {
                
            case .trackPoint:
                let marker: GMSMarker = GMSMarker() // Allocating Marker
                 marker.title = $0.title // Setting title
                 marker.snippet = $0.snippet // Setting sub title
                 marker.icon = $0.icon // Marker icon
                 marker.appearAnimation = .pop // Appearing animation. default
                 marker.position = $0.location // CLLocationCoordinate2D
                DispatchQueue.main.async { [weak self] in
                    guard let mapView = self?.mapView else { return }
                   marker.map = mapView
                }
                
            case .orderPoint:
                let marker: GMSMarker = GMSMarker() // Allocating Marker
                 marker.title = $0.title // Setting title
                 marker.snippet = $0.snippet // Setting sub title
                 marker.icon = $0.icon // Marker icon
                 marker.appearAnimation = .pop // Appearing animation. default
                 marker.position = $0.location // CLLocationCoordinate2D
                DispatchQueue.main.async { [weak self] in
                    guard let mapView = self?.mapView else { return }
                   marker.map = mapView
                }
            }
        }
        
        if let last = markers.last {
            mapView.animate(toLocation: CLLocationCoordinate2D(
                                latitude: last.location.latitude,
                                longitude: last.location.longitude))
        }
    }
}


