//
//  CoreModulePresenter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 13/06/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation

protocol CoreModuleViewProtocol: class {

}

protocol CoreModuleViewPresenter: class {
    init(with view: CoreModuleViewProtocol, router: CoreModuleRouter)
}

final class CoreModulePresenter: CoreModuleViewPresenter {

    // MARK: Constants

    // MARK: Properties

    weak var view: CoreModuleViewProtocol?
    var router: CoreModuleRouter!

    init(with view: CoreModuleViewProtocol, router: CoreModuleRouter) {
        self.view = view
        self.router = router
        LocationService.shared.start()
    }
    
    // MARK: Methods
    
}
