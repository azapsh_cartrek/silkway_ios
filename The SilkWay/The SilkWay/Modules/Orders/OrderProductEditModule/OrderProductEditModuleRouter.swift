//
//  OrderProductEditModuleRouter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 19/09/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation
import UIKit

protocol OrderProductEditModuleRouterProtocol: AnyObject {
    func close ()
}

final class OrderProductEditModuleRouter: OrderProductEditModuleRouterProtocol {
    
    // MARK: Properties
    weak var view: UIViewController?
    
    init(view: UIViewController) {
        self.view = view
    }
    // MARK: Internal helpers

    func close () {
        view?.navigationController?.popViewController(animated: true)
    }
}

