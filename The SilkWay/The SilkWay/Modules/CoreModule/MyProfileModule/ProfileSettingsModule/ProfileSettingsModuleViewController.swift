//
//  ProfileSettingsModuleViewController.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 31/08/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import UIKit

final class ProfileSettingsModuleViewController: UIViewController {
    
    // MARK: Constants
    private enum Constants {
        static let offset: CGFloat = 20
    }
    
    // MARK: IBOutlets
    var scrollView = UIScrollView()
    var box = UIView()
    
    var buttonExit = UIButton()
    // MARK: Properties
    
    var presenter: ProfileSettingsModulePresenter!
    
    // MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        print("ProfileSettingsModuleViewController>viewDidLoad")
         configureLayoutBox()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("viewWillAppear>")
        presenter?.start()
    }
    
    
    override func loadView() {
        super.loadView()
    }
    
    // MARK: Methods
    func configureLayoutBox() {
        box.backgroundColor = .clear
        view.backgroundColor = R.color.backgroundColor()
        
        self.view.addSubview(scrollView)
        scrollView.addSubview(box)
        scrollView.showsVerticalScrollIndicator = false
        //scrollView.delegate = self
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        box.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.centerX.top.bottom.equalToSuperview()
        }
    }
    
    func configureLayout(userProfile: UserProfile) {
        for obj in box.subviews {
            obj.removeFromSuperview()
        }
        
        let backButton = UIButton()
        backButton.setImage(R.image.back(), for: .normal)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: -20, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(tapBack), for: .touchUpInside)
        box.addSubview(backButton)
        backButton.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(box).offset(8)
            make.left.equalTo(box).offset(Constants.offset)
            make.height.width.equalTo(40)
        }
        
        let settingButton = UIButton()
        settingButton.setImage(R.image.pencilDarkl(), for: .normal)
        settingButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -20)
        settingButton.addTarget(self, action: #selector(tapEdit), for: .touchUpInside)
        box.addSubview(settingButton)
        settingButton.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(box).offset(8)
            make.right.equalTo(box).inset(Constants.offset)
            make.height.width.equalTo(40)
        }
        
        let avatarImageView = UIImageView()
        box.addSubview(avatarImageView)
        avatarImageView.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(box).offset(64)
            make.centerX.equalTo(box)
            make.height.width.equalTo(128)
        }
        avatarImageView.sd_setImage(with: URL(string: userProfile.avatarUrl), placeholderImage: R.image.addPhoto())
        avatarImageView.contentMode = .scaleAspectFill
        avatarImageView.asCircle(cornerRadius: 64)
        
        box.addLine(color: R.color.lineColor(), offset: 4)
        
        box.addLabel(text: userProfile.fullName,
                     textColor: R.color.labelTextColor(),
                     font: .h2TitleFont(),
                     topOffset: 8,
                     leftOffset: 20,
                     align: .left)
        
        box.addLabel(text: R.string.localizable.profileSettingsModulePhoneNumber(),
                     textColor: R.color.labelTextColor(),
                     font: .subtitleFont(),
                     topOffset: 16,
                     leftOffset: 20,
                     align: .left)
        
        box.addLabel(text: R.string.localizable.profileSettingsModuleBankCardNumber(),
                     textColor: R.color.labelTextColor(),
                     font: .subtitleFont(),
                     topOffset: 16,
                     leftOffset: 20,
                     align: .left)
        
        box.addLine(color: R.color.lineColor(), offset: 8)
        
        box.addButton(text: R.string.localizable.profileSettingsModuleButtonIsVerifik(),
                      textColor: R.color.labelTextColor(),
                      font: .subtitleFont(),
                      topOffset: 8,
                      leftOffset: 20,
                      align: .left)
        
        box.addLine(color: R.color.lineColor(), offset: 8)
        
        let buttonPush = box.addButton(text: R.string.localizable.profileSettingsModuleButtonPushTitle(),
                      textColor: R.color.labelTextColor(),
                      font: .subtitleFont(),
                      topOffset: 8,
                      leftOffset: 20,
                      align: .left)
        let pushSwitch = UISwitch()
        box.addSubview(pushSwitch)
        pushSwitch.isOn = true
        pushSwitch.onTintColor = .blue()
        pushSwitch.snp.makeConstraints{
            $0.top.equalTo(buttonPush.snp.top)
            $0.bottom.equalTo(buttonPush.snp.bottom)
            $0.trailing.equalTo(box).offset(-Constants.offset)
            $0.width.equalTo(60)
        }
        
        box.addLine(color: R.color.lineColor(), offset: 8)
        
        let buttonDarkMode = box.addButton(text: R.string.localizable.profileSettingsModuleButtonDarkModeEnable(),
                      textColor: R.color.labelTextColor(),
                      font: .subtitleFont(),
                      topOffset: 8,
                      leftOffset: 20,
                      align: .left)
        
        let darkModeSwitch = UISwitch()
        box.addSubview(darkModeSwitch)
        darkModeSwitch.isOn = true
        darkModeSwitch.onTintColor = .blue()
        
        darkModeSwitch.snp.makeConstraints{
            $0.top.equalTo(buttonDarkMode.snp.top)
            $0.bottom.equalTo(buttonDarkMode.snp.bottom)
            $0.right.equalTo(buttonDarkMode.snp.right)
            $0.width.equalTo(60)
        }
        
        box.addLine(color: R.color.lineColor(), offset: 8)
        
        box.addButton(text: R.string.localizable.profileSettingsModuleButtonFAQ(),
                      textColor: R.color.labelTextColor(),
                      font: .subtitleFont(),
                      topOffset: 8,
                      leftOffset: 20,
                      align: .left)
        
        box.addLine(color: R.color.lineColor(), offset: 8)
        
        box.addButton(text: R.string.localizable.profileSettingsModuleButtonSupport(),
                      textColor: R.color.labelTextColor(),
                      font: .subtitleFont(),
                      topOffset: 8,
                      leftOffset: 20,
                      align: .left)
        
        box.addLine(color: R.color.lineColor(), offset: 8)
        
        box.addButton(text: R.string.localizable.profileSettingsModuleButtonPrivacyPolicy(),
                      textColor: R.color.labelTextColor(),
                      font: .subtitleFont(),
                      topOffset: 8,
                      leftOffset: 20,
                      align: .left)
        
        box.addLine(color: R.color.lineColor(), offset: 8)
        
        box.addButton(text: R.string.localizable.profileSettingsModuleButtonUseOfTheService(),
                      textColor: R.color.labelTextColor(),
                      font: .subtitleFont(),
                      topOffset: 8,
                      leftOffset: 20,
                      align: .left)
        
        box.addLine(color: R.color.lineColor(), offset: 8)
        
        box.addButton(text: R.string.localizable.profileSettingsModuleVersion(),
                      textColor: R.color.labelTextColor(),
                      font: .subtitleFont(),
                      topOffset: 8,
                      leftOffset: 20,
                      align: .left)
        
        box.addLine(color: R.color.lineColor(), offset: 8)
        
        buttonExit = box.addButton(text: R.string.localizable.profileSettingsModuleExit(),
                      textColor: R.color.labelTextColor(),
                      font: .subtitleFont(),
                      topOffset: 8,
                      leftOffset: 20,
                      align: .left)
        buttonExit.addTarget(self, action: #selector(self.tapButtonExit), for: .touchUpInside)
        
        let last = box.subviews.last ?? box
        last.snp.makeConstraints{ (make) -> Void in
            make.bottom.equalTo(box)
        }
    }
    
    // MARK: IBActions
    @objc func tapBack(_ sender: UIButton) {
        print(#function)
        navigationController?.popViewController(animated: true)
    }
    @objc func tapEdit(_ sender: UIButton) {
        print(#function)
        presenter?.tapProfileEdit()
    }
    @objc func tapButtonExit(sender: UIButton!) {
        presenter?.logout()
    }
    
}

extension ProfileSettingsModuleViewController: ProfileSettingsModuleViewProtocol {
    func setUserProfile(userProfile: UserProfile) {
        configureLayout(userProfile: userProfile)
    }
}

