//
//  OrderSuccessViewController.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 05/01/2021.
//  Copyright © 2021 SilkWay. All rights reserved.
//

import UIKit
import SnapKit

final class OrderSuccessViewController: UIViewController {
    
    // MARK: Constants
    private enum Constants {
        static let offset: CGFloat = 20
    }
    
    // MARK: - Subviews
    private let backButton: UIButton = {
        let button = UIButton()
        button.setImage(R.image.closeIcon(), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -20, bottom: 0, right: 0)
        button.contentMode = .scaleAspectFit
        button.isEnabled = false
        
        return button
    }()
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = R.string.localizable.orderSuccessTitle()
        label.font = .h1TitleFont()
        label.textColor = R.color.labelTextColor()
        
        return label
    }()
    private let infoLabel: UILabel = {
        let label = UILabel()
        label.text = R.string.localizable.orderSuccessInfo()
        label.font = .subtitleFont()
        label.textColor = R.color.infoLabelTextColor()
        label.numberOfLines = 0
        
        return label
    }()
    
    private let okButton: UIButton = {
        let button = UIButton()
        button.setTitle(R.string.localizable.good(), for: .normal)
        button.isEnabled = false
        
        return button
    }()
    
    // MARK: IBOutlets

    // MARK: Properties

    var presenter: OrderSuccessPresenter!

    // MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) { [ weak self] in
            self?.backButton.isEnabled = true
            self?.okButton.isEnabled = true
            
            self?.showPopupVerify()
        }
    }

    override func loadView() {
        super.loadView()
        
        view.backgroundColor = R.color.backgroundColor()
        
        view.addSubview(backButton)
        view.addSubview(titleLabel)
        view.addSubview(infoLabel)
        view.addSubview(okButton)
        
        backButton.snp.makeConstraints {
            $0.top.equalToSuperview().offset(36)
            $0.width.height.equalTo(30)
            $0.trailing.equalTo(-Constants.offset)
        }
        backButton.addTarget(self, action: #selector(tapBack), for: .touchUpInside)
        
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(backButton.snp.bottom).offset(16)
            $0.leading.equalTo(Constants.offset)
            $0.trailing.equalTo(-Constants.offset)
        }
        infoLabel.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(16)
            $0.leading.equalTo(Constants.offset)
            $0.trailing.equalTo(-Constants.offset)
        }
        okButton.snp.makeConstraints {
            $0.bottom.equalToSuperview().offset(-80)
            $0.height.equalTo(52)
            $0.leading.equalTo(Constants.offset)
            $0.trailing.equalTo(-Constants.offset)
        }
        okButton.setStyle(style: .blue)
        okButton.addTarget(self, action: #selector(tapBack), for: .touchUpInside)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    // MARK: Methods

    func showPopupVerify() {
        Popup().showAlertView(title: R.string.localizable.orderSuccessPopupTitle(),
                              message: R.string.localizable.orderSuccessPopupInfo(),
                              buttonTitle: R.string.localizable.orderSuccessPopupButton(),
                              icon: R.image.checkOn())
    }

    
    // MARK: IBActions
    @objc func tapBack(_ sender: UIButton) {
        print(#function)
        presenter?.close()
    }

}

extension OrderSuccessViewController : OrderSuccessViewProtocol {
    
}
