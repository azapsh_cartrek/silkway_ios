//
//  ModuleBuilder.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 17.05.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//

import UIKit

protocol Builder {
   static func login() -> UIViewController
}

class ModuleBulder: Builder {
    
    static func launchConfigure(view: LaunchModuleViewController)  {
        let router = LaunchModuleRouterImplementation(with: view)
        let presenter = LaunchModulePresenter(with: view, router: router)
        view.presenter = presenter
    }
    
    static func login() -> UIViewController {
        let view = LoginModuleViewController()
        let router = LoginModuleRouterImplementation(with: view)
        let presenter = LoginModulePresenter(with: view, router: router)
        view.presenter = presenter
        
        return view
    }
    
    static func countrys() -> CountryModuleViewController {
        let view = CountryModuleViewController()
        let router = CountryModuleRouterImplementation(with: view)
        let presenter = CountryModulePresenter(with: view, router: router)
        view.presenter = presenter
        
        return view
    }
    
    static func smsSign(verificationID: String) -> UIViewController {
        let view = SMSSignModuleViewController()
        let router = SMSSignModuleRouterImplementation(with: view)
        let presenter = SMSSignModulePresenter(with: view, router: router)
        presenter.verificationID = verificationID
        view.presenter = presenter
        
        return view
    }
    
//    static func bankCard() -> UIViewController {
//        let view = BankCardModuleViewController()
//        let router = BankCardModuleRouterImplementation(with: view)
//        let presenter = BankCardModulePresenter(with: view, router: router)
//        view.presenter = presenter
//        
//        return view
//    }

    static func profileEdit() -> ProfileEditViewController {
        let view = ProfileEditViewController()
        let router = ProfileEditRouterImplementation(with: view)
        let presenter = ProfileEditPresenter(with: view, router: router)
        view.setPresenter(presenter: presenter)
        
        return view
    }
    static func publicProfileView() -> MyProfileModuleViewController {
        let view = MyProfileModuleViewController()
        let router = MyProfileModuleRouterImplementation(with: view)
        let presenter = MyProfileModulePresenter(with: view, router: router)
        view.presenter = presenter
        
        return view
    }
    
    static func permissionGPS() -> UIViewController {
        let view = PermissionGPSModuleViewController()
        let router = PermissionGPSModuleRouterImplementation(with: view)
        let presenter = PermissionGPSModulePresenter(with: view, router: router)
        view.presenter = presenter
        
        return view
    }
    
    static func permissionPush() -> UIViewController {
        let view = PermissionPushModuleViewController()
        let router = PermissionPushModuleRouterImplementation(with: view)
        let presenter = PermissionPushModulePresenter(with: view, router: router)
        view.presenter = presenter
        
        return view
    }
    
    static func newOrder() -> UIViewController {
        let view = NewOrderModuleViewController()
        let router = NewOrderModuleRouter(viewController: view)
        let orderModel = Order( owner: AuthService.shared.uid ?? "")
        let presenter = NewOrderModulePresenter(with: view, router: router, orderModel: orderModel)
        view.presenter = presenter
        
        return view
    }

    static func core() -> UITabBarController {
        let tbController = CoreModuleViewController()
        let router = CoreModuleRouterImplementation(with: tbController)
        let presenter = CoreModulePresenter(with: tbController, router: router)
        tbController.presenter = presenter
    
        let main = ModuleBulder.main()
        let mainTabBarItem  = UITabBarItem(title: "", image: R.image.iconMain(), tag: 0)
        mainTabBarItem.image = R.image.iconMain()?.withRenderingMode(.alwaysOriginal)
        mainTabBarItem.selectedImage = R.image.iconMainActive()?.withRenderingMode(.alwaysOriginal)
        main.tabBarItem = mainTabBarItem
        
        let map = ModuleBulder.map()
        let mapTabBarItem  = UITabBarItem(title: "", image: R.image.iconMap(), tag: 1)
        mapTabBarItem.image = R.image.iconMap()?.withRenderingMode(.alwaysOriginal)
        mapTabBarItem.selectedImage = R.image.iconMapActive()?.withRenderingMode(.alwaysOriginal)
        map.tabBarItem = mapTabBarItem
        
        let ordersLines = ModuleBulder.ordersLine()
        let ordersTabBarItem  = UITabBarItem(title: "", image: R.image.iconNotification(), tag: 2)
        ordersTabBarItem.image = R.image.iconNotification()?.withRenderingMode(.alwaysOriginal)
        ordersTabBarItem.selectedImage = R.image.iconNotificationActive()?.withRenderingMode(.alwaysOriginal)
        ordersLines.tabBarItem = ordersTabBarItem
        
        let chat = ModuleBulder.chat()
        let chatTabBarItem  = UITabBarItem(title: "", image: R.image.iconChat(), tag: 3)
        chatTabBarItem.image = R.image.iconChat()?.withRenderingMode(.alwaysOriginal)
        chatTabBarItem.selectedImage = R.image.iconChatActive()?.withRenderingMode(.alwaysOriginal)
        chat.tabBarItem = chatTabBarItem
        
        let myProfile = ModuleBulder.myProfile()
        let myProfileTabBarItem  = UITabBarItem(title: "", image: R.image.iconMyProfile(), tag: 4)
        myProfileTabBarItem.image = R.image.iconMyProfile()?.withRenderingMode(.alwaysOriginal)
        myProfileTabBarItem.selectedImage = R.image.iconMyProfileActive()?.withRenderingMode(.alwaysOriginal)
        myProfile.tabBarItem = myProfileTabBarItem
        
        let tabBarList = [main, map, ordersLines, chat, myProfile]
        tbController.viewControllers = tabBarList
        
        return tbController
    }
    
    static func main() -> UINavigationController {
        let view = MainModuleViewController()
        let router = MainModuleRouter(viewController: view)
        let presenter = MainModulePresenter(with: view, router: router)
        view.presenter = presenter
        let nc = UINavigationController(rootViewController: view)
        nc.setNavigationBarHidden(true, animated: false)
        return nc
    }
    static func map() -> UINavigationController {
        let view = MapModuleViewController()
        let router = MapModuleRouterImplementation(view: view)
        let presenter = MapModulePresenter(with: view, router: router)
        view.presenter = presenter
        let nc = UINavigationController(rootViewController: view)
        nc.setNavigationBarHidden(true, animated: false)
        return nc
    }
    static func ordersLine() -> UINavigationController {
        let view = OrdersLineViewController()
        let router = OrdersLineRouter(viewController: view)
        let presenter = OrdersLinePresenter(with: view, router: router)
        view.presenter = presenter
        let nc = UINavigationController(rootViewController: view)
        nc.setNavigationBarHidden(true, animated: false)
        return nc
    }
    
    static func chat() -> UINavigationController {
        let view = ChatsModuleViewController()
        let router = ChatsModuleRouterImplementation(with: view)
        let presenter = ChatsModulePresenter(with: view, router: router)
        view.presenter = presenter
        let nc = UINavigationController(rootViewController: view)
        nc.setNavigationBarHidden(true, animated: false)
        return nc
    }
    static func myProfile() -> UINavigationController {
        let view = MyProfileModuleViewController()
        let router = MyProfileModuleRouterImplementation(with: view)
        let presenter = MyProfileModulePresenter(with: view, router: router)
        view.presenter = presenter
        let nc = UINavigationController(rootViewController: view)
        nc.setNavigationBarHidden(true, animated: false)
        return nc
    }
    static func profileSettings() -> ProfileSettingsModuleViewController {
        let view = ProfileSettingsModuleViewController()
        let router = ProfileSettingsModuleRouterImplementation(view)
        let presenter = ProfileSettingsModulePresenter( with: view, router: router)
        view.presenter = presenter
        return view
    }

}
