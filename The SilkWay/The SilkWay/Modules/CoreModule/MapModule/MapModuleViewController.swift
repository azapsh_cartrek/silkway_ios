//
//  MapModuleViewController.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 13/06/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import UIKit

final class MapModuleViewController: UIViewController {
    // MARK: Constants

    // MARK: IBOutlets

    // MARK: Properties

    var presenter: MapModulePresenter!

    // MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewDidLayoutSubviews() {
        view.backgroundColor = R.color.backgroundColor()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    // MARK: Methods

    // MARK: IBActions

}

extension MapModuleViewController: MapModuleViewProtocol{
    
}
