//
//  OrderProductEditModulePresenter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 19/09/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation
import UIKit

protocol OrderProductEditModuleViewProtocol: AnyObject {
    func updateView(product: Product?)
}

protocol OrderProductEditPresenterProtocol: AnyObject {
    func tapSaveProduct(product: Product)
}

final class OrderProductEditModulePresenter: OrderProductEditPresenterProtocol {

    // MARK: Constants

    // MARK: Properties

    weak var view: OrderProductEditModuleViewProtocol?
    let router: OrderProductEditModuleRouterProtocol
    weak var newOrderPresenter: NewOrderModulePresenterProtocol?
    private var orderModel: Order?
    
    init(with view: OrderProductEditModuleViewProtocol, router: OrderProductEditModuleRouterProtocol, orderModel: Order) {
        self.view = view
        self.router = router
        self.orderModel = orderModel
        view.updateView(product: orderModel.product)
    }
    
    // MARK: Methods
    func tapSaveProduct(product: Product) {
        newOrderPresenter?.orderModel.product = product
        router.close()
        newOrderPresenter?.updateView()
    }
}
