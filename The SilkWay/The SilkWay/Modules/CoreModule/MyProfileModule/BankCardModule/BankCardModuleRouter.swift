//
//  BankCardModuleRouter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 11/06/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation
import UIKit

protocol BankCardModuleRouter: class {
    func close()
}

final class BankCardModuleRouterImplementation: BankCardModuleRouter {
    
    // MARK: Properties
    weak var view: BankCardModuleViewController?
    weak var presenter: BankCardModulePresenter? 
    
    init(with viewController: BankCardModuleViewController) {
        self.view = viewController
    }
    // MARK: Internal helpers
    func close() {
        view?.navigationController?.popViewController(animated: true)
    }
}

