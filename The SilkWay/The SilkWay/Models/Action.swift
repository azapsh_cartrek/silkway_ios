//
//  Action.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 13.09.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//

import Foundation

enum Action: String {
    case to = "to"
    case from = "from"
    case product = "product"
    case selectDate = "date"
    case selectTime = "time"
    case stopOver = "stopOver"
    case deviation = "deviation"
    case none = "none"
}
