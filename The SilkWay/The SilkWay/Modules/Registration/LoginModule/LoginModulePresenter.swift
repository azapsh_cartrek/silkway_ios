//
//  LoginModulePresenter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 14/05/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation

protocol LoginModuleViewProtocol: class {
    func changeCountry(country: Country)
}

protocol LoginModuleViewPresenter: class {
    init(with view: LoginModuleViewProtocol, router: LoginModuleRouter)
    func showSingSms(verificationID: String)
    func selectCoutry()
}

final class LoginModulePresenter: LoginModuleViewPresenter {
    // MARK: Constants
    
    // MARK: Properties
    weak var view: LoginModuleViewProtocol?
    var router: LoginModuleRouter!

    init(with view: LoginModuleViewProtocol, router: LoginModuleRouter) {
        self.view = view
        self.router = router
    }
    
    // MARK: Methods
    func showSingSms(verificationID: String) {
        router?.showSMSSingModule(verificationID: verificationID)
    }
    func selectCoutry() {
        router?.selectCountries()
    }
}

extension LoginModulePresenter: CountryModuleViewControllerDelegate {
    func countryDidSelect(country: Country) {
        view?.changeCountry(country: country)
    }
}
