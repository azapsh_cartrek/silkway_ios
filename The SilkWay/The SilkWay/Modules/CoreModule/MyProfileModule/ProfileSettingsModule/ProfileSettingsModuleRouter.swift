//
//  ProfileSettingsModuleRouter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 31/08/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation
import UIKit

protocol ProfileSettingsModuleRouter: class {
 func showProfileEditVC(userProfile: UserProfile?)
}

final class ProfileSettingsModuleRouterImplementation: ProfileSettingsModuleRouter {
    
    // MARK: Properties
    weak var view: ProfileSettingsModuleViewController?
    weak var presenter: ProfileSettingsModulePresenter? 
    
    init(_ view: ProfileSettingsModuleViewController) {
        self.view = view
    }
    // MARK: Internal helpers
    func showProfileEditVC(userProfile: UserProfile?) {
        let vc = ModuleBulder.profileEdit()
        vc.presenter?.newProfileMode = false
        vc.presenter?.userProfile = userProfile
        view?.navigationController?.pushViewController(vc, animated: true)
        vc.navigationController?.setNavigationBarHidden(true, animated: true)
    }
}

