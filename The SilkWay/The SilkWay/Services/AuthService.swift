//
//  AuthService.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 24.05.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//

import Foundation
import Firebase

class AuthService {
    static let shared = AuthService()
    
    var uid: String? {
        get {
            return UserDefaults.standard.string(forKey: #function)
        }
        set(value) {
            UserDefaults.standard.set(value, forKey: #function)
        }
    }
    var fbToken: String? {
        get {
            return UserDefaults.standard.string(forKey: #function)
        }
        set(value) {
            UserDefaults.standard.set(value, forKey: #function)
        }
    }
    
    func startAuth( completion: @escaping (_ success: Bool) -> Void) {
        Auth.auth().addStateDidChangeListener { [weak self] (auth, user) in
            if user != nil {
                self?.uid = user?.uid
                completion(true)
            } else {
                completion(false)
            }
        }
        Auth.auth().addIDTokenDidChangeListener { (auth, user) in
            self.updateFBToken()
        }
    }
    
    private func updateFBToken() {
        let currentUser = Auth.auth().currentUser
        currentUser?.getIDTokenForcingRefresh(false) { [weak self] idToken, error in
            guard let idToken = idToken else { return }
            print("fbtoken:", idToken)
            self?.fbToken = idToken
        }
    }
    
    func singIn(verificationID: String, verificationCode: String, completion: @escaping (_ success: Bool, _ error: String) -> Void) {
        
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID, verificationCode: verificationCode)
        Auth.auth().signIn(with: credential) { (authResult, error) in
            if let error = error {
                completion(false, error.localizedDescription)
                print("При отправке СМС кода возникла ошибка. Попробуйте повторить попытку позже.")
                return
            }
            self.uid = authResult?.user.uid ?? ""
            print("singIn uid>",self.uid)
            completion(true, "")
        }
    }
    
    func logOut() {
        try! Auth.auth().signOut()
    }
    
}
