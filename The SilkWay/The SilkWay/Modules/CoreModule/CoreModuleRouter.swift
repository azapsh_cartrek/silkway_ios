//
//  CoreModuleRouter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 13/06/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation
import UIKit

protocol CoreModuleRouter: AnyObject {

}

final class CoreModuleRouterImplementation: CoreModuleRouter {
    
    // MARK: Properties
    weak var view: CoreModuleViewController?
    weak var presenter: CoreModulePresenter? 
    
    init(with viewController: CoreModuleViewController) {
        self.view = viewController
    }
    // MARK: Internal helpers

}

