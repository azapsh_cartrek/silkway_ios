//
//  OrderAddrssSelectModulePresenter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 19/09/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation
import MapKit

protocol OrderAddrssSelectModuleViewProtocol: AnyObject {
    func tableUpdate()
}

protocol AddrssSelectDelegate {
    func didAddress(direction: Direction, addressPoint: AddressPoint, deliveryDateTime: Date?, comment: String?)
}

protocol OrderAddrssSelectModulePresenterProtocol: AnyObject {
    var delegate: AddrssSelectDelegate? { get }
    var addresList: [AddressPoint] { get }
    var direction: Direction { get }
    func didsearchTextChanged(text: String)
    func tapMyLocation()
}

final class OrderAddrssSelectModulePresenter: OrderAddrssSelectModulePresenterProtocol {

    // MARK: Constants
    private enum Constants {
        static let radiusMeters: CLLocationDistance = 20000000
    }
    
    // MARK: Properties
    weak var view: OrderAddrssSelectModuleViewProtocol?
    private var router: OrderAddrssSelectModuleRouter!
    var direction: Direction
    var addresList = [AddressPoint]()
    var delegate: AddrssSelectDelegate?
    var throttlerUpdate = Throttler(maxInterval: 0.4)
    private let addressService: AddressServiceProtocol
    private let addressCacheService = AddressCacheService()
    
    init(with view: OrderAddrssSelectModuleViewProtocol,
         router: OrderAddrssSelectModuleRouter,
         addressService: AddressServiceProtocol,
         direction: Direction) {
        self.view = view
        self.router = router
        self.direction = direction
        self.addressService = addressService
        
        if let list = try? addressCacheService.getCacheAddressPointList() {
          addresList = list
        }
    }
    
    // MARK: Methods
    func tapMyLocation() {
        let coordinate = LocationService.shared.location
        addressService.getAddressPoint(coordinate: coordinate) { [weak self] (addressPoint, error) in
            guard let self = self else { return }
            if let addressPoint = addressPoint {
                self.selectAdderss(addressPoint: addressPoint)
            } else {
                print("tapMyLocation>error", error)
            }
        }
    }
    
    func selectAdderss(addressPoint: AddressPoint) {
        self.addressCacheService.saveAddressPoint(value: addressPoint)
        delegate?.didAddress(direction: direction, addressPoint: addressPoint, deliveryDateTime: nil, comment: "")
        router?.close()
    }
    
    func didsearchTextChanged(text: String) {
        throttlerUpdate.throttle { [weak self] in
            let coordinate = LocationService.shared.location
            self?.addressService.searchText(text: text, coordinate: coordinate, radiusMeters: Constants.radiusMeters, completion: { [weak self] (success, list, error) in
                if success {
                    self?.addresList.removeAll()
                    self?.addresList = list
                    self?.view?.tableUpdate()
                } else {
                    self?.addresList.removeAll()
                    if let list = try? self?.addressCacheService.getCacheAddressPointList() {
                        self?.addresList = list
                    }
                    
                    self?.view?.tableUpdate()
                    print("didsearchTextChanged>", error)
                }
            })
        }
    }
}
