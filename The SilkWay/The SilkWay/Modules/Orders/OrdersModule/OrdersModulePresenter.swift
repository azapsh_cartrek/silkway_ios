//
//  OrdersModulePresenter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 13/06/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation

protocol OrdersModuleViewProtocol: class {

}

protocol OrdersModuleViewPresenter: class {
    init(with view: OrdersModuleViewProtocol, router: OrdersModuleRouter)
}

final class OrdersModulePresenter: OrdersModuleViewPresenter {

    // MARK: Constants

    // MARK: Properties

    weak var view: OrdersModuleViewProtocol?
    var router: OrdersModuleRouter!

    init(with view: OrdersModuleViewProtocol, router: OrdersModuleRouter) {
        self.view = view
        self.router = router
    }
    
    // MARK: Methods
    
}
