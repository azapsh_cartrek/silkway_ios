//
//  CountryModuleRouter.swift
//  urent-ios
//
//  Created by Denis Borodavchenko on 27/12/2019.
//  Copyright © 2019 Urent. All rights reserved.
//

import Foundation
import UIKit

protocol CountryModuleRouter: class {

}

final class CountryModuleRouterImplementation: CountryModuleRouter {
    
    // MARK: Properties
    weak var view: CountryModuleViewController?
    weak var presenter: CountryModulePresenter? 
    
    init(with viewController: CountryModuleViewController) {
        self.view = viewController
    }
    // MARK: Internal helpers
}
