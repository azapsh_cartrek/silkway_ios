//
//  Travel.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 14.02.2021.
//  Copyright © 2021 Ахмед Фокичев. All rights reserved.
//

import Foundation
import Firebase

struct TravelModel {
    var id: String
    let owner: String
    var addressFrom: WaypointModel?
    var addressTo: WaypointModel?
    var waypoints = [WaypointModel]()
    var reward: String?
    var comment: String?
    var deviation: Int = 0
    var deviationText: String?
    var compensationForm = 0
    var compensationTo = 0
    var weightFrom = 0
    var weightTo = 0
    var volumeFrom = 0
    var volumeTo = 0
}

extension TravelModel: FirestoreDocumentProtocol {
    var documentType: DocumentType {
        .travel
    }
    
    var documentData: [String : Any] {

        var list = waypoints
        if let addressFrom = addressFrom {
            list.insert(addressFrom, at: 0)
        }
        if let addressTo = addressTo {
            list.append(addressTo)
        }
        
        var fbWaypoints = [WaypointModel]()
        var sort = 0
        list.forEach {
            var wpoint = $0
            wpoint.sort = sort
            wpoint.tripId = id
            fbWaypoints.append(wpoint)
            sort += 1
        }
        
        return [
            "owner": owner,
            "state": "",
            "volume": volumeToJSON,
            "comment": comment ?? "",
            "compensation": compensationJSON,
            "createdAt": Timestamp(date: Date()),
            "deviation": deviation,
            "weight": weightToJSON,
            "waypoints": fbWaypoints.compactMap{ $0.toJson },
        ]
    }
    
    var documentId: String? {
        id
    }
    
    private var compensationJSON: [String : Any] {
        return [
            "from": compensationForm,
            "to": compensationTo
        ]
    }
    
    private var weightToJSON: [String : Any] {
        return [
            "from": volumeFrom,
            "to": volumeTo
        ]
    }
    
    private var volumeToJSON: [String : Any] {
        return [
            "from": weightFrom,
            "to": weightTo
        ]
    }
}
