//
//  UserProfile.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 28.05.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//

import Foundation


enum UserType: String {
    case individual = "individual"
    case organization = "organization"
    case none = ""
}

class UserProfile {
    var id = ""
    var type = UserType.individual
    var firstName = ""
    var lastName = ""
    var rate: Double = 4.0
    var deliverysCounts = 58
    
    var phoneNumber = ""
    var organizationName = ""
    var organizationInnNumber = ""
    var organizationOgrnNumber = ""
    var organizationRepresentativeName = ""
    var organizationRepresentativeSurName = ""
    var organizationRepresentativePosition = ""
    
    var avatarUrl = ""
    var isVerified = false
    var pushToken = ""
    var photoUrl = ""
    
    var fullName: String {
        if type == .individual {
            return firstName + " " + lastName
        } else {
           return organizationName
        }
    }
    
    init() {
    
    }
    
    init(userProfileModel: UserProfileModel) {
        self.id = userProfileModel.id
        self.phoneNumber = userProfileModel.phoneNumber
        
        self.firstName = userProfileModel.firstName
        self.lastName = userProfileModel.lastName
        if let _type = UserType(rawValue: userProfileModel.userType) {
            self.type = _type
        }
        self.avatarUrl = userProfileModel.avatarUrl
        self.organizationName = userProfileModel.organizationName
        self.organizationInnNumber = userProfileModel.organizationInnNumber
        self.organizationOgrnNumber = userProfileModel.organizationOgrnNumber
        self.organizationRepresentativeName = userProfileModel.firstName
        self.organizationRepresentativeSurName = userProfileModel.lastName
        self.organizationRepresentativePosition = userProfileModel.position
        
        self.isVerified = userProfileModel.verified == 1 ? true : false
        self.pushToken = userProfileModel.pushToken
    }
    
}


//var position: String? = null


extension UserProfile: FirestoreDocumentProtocol {
    
    var documentType: DocumentType {
        return .users
    }
    
    var documentData: [String : Any] {
        
        if type == .organization {
            return [
                "id": AuthService.shared.uid ?? "",
                "userType": type.rawValue,
                "avatarUrl": photoUrl,
                "firstName": organizationRepresentativeName,
                "lastName": organizationRepresentativeSurName,
                "organizationName": organizationName,
                "organizationInnNumber": organizationInnNumber,
                "organizationOgrnNumber": organizationOgrnNumber,
                "position": organizationRepresentativePosition,
                "phoneNumber": phoneNumber,
                "isVerified": isVerified,
                "pushToken": pushToken,
                "rating" : rate
            ]
        } else {
            return [
                "id": AuthService.shared.uid ?? "",
                "userType": type.rawValue,
                "avatarUrl": photoUrl,
                "firstName": firstName,
                "lastName": lastName,
                "organizationName": "",
                "organizationInnNumber": "",
                "organizationOgrnNumber": "",
                "position": "",
                "phoneNumber": phoneNumber,
                "isVerified": isVerified,
                "pushToken": pushToken,
                "rating" : rate
            ]
        }
        
    }
    
    var documentId: String? {
        return id
    }
}
