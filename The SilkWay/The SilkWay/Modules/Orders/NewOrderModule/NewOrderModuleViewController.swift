//
//  NewOrderModuleViewController.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 12/09/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import UIKit
import SnapKit
import CoreLocation

final class NewOrderModuleViewController: UIViewController {
    
    // MARK: Constants
    private enum Constants {
        static let offset: CGFloat = 20
    }
    // MARK: IBOutlets

    // MARK: Properties
    var scrollView = UIScrollView()
    var box = UIView()
    
    // MARK: - Subviews
    private let backButton: UIButton = {
        let button = UIButton()
        button.setImage(R.image.back(), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -20, bottom: 0, right: 0)
        button.contentMode = .scaleAspectFit
        
        return button
    }()
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.text = R.string.localizable.newOrderModuleTitle()
        label.font = .h2TitleFont()
        label.textAlignment = .left
        return label
    }()
    private let productButtonView: ButtonView = {
        let buttonView = ButtonView()
        buttonView.configure(action: Action.product)
        buttonView.placeholder = R.string.localizable.newOrderModuleProductPlaceholder()
        buttonView.onRightButtonAction(action: .product, image: R.image.rightArrow())
        return buttonView
    }()
    
    private let fromButtonView: ButtonView = {
        let buttonView = ButtonView()
        buttonView.configure(action: Action.from)
        buttonView.placeholder = R.string.localizable.newOrderModuleFromPlaceholder()
        buttonView.onRightButtonAction(action: .from, image: R.image.rightArrow())
        
        return buttonView
    }()
    
    private let toButton: ButtonView = {
        let buttonView = ButtonView()
        buttonView.configure(action: Action.to)
        buttonView.placeholder = R.string.localizable.newOrderModuleToPlaceholder()
        buttonView.onRightButtonAction(action: .to, image: R.image.rightArrow())
        
        return buttonView
    }()
    
    
    private let deliveryDateButton: ButtonView = {
        let buttonView = ButtonView()
        buttonView.configure(action: Action.selectDate)
        buttonView.placeholder = R.string.localizable.newOrderModuleDeliveryDatePlaceholder()
        buttonView.onLeftButtonAction(action: .selectDate, image: R.image.calendarIcon())
        
        return buttonView
    }()
    private let deliveryTimeButtonView: ButtonView = {
        let buttonView = ButtonView()
        buttonView.configure(action: Action.selectTime)
        buttonView.placeholder = R.string.localizable.newOrderModuleTravelTimePlaceholder()
        buttonView.onLeftButtonAction(action: .selectTime, image: R.image.timeIcon())
        
        return buttonView
    }()
    
    private let rewardButtonView: TextFieldView = {
        let buttonView = TextFieldView()
        buttonView.configure(id:"",
                             title: R.string.localizable.newOrderModuleRewardPlaceholder(),
                             placeholder: R.string.localizable.newOrderModuleRewardPlaceholder(),
                             maskString: nil)
        buttonView.setKeybord(keyboardType: .numberPad)
        
        return buttonView
    }()
    
    private let commentTextViewView: TextViewView = {
        let textViewView = TextViewView()
        textViewView.configure(title: R.string.localizable.newOrderModuleCommentsPlaceholder(),
                                placeholder: R.string.localizable.newOrderModuleCommentsPlaceholder())
        
        return textViewView
    }()
    
    private let checkboxSaveDelivery: CheckBoxButton = {
        let button = CheckBoxButton()
        return button
    }()
    
    private let imageViewShield: UIImageView = {
        let imageView = UIImageView()
        imageView.image = R.image.shield()
        return imageView
    }()
    private let labelSaveDeliveryTitle: UILabel = {
        let label = UILabel()
        label.font = .h4TitleFontSB()
        label.textColor = R.color.labelTextColor()
        label.text = R.string.localizable.safeDelivery()
        return label
    }()
    
    private let priceButtonView: TextFieldView = {
        let buttonView = TextFieldView()
        buttonView.configure(id:"",
                             title: R.string.localizable.newOrderModuleCargoPricePlaceholder(),
                             placeholder: R.string.localizable.newOrderModuleCargoPricePlaceholder(),
                             maskString: nil)
        buttonView.setKeybord(keyboardType: .numberPad)
        
        return buttonView
    }()
    
    private let labelConditionsUse: UILabel = {
        let label = UILabel()
        label.font = .subtitleFont()
        label.textColor = R.color.labelTextColor()
        label.numberOfLines = 0
        label.text = R.string.localizable.newOrderModuleIAmFamiliar()
        return label
    }()
    private let checkboxConditionsUse: CheckBoxButton = {
        let button = CheckBoxButton()
        button.setImage(R.image.checkboxOff(), for: .normal)
        return button
    }()
    private let nextButton: UIButton = {
        let button = UIButton()
        button.setImage(R.image.arrowRightButton(), for: .normal)
        return button
    }()
    
    private let infoButton: UIButton = {
        let button = UIButton()
        button.setImage(R.image.info(), for: .normal)
        return button
    }()
    private let labelInfoRule: UILabel = {
        let label = UILabel()
        label.font = .typeFont()
        label.textColor = R.color.labelTextColor()
        label.numberOfLines = 0
        label.text = R.string.localizable.newOrderModuleOrdersHintPlaceholder()
        return label
    }()
    
    private let datePicker: UITextView = {
        let picker = UITextView()
        picker.isHidden = true
        
        return picker
    }()
    private let timePicker: UITextView = {
        let picker = UITextView()
        picker.isHidden = true
        
        return picker
    }()

    var priceButtonViewheight: Constraint?
    var presenter: NewOrderModulePresenterProtocol?

    // MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let labelTap = UITapGestureRecognizer(target: self, action: #selector(saveDeliveryTitleTapped))
        labelSaveDeliveryTitle.isUserInteractionEnabled = true
        labelSaveDeliveryTitle.addGestureRecognizer(labelTap)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func loadView() {
        super.loadView()
        
        configureLayoutBox()
        
        box.addSubview(backButton)
        box.addSubview(titleLabel)
        box.addSubview(productButtonView)
        box.addSubview(fromButtonView)
        box.addSubview(toButton)
        box.addSubview(deliveryDateButton)
        box.addSubview(deliveryTimeButtonView)
        box.addSubview(rewardButtonView)
        box.addSubview(commentTextViewView)
        box.addSubview(checkboxSaveDelivery)
        box.addSubview(labelConditionsUse)
        box.addSubview(checkboxConditionsUse)
        box.addSubview(nextButton)
        box.addSubview(infoButton)
        box.addSubview(checkboxSaveDelivery)
        box.addSubview(imageViewShield)
        box.addSubview(labelSaveDeliveryTitle)
        box.addSubview(priceButtonView)
        box.addSubview(labelInfoRule)
        
        deliveryDateButton.delegate = self
        deliveryTimeButtonView.delegate = self
        
        
        backButton.addTarget(self, action: #selector(tapBack), for: .touchUpInside)
        backButton.snp.makeConstraints {
            $0.top.equalTo(box).offset(8)
            $0.left.equalTo(box).offset(Constants.offset)
            $0.height.width.equalTo(40)
        }
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(box).offset(56)
            $0.trailing.equalTo(box).inset(Constants.offset)
            $0.leading.equalTo(box).offset(Constants.offset)
            $0.height.equalTo(24)
        }
        productButtonView.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(16)
            $0.trailing.equalTo(box).inset(Constants.offset)
            $0.leading.equalTo(box).offset(Constants.offset)
            $0.height.equalTo(56)
        }
        productButtonView.delegate = self
        
        fromButtonView.snp.makeConstraints {
            $0.top.equalTo(productButtonView.snp.bottom).offset(16)
            $0.trailing.equalTo(box).inset(Constants.offset)
            $0.leading.equalTo(box).offset(Constants.offset)
            $0.height.equalTo(56)
        }
        fromButtonView.delegate = self
        
        toButton.snp.makeConstraints {
            $0.top.equalTo(fromButtonView.snp.bottom).offset(16)
            $0.trailing.equalTo(box).inset(Constants.offset)
            $0.leading.equalTo(box).offset(Constants.offset)
            $0.height.equalTo(56)
        }
        toButton.delegate = self
        
        deliveryDateButton.snp.makeConstraints {
            $0.top.equalTo(toButton.snp.bottom).offset(16)
            $0.trailing.equalTo(box).inset(Constants.offset)
            $0.leading.equalTo(box).offset(Constants.offset)
            $0.height.equalTo(56)
        }
        deliveryTimeButtonView.snp.makeConstraints {
            $0.top.equalTo(deliveryDateButton.snp.bottom).offset(16)
            $0.trailing.equalTo(box).inset(Constants.offset)
            $0.leading.equalTo(box).offset(Constants.offset)
            $0.height.equalTo(56)
        }
        rewardButtonView.snp.makeConstraints {
            $0.top.equalTo(deliveryTimeButtonView.snp.bottom).offset(16)
            $0.trailing.equalTo(box).inset(Constants.offset)
            $0.leading.equalTo(box).offset(Constants.offset)
            $0.height.equalTo(56)
        }
        commentTextViewView.snp.makeConstraints {
            $0.top.equalTo(rewardButtonView.snp.bottom).offset(16)
            $0.trailing.equalTo(box).inset(Constants.offset)
            $0.leading.equalTo(box).offset(Constants.offset)
            $0.height.equalTo(146)
        }
        
        checkboxSaveDelivery.snp.makeConstraints {
            $0.top.equalTo(commentTextViewView.snp.bottom).offset(16)
            $0.width.height.equalTo(24)
            $0.leading.equalTo(box).offset(Constants.offset)
        }
        checkboxSaveDelivery.setChecked(false)
        checkboxSaveDelivery.delegate = self
        
        imageViewShield.snp.makeConstraints {
            $0.top.equalTo(commentTextViewView.snp.bottom).offset(16)
            $0.width.height.equalTo(24)
            $0.leading.equalTo(checkboxSaveDelivery.snp.trailing).offset(8)
        }
        labelSaveDeliveryTitle.snp.makeConstraints {
            $0.top.equalTo(commentTextViewView.snp.bottom).offset(16)
            $0.trailing.equalTo(box).offset(-Constants.offset)
            $0.leading.equalTo(imageViewShield.snp.trailing).offset(8)
        }
        
        let safeDeliveryText = R.string.localizable.safeDelivery()
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: safeDeliveryText)
        attributeString.addAttribute(.underlineStyle, value: NSUnderlineStyle.patternDot.rawValue|NSUnderlineStyle.single.rawValue, range: NSMakeRange(0, safeDeliveryText.count))
        labelSaveDeliveryTitle.attributedText = attributeString
        
        priceButtonView.snp.makeConstraints {
            $0.top.equalTo(labelSaveDeliveryTitle.snp.bottom).offset(16)
            $0.trailing.equalTo(box).offset(-Constants.offset)
            $0.leading.equalTo(box).offset(Constants.offset)
            priceButtonViewheight = $0.height.equalTo(0).constraint
        }
        
        labelConditionsUse.snp.makeConstraints {
            $0.top.equalTo(priceButtonView.snp.bottom).offset(16)
            $0.leading.equalTo(checkboxSaveDelivery.snp.trailing).offset(8)
            $0.trailing.equalTo(box).offset(-Constants.offset)
        }
        let conditionsUseText = R.string.localizable.newOrderModuleIAmFamiliar()
        let attributeString2: NSMutableAttributedString =  NSMutableAttributedString(string: conditionsUseText)
        attributeString2.addAttribute(.underlineStyle, value: NSUnderlineStyle.patternDot.rawValue|NSUnderlineStyle.single.rawValue, range: NSMakeRange(13, 34))
        labelConditionsUse.attributedText = attributeString2
        labelConditionsUse.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapConditionsUseText(_:))))
        labelConditionsUse.isUserInteractionEnabled = true
        
        checkboxConditionsUse.snp.makeConstraints {
            $0.top.equalTo(labelConditionsUse.snp.top).offset(4)
            $0.width.height.equalTo(24)
            $0.leading.equalTo(box).offset(Constants.offset)
        }
        
        nextButton.snp.makeConstraints {
            $0.top.equalTo(labelConditionsUse.snp.bottom).offset(16)
            $0.trailing.equalTo(box).offset(-Constants.offset)
            $0.width.height.equalTo(52)
        }
        nextButton.addTarget(self, action: #selector(tapCreateOrder), for: .touchUpInside)
        
        infoButton.snp.makeConstraints {
            $0.top.equalTo(nextButton.snp.bottom).offset(32)
            $0.leading.equalTo(box).offset(Constants.offset)
            $0.width.height.equalTo(24)
        }
        
        labelInfoRule.snp.makeConstraints {
            $0.top.equalTo(infoButton.snp.bottom).offset(16)
            $0.leading.equalTo(box).offset(Constants.offset)
            $0.trailing.equalTo(box).offset(-Constants.offset)
        }
        
        
        let last = box.subviews.last ?? box
        last.snp.makeConstraints {
            $0.bottom.equalTo(box).offset(-32)
        }
    }
    
    // MARK: ConfigureLayouts
    func configureLayoutBox() {
        view.addSubview(scrollView)
        scrollView.addSubview(box)
        view.addSubview(datePicker)
        view.addSubview(timePicker)
        
        box.backgroundColor = .clear
        view.backgroundColor = R.color.backgroundColor()
        
        scrollView.showsVerticalScrollIndicator = false
        scrollView.delegate = self
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        box.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.centerX.top.bottom.equalToSuperview()
        }
        
        datePicker.snp.makeConstraints {
            $0.bottom.equalToSuperview().offset(-16)
            $0.leading.equalToSuperview()
            $0.trailing.equalToSuperview()
        }
        timePicker.snp.makeConstraints {
            $0.bottom.equalToSuperview().offset(-16)
            $0.leading.equalToSuperview()
            $0.trailing.equalToSuperview()
        }
    }
    
    // MARK: Methods
    func checkDateValidation() -> Bool {
        var result = true
    
        productButtonView.checkValidation(minChars: 1, result: &result)
        fromButtonView.checkValidation(minChars: 1, result: &result)
        toButton.checkValidation(minChars: 1, result: &result)
        deliveryDateButton.checkValidation(minChars: 1, result: &result)
        deliveryTimeButtonView.checkValidation(minChars: 1, result: &result)
        checkboxConditionsUse.checkValidation(needChecked: true, result: &result)
        return result
    }
    func showDialogAddBankCard() {
        Popup().showAlertView(title: R.string.localizable.bankCardModuleTitle(),
                              message: R.string.localizable.bankCardModuleNewOrderInfo(),
                              buttonTitle: R.string.localizable.bind(),
                              icon: nil,
                              onReject: { [weak self] in
                                self?.presenter?.showAddBankCardView()
                              })
    }
    
    // MARK: IBActions
    @objc func tapBack(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func tapConditionsUseText(_ sender: UITapGestureRecognizer) {
        presenter?.tapLabelConditionsUse()
    }
    
    @objc func saveDeliveryTitleTapped() {
        Popup().showAlertView(title: R.string.localizable.safeDelivery(),
                              message: R.string.localizable.newOrderModuleSafeDeliveryInfo(),
                              buttonTitle: R.string.localizable.itisClear(),
                              icon: R.image.shield())
    }
    
    @objc func tapCreateOrder(_ sender: UIButton) {
        presenter?.orderModel.compensation?.amount = Int(rewardButtonView.text) ?? 0
        presenter?.orderModel.comment = commentTextViewView.text
        
        let dateValidation = checkDateValidation()
        if dateValidation {
            if checkboxSaveDelivery.isChecked() {
            Popup().showAlertView(title: R.string.localizable.safeDelivery(),
                                  message: R.string.localizable.newOrderModuleSafeDeliveryInfo(),
                                  buttonTitle: R.string.localizable.itisClear(),
                                  icon: R.image.shield(),
                                  onReject: { [weak self] in
                                    self?.showDialogAddBankCard()
                                  })
            } else {
                self.showDialogAddBankCard()
            }
        }
        
    }
    
    func tapSelectDateTime() {
        print(#function)
        let picker: UIDatePicker = UIDatePicker()
        picker.datePickerMode = UIDatePicker.Mode.dateAndTime
        
        if let deliveryDateTime = presenter?.orderModel.deliveryDateTime {
            picker.date = deliveryDateTime
        } else {
            let calendar = Calendar.current
            var dateComponents: DateComponents? = calendar.dateComponents([.year, .month, .day, .hour, .month], from: Date())
            dateComponents!.month = dateComponents!.month!
            dateComponents?.hour = 10
            dateComponents?.minute = 0
            picker.date = calendar.date(from: dateComponents!)!
        }
        if #available(iOS 13.4, *) {
            picker.preferredDatePickerStyle = .wheels
        }

        datePicker.inputView = picker
        picker.addTarget(self, action: #selector(datePickerValueChanged), for: UIControl.Event.valueChanged)
        datePicker.becomeFirstResponder()
    }
    
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        presenter?.changeDeliveryDateTime(date: sender.date)
    }

}

extension NewOrderModuleViewController: NewOrderModuleViewProtocol {
    func updateView(orderModel: Order) {

        if let product = orderModel.product {
            productButtonView.title = product.name
        }
        if let addressFrom = orderModel.addressFrom {
            fromButtonView.title = addressFrom.streetAddress
        }
        if let addressTo = orderModel.addressTo {
            toButton.title = addressTo.streetAddress
        }
        
        if let deliveryDateTime = orderModel.deliveryDateTime {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.medium
            dateFormatter.timeStyle = DateFormatter.Style.none
            deliveryDateButton.title = dateFormatter.string(from: deliveryDateTime)
            dateFormatter.dateStyle = DateFormatter.Style.none
            dateFormatter.timeStyle = DateFormatter.Style.short
            deliveryTimeButtonView.title = dateFormatter.string(from: deliveryDateTime)
        }

    }
}

extension NewOrderModuleViewController: CheckBoxButtonDelegate {
    func isChange(state: Bool) {
        presenter?.orderModel.isSafeDelivery = state
        UIView.animate(withDuration: 0.2, animations: { [weak self] in
            if state {
                self?.priceButtonViewheight?.update(offset: 56)
            } else {
                self?.priceButtonViewheight?.update(offset: 0)
            }
            self?.priceButtonView.layoutIfNeeded()
        })
    }
}

extension NewOrderModuleViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //
    }
}

// MARK : TextFieldViewDelegate
extension NewOrderModuleViewController: TextFieldViewDelegate {
    
    func tapPrimaryAction(id: String, text: String) {
    }
    
    func tapRightButtonAction(action: Action) {
        if action == .product {
            presenter?.tapProductSelect()
        }
    }
}

// MARK : ButtonViewDelegate
extension NewOrderModuleViewController: ButtonViewDelegate {
    
    func tapAction(action: Action) {
        print(action.rawValue)
        
        switch action {
        case .product:
            presenter?.tapProductSelect()
        
        case .from:
            presenter?.tapAddressSelect(direction: .from)

        case .to:
            presenter?.tapAddressSelect(direction: .to)
          
        case .selectDate:
            tapSelectDateTime()
            
        case .selectTime:
            tapSelectDateTime()
        
        default: break
        }
        
    }
    
    func tapRightAction(action: Action) {
        print(#function)
        print(action.rawValue)
        
        switch action {
        case .product:
            presenter?.tapProductSelect()
            break
        case .from:
            presenter?.tapAddressSelect(direction: .from)
            break
        case .to:
            presenter?.tapAddressSelect(direction: .to)
            break
        default: break
        }
    }
}


