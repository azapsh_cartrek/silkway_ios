//
//  OrdersLineRouter.swift
//  The SilkWay
//
//  Created by Azapsh on 06.06.2021.
//  Copyright © 2021 Ахмед Фокичев. All rights reserved.
//

import Foundation
import UIKit

protocol OrdersLineRouterProtocol: AnyObject {

}

final class OrdersLineRouter: OrdersLineRouterProtocol {
    
    // MARK: Properties
    weak var viewController: UIViewController?
    
    init(viewController: UIViewController) {
        self.viewController = viewController
    }
    // MARK: Internal helpers

}
