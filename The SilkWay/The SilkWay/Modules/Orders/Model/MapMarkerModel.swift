//
//  MapMarkerModel.swift
//  The SilkWay
//
//  Created by Azapsh on 17.07.2021.
//  Copyright © 2021 Ахмед Фокичев. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

enum  MapMarkerType {
    case trackPoint
    case orderPoint
}

struct MapMarkerModel {
    var location: CLLocationCoordinate2D
    var title: String
    var snippet: String?
    var icon: UIImage?
    var type = MapMarkerType.trackPoint
}
