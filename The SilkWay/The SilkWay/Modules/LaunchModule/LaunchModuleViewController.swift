//
//  LaunchModuleViewController.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 14/05/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import UIKit

final class LaunchModuleViewController: UIViewController {
    // MARK: Constants

    // MARK: IBOutlets

    // MARK: Properties
    var presenter: LaunchModulePresenter!

    // MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ModuleBulder.launchConfigure(view: self)
        presenter?.start()
    }
    override func viewDidLayoutSubviews() {
        if #available(iOS 12.0, *) , traitCollection.userInterfaceStyle == .dark {
            view.backgroundColor = .theBlack()
        } else {
            view.backgroundColor = .white
        }
    }
    // MARK: Methods

    // MARK: IBActions

}

extension LaunchModuleViewController: LaunchModuleViewProtocol {
    
}
