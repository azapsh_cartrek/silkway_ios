//
//  Direction.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 01.01.2021.
//  Copyright © 2021 Ахмед Фокичев. All rights reserved.
//

import Foundation

enum Direction {
    case to
    case from
    case stopOver
}
