//
//  TravelModuleRouter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 13/02/2021.
//  Copyright © 2021 SilkWay. All rights reserved.
//

import Foundation
import UIKit

protocol NewTravelModuleRouterProtocol: AnyObject {
    func showAddressSelectVC(presenterDelegate: AddrssSelectDelegate, direction: Direction)
    func showNewTravelStopOver(delegate: StopOverModuleDelegate, tripId: String)
}

final class NewTravelModuleRouter: Router, NewTravelModuleRouterProtocol {
    
    // MARK: Properties
    weak var presenter: NewTravelModulePresenter? 
    
    // MARK: Internal helpers
    func showAddressSelectVC(presenterDelegate: AddrssSelectDelegate, direction: Direction) {
        let vc = OrderAddrssSelectModuleViewController()
        let router = OrderAddrssSelectModuleRouterImplementation(view: vc)
        let addressService = AddressService()
        let presenter = OrderAddrssSelectModulePresenter(with: vc,
                                                         router: router,
                                                         addressService: addressService,
                                                         direction: direction)
        presenter.delegate = presenterDelegate
        vc.presenter = presenter
        view?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showNewTravelStopOver(delegate: StopOverModuleDelegate, tripId: String) {
        let vc = TravelStopOverModuleViewController()
        let router = TravelStopOverModuleRouter(viewController: vc)
        let uuid = UUID().uuidString
        let waypointModel = WaypointModel()
        let presenter = TravelStopOverModulePresenter(with: vc, router: router, delegate: delegate, waypointModel: waypointModel)
        vc.presenter = presenter
        view?.navigationController?.pushViewController(vc, animated: true)
    }
}

