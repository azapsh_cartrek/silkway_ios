//
//  OrdersModuleRouter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 13/06/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation
import UIKit

protocol OrdersModuleRouter: class {

}

final class OrdersModuleRouterImplementation: OrdersModuleRouter {
    
    // MARK: Properties
    weak var view: OrdersModuleViewController?
    weak var presenter: OrdersModulePresenter? 
    
    init(with viewController: OrdersModuleViewController) {
        self.view = viewController
    }
    // MARK: Internal helpers

}

