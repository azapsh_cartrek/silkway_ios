//
//  MyProfileModuleViewController.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 13/06/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import UIKit
import SDWebImage

final class MyProfileModuleViewController: UIViewController {
    // MARK: Constants
    private enum Constants {
        static let offset: CGFloat = 20
    }
    // MARK: IBOutlets
    var scrollView = UIScrollView()
    var box = UIView()
    var last = UIView()
    var publicMode = false
    var navBarView = NavBarView()
    
    // MARK: Properties
    var presenter: MyProfileModulePresenter!
    
    // MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewDidLoad")
        
        if !publicMode {
          setNavigationNavBarView()
        }
        
        configureLayoutBox()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.start()
        print("viewWillAppear>")
    }
    
    
    private func setNavigationNavBarView() {
        navigationItem.title = nil
        if let navigationBar = self.navigationController?.navigationBar {
            navigationBar.addSubview(navBarView)
            navBarView.pin(to: navigationBar)
            navBarView.setup(leftButtonTitle: nil, rightButtonTitle: nil, leftButtonImage: nil, rightButtonImage: R.image.settings())
            navBarView.delegate = self
        }
    }
    
    // MARK: ConfigureLayouts
    func configureLayoutBox() {
        box.backgroundColor = .clear
        if #available(iOS 12.0, *) , traitCollection.userInterfaceStyle == .dark {
            view.backgroundColor = .theBlack()
        } else {
            view.backgroundColor = .white
        }
        self.view.addSubview(scrollView)
        scrollView.addSubview(box)
        scrollView.showsVerticalScrollIndicator = false
        scrollView.delegate = self
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        box.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.centerX.top.bottom.equalToSuperview()
        }
        if #available(iOS 12.0, *) , traitCollection.userInterfaceStyle == .dark {
            box.backgroundColor = .theBlack()
        }
    }
    
    func configureLayoutOrganization(userProfile: UserProfile, reviews: [Review]) {
        for obj in box.subviews {
            obj.removeFromSuperview()
        }
        
        let backButton = UIButton()
        backButton.setImage(R.image.back(), for: .normal)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: -20, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(tapBack), for: .touchUpInside)
        box.addSubview(backButton)
        backButton.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(box).offset(8)
            make.left.equalTo(box).offset(Constants.offset)
            make.height.width.equalTo(40)
        }
        backButton.isHidden = !publicMode
        
        if !publicMode {
            let settingButton = UIButton()
            settingButton.setImage(R.image.settings(), for: .normal)
            settingButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -20)
            settingButton.addTarget(self, action: #selector(tapSettings), for: .touchUpInside)
            box.addSubview(settingButton)
            settingButton.snp.makeConstraints{ (make) -> Void in
                make.top.equalTo(box).offset(8)
                make.right.equalTo(box).inset(Constants.offset)
                make.height.width.equalTo(40)
            }
        }
        
        if publicMode {
            let infoButton = UIButton()
            infoButton.setImage(R.image.info(), for: .normal)
            infoButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -20)
            infoButton.addTarget(self, action: #selector(tapInfo(_:)), for: .touchUpInside)
            box.addSubview(infoButton)
            infoButton.snp.makeConstraints{ (make) -> Void in
                make.top.equalTo(box).offset(8)
                make.right.equalTo(box).inset(Constants.offset)
                make.height.width.equalTo(40)
            }
        }
        
        
        let avatarImageView = UIImageView()
        box.addSubview(avatarImageView)
        avatarImageView.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(box).offset(64)
            make.centerX.equalTo(box)
            make.height.width.equalTo(128)
        }
        avatarImageView.sd_setImage(with: URL(string: userProfile.avatarUrl), placeholderImage: R.image.addPhoto())
        avatarImageView.contentMode = .scaleAspectFill
        avatarImageView.asCircle(cornerRadius: 64)
        
        
        let organizationNameLabel = UILabel()
        box.addSubview(organizationNameLabel)
        organizationNameLabel.text = userProfile.organizationName
        organizationNameLabel.font = .h2TitleFont()
        organizationNameLabel.textAlignment = .center
        organizationNameLabel.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(avatarImageView.snp.bottom).offset(16)
            make.trailing.equalTo(box).inset(Constants.offset)
            make.leading.equalTo(box).offset(Constants.offset)
            make.height.equalTo(24)
        }
        navBarView.setTitle(title: userProfile.organizationName)
        
        let organizationUserName = UILabel()
        box.addSubview(organizationUserName)
        let name = userProfile.organizationRepresentativeName + " " + userProfile.organizationRepresentativeSurName
        organizationUserName.text = name + ", " + userProfile.organizationRepresentativePosition
        organizationUserName.font = .h4TitleFontSB()
        organizationUserName.textAlignment = .center
        organizationUserName.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(organizationNameLabel.snp.bottom).offset(8)
            make.trailing.equalTo(box).inset(Constants.offset)
            make.leading.equalTo(box).offset(Constants.offset)
            make.height.equalTo(24)
        }
        
        let organizationInnNumberLabel = UILabel()
        box.addSubview(organizationInnNumberLabel)
        organizationInnNumberLabel.text = R.string.localizable.inN() + ": " + userProfile.organizationInnNumber
        organizationInnNumberLabel.font = .h4TitleFontSB()
        organizationInnNumberLabel.textAlignment = .center
        organizationInnNumberLabel.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(organizationUserName.snp.bottom).offset(8)
            make.trailing.equalTo(box).inset(Constants.offset)
            make.leading.equalTo(box).offset(Constants.offset)
            make.height.equalTo(24)
        }
        
        let organizationOgrnNumberLabel = UILabel()
        box.addSubview(organizationOgrnNumberLabel)
        organizationOgrnNumberLabel.text =  R.string.localizable.ogrN() + ": " + userProfile.organizationOgrnNumber
        organizationOgrnNumberLabel.font = .h4TitleFontSB()
        organizationOgrnNumberLabel.textAlignment = .center
        organizationOgrnNumberLabel.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(organizationInnNumberLabel.snp.bottom).offset(8)
            make.trailing.equalTo(box).inset(Constants.offset)
            make.leading.equalTo(box).offset(Constants.offset)
            make.height.equalTo(24)
        }
        
        
        let isVerifiedLabel = UILabel()
        last = box.subviews.last ?? box
        box.addSubview(isVerifiedLabel)
        isVerifiedLabel.font = .typeFont()
        isVerifiedLabel.textAlignment = .center
        if userProfile.isVerified {
            isVerifiedLabel.set(text: " " + R.string.localizable.profileVerified(), leftIcon: R.image.checkOn(), rightIcon: nil)
        } else {
            isVerifiedLabel.set(text: " " + R.string.localizable.profileNotVerified(), leftIcon: R.image.checkOff(), rightIcon: nil)
        }
        isVerifiedLabel.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(last.snp.bottom).offset(8)
            make.trailing.equalTo(box).inset(Constants.offset)
            make.leading.equalTo(box).offset(Constants.offset)
            make.height.equalTo(24)
        }
        
        box.addLine( color: R.color.lineColor(), offset: 16)
        
        let titleRate = UILabel()
        last = box.subviews.last ?? box
        box.addSubview(titleRate)
        titleRate.text = R.string.localizable.rating()
        titleRate.font = .h4TitleFontSB()
        titleRate.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(last.snp.bottom).offset(16)
            make.leading.equalTo(box).offset(Constants.offset)
            make.width.equalTo(box.snp.width).dividedBy(2).dividedBy(2).offset(-30)
            make.height.equalTo(20)
        }
        
        let titleDeliverys = UILabel()
        last = box.subviews.last ?? box
        box.addSubview(titleDeliverys)
        titleDeliverys.text = R.string.localizable.deliverys()
        titleDeliverys.font = .h4TitleFontSB()
        //titleDeliverys.backgroundColor = .orange
        titleDeliverys.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(titleRate.snp.top)
            make.trailing.equalTo(box).inset(Constants.offset)
            make.width.equalTo(box.snp.width).dividedBy(2).offset(-30)
            make.height.equalTo(20)
        }
        
        let starsView = StarsView()
        box.addSubview(starsView)
        starsView.setStars(value: userProfile.rate)
        starsView.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(titleRate.snp.bottom).offset(8)
            make.leading.equalTo(box).offset(Constants.offset)
            make.height.equalTo(20)
            make.width.equalTo(120)
        }
        
        let deliverysCounts = UILabel()
        box.addSubview(deliverysCounts)
        deliverysCounts.text = String(userProfile.deliverysCounts)
        deliverysCounts.font = .h3TitleFont()
        deliverysCounts.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(titleDeliverys.snp.bottom).offset(8)
            make.trailing.equalTo(box).inset(Constants.offset)
            make.height.equalTo(20)
            make.width.equalTo(box.snp.width).dividedBy(2).offset(-30)
        }
        
        if !publicMode {
            let phoneNumberLabel = UILabel()
            box.addSubview(phoneNumberLabel)
            phoneNumberLabel.text =  R.string.localizable.phoneNumber() + ": " + userProfile.phoneNumber
            phoneNumberLabel.font = .subtitleFont()
            phoneNumberLabel.textAlignment = .left
            phoneNumberLabel.snp.makeConstraints{ (make) -> Void in
                make.top.equalTo(starsView.snp.bottom).offset(16)
                make.leading.equalTo(box).offset(Constants.offset)
                make.height.equalTo(24)
                make.width.equalTo(box.snp.width).offset(-30)
            }
            
            let bankCardNumberLabel = UILabel()
            box.addSubview(bankCardNumberLabel)
            bankCardNumberLabel.text =  R.string.localizable.bankCard() + ": " + userProfile.phoneNumber
            bankCardNumberLabel.font = .subtitleFont()
            bankCardNumberLabel.textAlignment = .left
            bankCardNumberLabel.snp.makeConstraints{ (make) -> Void in
                make.top.equalTo(phoneNumberLabel.snp.bottom).offset(8)
                make.leading.equalTo(box).offset(Constants.offset)
                make.height.equalTo(24)
                make.width.equalTo(box.snp.width).offset(-30)
            }
        }
        
        box.addLine( color: R.color.lineColor(), offset: 16)
        
        let reviewsLabel = UILabel()
        last = box.subviews.last ?? box
        box.addSubview(reviewsLabel)
        reviewsLabel.tag = 56
        reviewsLabel.text =  R.string.localizable.reviews()
        reviewsLabel.font = .h3TitleFont()
        reviewsLabel.textAlignment = .left
        reviewsLabel.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(last.snp.bottom).offset(8)
            make.leading.equalTo(box).offset(Constants.offset)
            make.height.equalTo(54)
            make.width.equalTo(box.snp.width).offset(-30)
        }
        
        for review in reviews {
            let reViewView = ReViewView()
            last = box.subviews.last ?? box
            box.addSubview(reViewView)
            reViewView.setReview(review: review)
            reViewView.delegate = self
            reViewView.snp.makeConstraints { (make) in
                make.top.equalTo(last.snp.bottom).offset(16)
                make.width.equalTo(box)
                make.left.equalTo(box).offset(Constants.offset)
            }
            
            let reviewLine = UIView()
            box.addSubview(reviewLine)
            reviewLine.backgroundColor = R.color.lineColor()
            reviewLine.snp.makeConstraints{ (make) -> Void in
                make.top.equalTo(reViewView.snp.bottom).offset(32)
                make.trailing.equalTo(box)
                make.leading.equalTo(box)
                make.height.equalTo(1)
            }
        }
        
        last = box.subviews.last ?? box
        last.snp.makeConstraints{ (make) -> Void in
            make.bottom.equalTo(box)
        }
    }
    
    func configureLayoutPerson(userProfile: UserProfile, reviews: [Review]) {
        for obj in box.subviews {
            obj.removeFromSuperview()
        }
        
        let backButton = UIButton()
        backButton.setImage(R.image.back(), for: .normal)
        backButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: -20, bottom: 0, right: 0)
        backButton.addTarget(self, action: #selector(tapBack), for: .touchUpInside)
        box.addSubview(backButton)
        backButton.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(box).offset(8)
            make.left.equalTo(box).offset(Constants.offset)
            make.height.width.equalTo(40)
        }
        backButton.isHidden = !publicMode
        
        if !publicMode {
            let settingButton = UIButton()
            settingButton.setImage(R.image.settings(), for: .normal)
            settingButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -20)
            settingButton.addTarget(self, action: #selector(tapSettings), for: .touchUpInside)
            box.addSubview(settingButton)
            settingButton.snp.makeConstraints{ (make) -> Void in
                make.top.equalTo(box).offset(8)
                make.right.equalTo(box).inset(Constants.offset)
                make.height.width.equalTo(40)
            }
        }
        
        if publicMode {
            let infoButton = UIButton()
            infoButton.setImage(R.image.info(), for: .normal)
            infoButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -20)
            infoButton.addTarget(self, action: #selector(tapInfo(_:)), for: .touchUpInside)
            box.addSubview(infoButton)
            infoButton.snp.makeConstraints{ (make) -> Void in
                make.top.equalTo(box).offset(8)
                make.right.equalTo(box).inset(Constants.offset)
                make.height.width.equalTo(40)
            }
        }
        
        
        let avatarImageView = UIImageView()
        box.addSubview(avatarImageView)
        avatarImageView.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(box).offset(64)
            make.centerX.equalTo(box)
            make.height.width.equalTo(128)
        }
        avatarImageView.sd_setImage(with: URL(string: userProfile.avatarUrl), placeholderImage: R.image.addPhoto())
        avatarImageView.contentMode = .scaleAspectFill
        avatarImageView.asCircle(cornerRadius: 64)
        
        let persionName = UILabel()
        box.addSubview(persionName)
        let name = userProfile.firstName + " " + userProfile.lastName
        persionName.text = name
        persionName.font = .h4TitleFontSB()
        persionName.textAlignment = .center
        persionName.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo( avatarImageView.snp.bottom).offset(8)
            make.trailing.equalTo(box).inset(Constants.offset)
            make.leading.equalTo(box).offset(Constants.offset)
            make.height.equalTo(24)
        }
        navBarView.setTitle(title: name)
        
        
        let isVerifiedLabel = UILabel()
        last = box.subviews.last ?? box
        box.addSubview(isVerifiedLabel)
        isVerifiedLabel.font = .typeFont()
        isVerifiedLabel.textAlignment = .center
        if userProfile.isVerified {
            isVerifiedLabel.set(text: " " + R.string.localizable.profileVerified(), leftIcon: R.image.checkOn(), rightIcon: nil)
        } else {
            isVerifiedLabel.set(text: " " + R.string.localizable.profileNotVerified(), leftIcon: R.image.checkOff(), rightIcon: nil)
        }
        isVerifiedLabel.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(last.snp.bottom).offset(8)
            make.trailing.equalTo(box).inset(Constants.offset)
            make.leading.equalTo(box).offset(Constants.offset)
            make.height.equalTo(24)
        }
        
        box.addLine( color: R.color.lineColor(), offset: 16)
        
        let titleRate = UILabel()
        last = box.subviews.last ?? box
        box.addSubview(titleRate)
        titleRate.text = R.string.localizable.rating()
        titleRate.font = .h4TitleFontSB()
        titleRate.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(last.snp.bottom).offset(16)
            make.leading.equalTo(box).offset(Constants.offset)
            make.width.equalTo(box.snp.width).dividedBy(2).dividedBy(2).offset(-30)
            make.height.equalTo(20)
        }
        
        let titleDeliverys = UILabel()
        last = box.subviews.last ?? box
        box.addSubview(titleDeliverys)
        titleDeliverys.text = R.string.localizable.deliverys()
        titleDeliverys.font = .h4TitleFontSB()
        //titleDeliverys.backgroundColor = .orange
        titleDeliverys.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(titleRate.snp.top)
            make.trailing.equalTo(box).inset(Constants.offset)
            make.width.equalTo(box.snp.width).dividedBy(2).offset(-30)
            make.height.equalTo(20)
        }
        
        let starsView = StarsView()
        box.addSubview(starsView)
        starsView.setStars(value: userProfile.rate)
        starsView.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(titleRate.snp.bottom).offset(8)
            make.leading.equalTo(box).offset(Constants.offset)
            make.height.equalTo(20)
            make.width.equalTo(120)
        }
        
        let deliverysCounts = UILabel()
        box.addSubview(deliverysCounts)
        deliverysCounts.text = String(userProfile.deliverysCounts)
        deliverysCounts.font = .h3TitleFont()
        deliverysCounts.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(titleDeliverys.snp.bottom).offset(8)
            make.trailing.equalTo(box).inset(Constants.offset)
            make.height.equalTo(20)
            make.width.equalTo(box.snp.width).dividedBy(2).offset(-30)
        }
        
        if !publicMode {
            let phoneNumberLabel = UILabel()
            box.addSubview(phoneNumberLabel)
            phoneNumberLabel.text =  R.string.localizable.phoneNumber() + ": " + userProfile.phoneNumber
            phoneNumberLabel.font = .subtitleFont()
            phoneNumberLabel.textAlignment = .left
            phoneNumberLabel.snp.makeConstraints{ (make) -> Void in
                make.top.equalTo(starsView.snp.bottom).offset(16)
                make.leading.equalTo(box).offset(Constants.offset)
                make.height.equalTo(24)
                make.width.equalTo(box.snp.width).offset(-30)
            }
            
            let bankCardNumberLabel = UILabel()
            box.addSubview(bankCardNumberLabel)
            bankCardNumberLabel.text =  R.string.localizable.bankCard() + ": " + userProfile.phoneNumber
            bankCardNumberLabel.font = .subtitleFont()
            bankCardNumberLabel.textAlignment = .left
            bankCardNumberLabel.snp.makeConstraints{ (make) -> Void in
                make.top.equalTo(phoneNumberLabel.snp.bottom).offset(8)
                make.leading.equalTo(box).offset(Constants.offset)
                make.height.equalTo(24)
                make.width.equalTo(box.snp.width).offset(-30)
            }
        }
        
        box.addLine( color: R.color.lineColor(), offset: 16)
        
        let reviewsLabel = UILabel()
        last = box.subviews.last ?? box
        box.addSubview(reviewsLabel)
        reviewsLabel.tag = 56
        reviewsLabel.text =  R.string.localizable.reviews()
        reviewsLabel.font = .h3TitleFont()
        reviewsLabel.textAlignment = .left
        reviewsLabel.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(last.snp.bottom).offset(8)
            make.leading.equalTo(box).offset(Constants.offset)
            make.height.equalTo(54)
            make.width.equalTo(box.snp.width).offset(-30)
        }
        
        for review in reviews {
            let reViewView = ReViewView()
            last = box.subviews.last ?? box
            box.addSubview(reViewView)
            reViewView.setReview(review: review)
            reViewView.delegate = self
            reViewView.snp.makeConstraints { (make) in
                make.top.equalTo(last.snp.bottom).offset(16)
                make.width.equalTo(box)
                make.left.equalTo(box).offset(Constants.offset)
            }
            
            let reviewLine = UIView()
            box.addSubview(reviewLine)
            reviewLine.backgroundColor = R.color.lineColor()
            reviewLine.snp.makeConstraints{ (make) -> Void in
                make.top.equalTo(reViewView.snp.bottom).offset(32)
                make.trailing.equalTo(box)
                make.leading.equalTo(box)
                make.height.equalTo(1)
            }
        }
        
        last = box.subviews.last ?? box
        last.snp.makeConstraints{ (make) -> Void in
            make.bottom.equalTo(box)
        }
    }
    
    // MARK: Methods
    func updateScrollposition(position: CGFloat) {
        guard let nc = self.navigationController else { return }
        
        if nc.isNavigationBarHidden && position > 140 && !publicMode {
            nc.setNavigationBarHidden(false, animated: true)
        }
        if !nc.isNavigationBarHidden && position < 140 {
            nc.setNavigationBarHidden(true, animated: true)
        }
    }
    
    // MARK: IBActions
    @objc func tapBack(_ sender: UIButton) {
        print(#function)
        navigationController?.popViewController(animated: true)
    }
    @objc func tapSettings(_ sender: UIButton) {
        print(#function)
        presenter?.tapSettings()
    }
    @objc func tapInfo(_ sender: UIButton) {
        print(#function)
        // presenter?.tapSettings()
    }
}

extension MyProfileModuleViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateScrollposition(position: scrollView.contentOffset.y)
    }
}

extension MyProfileModuleViewController: MyProfileModuleViewProtocol {
    func setUserProfile(userProfile: UserProfile, reviews: [Review]) {
        if userProfile.type == .organization {
            configureLayoutOrganization(userProfile: userProfile, reviews: reviews)
        }
        if userProfile.type == .individual {
            configureLayoutPerson(userProfile: userProfile, reviews: reviews)
        }
    }
}

extension MyProfileModuleViewController: ReViewViewDelegate {
    func tapReview(review: Review?) {
        presenter?.tapReview(review: review)
    }
}
extension MyProfileModuleViewController: NavBarViewViewDelegate {
    func tapLeftButton() {
        
    }
    
    func tapRightButton() {
        presenter?.tapSettings()
    }
}

