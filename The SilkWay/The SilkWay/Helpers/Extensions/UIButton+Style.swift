//
//  UIButton+Style.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 20.05.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//

import Foundation
import UIKit

enum ButtonStyle {
    case standart
    case blue
    case checkIn
    case checkOut
}

extension UIButton {
    
    func  setStyle(style: ButtonStyle) {
        
        if #available(iOS 12.0, *) , traitCollection.userInterfaceStyle == .dark {
            
            switch style {
            case .blue:
                self.backgroundColor = UIColor.blueLite()
                self.layer.cornerRadius = 25
                self.layer.borderWidth = 1
                self.layer.borderColor =  UIColor.blueLite().cgColor
                self.setTitleColor(UIColor.theBlack(), for: .normal)
                
            case .standart:
                self.backgroundColor = UIColor.blackLite()
                self.layer.cornerRadius = 25
                self.layer.borderWidth = 1
                self.layer.borderColor =  UIColor.blackLite2().cgColor
                self.setTitleColor(UIColor.blueLite(), for: .normal)
                
            case .checkIn:
                self.backgroundColor = UIColor.blackLite()
                self.layer.cornerRadius = 2
                self.layer.borderWidth = 1
                self.layer.borderColor =  UIColor.blackLite2().cgColor
                self.setTitleColor(UIColor.blueLite(), for: .normal)
                
            case .checkOut:
                self.backgroundColor = UIColor.blackLite()
                self.layer.cornerRadius = 2
                self.layer.borderWidth = 1
                self.layer.borderColor =  UIColor.blackLite2().cgColor
                self.setTitleColor(UIColor.blueLite(), for: .normal)
                
            }
            
        } else {
            
            switch style {
            case .blue:
                self.backgroundColor = UIColor.blue()
                self.layer.cornerRadius = 25
                self.layer.borderWidth = 1
                self.layer.borderColor = UIColor.blue().cgColor
                self.setTitleColor(UIColor.white, for: .normal)
                
            case .standart:
                self.backgroundColor = UIColor.white
                self.layer.cornerRadius = 25
                self.layer.borderWidth = 1
                self.layer.borderColor =  UIColor.greyLite().cgColor
                self.setTitleColor(UIColor.blue(), for: .normal)
                
            case .checkIn:
                self.backgroundColor = UIColor.blue().withAlphaComponent(0.1)
                self.layer.cornerRadius = 4
                self.layer.borderWidth = 1
                self.layer.borderColor = UIColor.blue().withAlphaComponent(0.4).cgColor
                self.setTitleColor(UIColor.black, for: .normal)
                
            case .checkOut:
                self.backgroundColor = UIColor.white
                self.layer.cornerRadius = 4
                self.layer.borderWidth = 1
                self.layer.borderColor = UIColor.greyLite().cgColor
                self.setTitleColor(UIColor.black, for: .normal)
            }
        }
        
    }
}
