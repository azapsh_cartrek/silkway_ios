//
//  ReviewsService.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 18.07.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//

import Foundation

class ReviewsService {
    
    public static let shared = ReviewsService()
    
    func getReviews(userid: String) -> [Review] {
        
        var result = [Review]()
        
        let review = Review(id: "2", userId: "2", description: "розовый мерс", rate: 2, sort: 0)
        let review2 = Review(id: "11", userId: "11",  description: "куплю тормаза не дорого", rate: 2,sort: 0)
        let review3 = Review(id: "16", userId: "16", description: "красное гавно не едет", rate: 2,sort: 0)
        
        let review5 = Review(id: "22", userId: "22", description: "Все хорошо, доставил оперативно, посылка в полной сохранности. Рекомендую!", rate: 3)
        
        let review6 = Review(id: "23",userId: "23", description: "Все хорошо!", rate: 4)
        let review4 = Review(id: "07", userId: "07", description: "президент финов, Все хорошо, доставил оперативно, посылка в полной сохранно, альфа рамео тоже херня", rate: 2, sort: 0)
        let review7 = Review(id: "23", userId: "23", description: "Все хорошо!", rate: 5)
        result.append(review)
        result.append(review2)
        result.append(review3)
        result.append(review5)
        result.append(review6)
        result.append(review4)
        result.append(review7)
        return result
    }
}


