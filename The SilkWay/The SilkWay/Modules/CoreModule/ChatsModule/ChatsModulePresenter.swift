//
//  ChatsModulePresenter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 13/06/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation

protocol ChatsModuleViewProtocol: class {

}

protocol ChatsModuleViewPresenter: class {
    init(with view: ChatsModuleViewProtocol, router: ChatsModuleRouter)
}

final class ChatsModulePresenter: ChatsModuleViewPresenter {

    // MARK: Constants

    // MARK: Properties

    weak var view: ChatsModuleViewProtocol?
    var router: ChatsModuleRouter!

    init(with view: ChatsModuleViewProtocol, router: ChatsModuleRouter) {
        self.view = view
        self.router = router
    }
    
    // MARK: Methods
    
}
