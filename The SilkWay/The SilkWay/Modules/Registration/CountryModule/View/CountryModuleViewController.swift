//
//  CountryModuleViewController.swift
//  urent-ios
//
//  Created by Denis Borodavchenko on 27/12/2019.
//  Copyright © 2019 Urent. All rights reserved.
//

import UIKit
import PhoneNumberKit

protocol CountryModuleViewControllerDelegate: class {
    func countryDidSelect(country: Country)
}

final class CountryModuleViewController: UIViewController {
    // MARK: Constants
    private enum Constants {
        static let rowHeight: CGFloat = 64
    }
    @IBOutlet
    private weak var leadingContainerViewConstraint: NSLayoutConstraint! {
        didSet {
            leadingContainerViewConstraint.constant = 12
        }
    }
    @IBOutlet
    private weak var trailingContainerViewConstraint: NSLayoutConstraint! {
        didSet {
            trailingContainerViewConstraint.constant = 12
        }
    }
    
    // MARK: IBOutlets
    @IBOutlet private var tableView: UITableView!
    // MARK: Properties
    var presenter: CountryModulePresenter!
    lazy var resultSearchController: UISearchController = {
        let controller = UISearchController(searchResultsController: nil)
        controller.searchResultsUpdater = self
        controller.hidesNavigationBarDuringPresentation = false
        controller.dimsBackgroundDuringPresentation = false
        controller.obscuresBackgroundDuringPresentation = false
        controller.searchBar.delegate = self
        definesPresentationContext = true
        return controller
    }()

    weak var delegate: CountryModuleViewControllerDelegate?
    private var filterCountries: [Country] = []
    // MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(R.nib.countryTableViewCell)
        navigationItem.searchController = resultSearchController
        navigationItem.hidesSearchBarWhenScrolling = false
        navigationController?.view.backgroundColor = .white
        if #available(iOS 12.0, *) , traitCollection.userInterfaceStyle == .dark {
            view.backgroundColor = .theBlack()
        } else {
            view.backgroundColor = .white
        }
        presenter?.start()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = R.string.localizable.selectCountryTitle()
        let closeButton = UIButton(frame: CGRect(x: 0, y: 0, width: 34, height: 34))
        closeButton.setImage(R.image.back(), for: .normal)
        closeButton.addTarget(self, action: #selector(dismiss(_:)), for: .touchUpInside)
        navigationItem.setLeftBarButton(UIBarButtonItem(customView: closeButton), animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func setlist(list: [Country]) {
        filterCountries = list
        tableView.reloadData()
    }
    
    // MARK: IBActions
    @objc func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: UITableViewDelegate, UITableViewDataSource
extension CountryModuleViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("filterCountries.count", filterCountries.count)
         return filterCountries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.countryItem, for: indexPath)!

        let country = filterCountries[indexPath.row]
        print("country>",country.name)
        cell.configure(with: country)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let country = filterCountries[indexPath.row]
        delegate?.countryDidSelect(country: country)
        self.dismiss(self)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.rowHeight
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        resultSearchController.searchBar.resignFirstResponder()
    }
}

// MARK: CountryModuleViewProtocol
extension CountryModuleViewController: CountryModuleViewProtocol {
    func updateTableView() {
        tableView.reloadData()
    }
    func setList(list: [Country]) {
        filterCountries = list
        tableView.reloadData()
    }
}

// MARK: UISearchResultsUpdating & UISearchBarDelegate
extension CountryModuleViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchString = searchController.searchBar.text else {
            return
        }
        presenter.filterCountries(searchString: searchString)
    }
}

extension CountryModuleViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print(#function)
    }
}
