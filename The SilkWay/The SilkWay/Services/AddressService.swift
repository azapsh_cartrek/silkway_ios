//
//  AddressService.swift
//  The SilkWay
//
//  Created by Azapsh on 09.04.2021.
//  Copyright © 2021 Ахмед Фокичев. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

protocol AddressServiceProtocol: class {
    func searchText(text: String, coordinate: CLLocationCoordinate2D, radiusMeters: CLLocationDistance, completion: @escaping (_ success: Bool, _ list: [AddressPoint], _ error: String) -> Void)
    func getAddressPoint(coordinate: CLLocationCoordinate2D, completion: @escaping ( _ addressPoint: AddressPoint?, _ error: String) -> Void)
}

final class AddressService: AddressServiceProtocol {
    
    func getAddressPoint(coordinate: CLLocationCoordinate2D, completion: @escaping ( _ addressPoint: AddressPoint?, _ error: String) -> Void) {
        
        let geocoder = CLGeocoder()
        let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        
        geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
            guard error == nil else {
                print("CLGeocoder>", error?.localizedDescription ?? "")
                completion(nil, error?.localizedDescription ?? "")
                return
            }
            //            print(placemarks)
            // Most geocoding requests contain only one result.
            if let firstPlacemark = placemarks?.first {
                //                print("address1:", placemark.thoroughfare ?? "")
                //                print("address2:", placemark.subThoroughfare ?? "")
                //                print("city:",     placemark.locality ?? "")
                //                print("state:",    placemark.administrativeArea ?? "")
                //                print("zip code:", placemark.postalCode ?? "")
                //                print("country:",  placemark.country ?? "")
                let thoroughfare = firstPlacemark.thoroughfare ?? ""
                let subThoroughfare = firstPlacemark.subThoroughfare ?? ""
                print(firstPlacemark.locality ?? "" , ", ", firstPlacemark.thoroughfare ?? "", ", " , subThoroughfare)
                
                let lat = firstPlacemark.location?.coordinate.latitude ?? 0
                let lng = firstPlacemark.location?.coordinate.longitude ?? 0
                
                let addressPoint = AddressPoint(
                    country: firstPlacemark.country,
                    state: firstPlacemark.administrativeArea,
                    city:  firstPlacemark.locality,
                    address1: thoroughfare,
                    address2: subThoroughfare,
                    location: CLLocationCoordinate2D(latitude: lat,longitude: lng)
                )

                completion(addressPoint, error?.localizedDescription ?? "")
            }
        }
    }
    
    func searchText(text: String, coordinate: CLLocationCoordinate2D, radiusMeters: CLLocationDistance, completion: @escaping (_ success: Bool, _ list: [AddressPoint], _ error: String) -> Void) {
        
        let searchRequest = MKLocalSearch.Request()
        searchRequest.naturalLanguageQuery = text
        searchRequest.region = MKCoordinateRegion(center: coordinate,
                                                  latitudinalMeters: radiusMeters, longitudinalMeters: radiusMeters)
        let search = MKLocalSearch(request: searchRequest)
        var addresList = [AddressPoint]()
        
        search.start { response, error in
            
            guard let response = response else {
                print("Error: \(error?.localizedDescription ?? "Unknown error").")
                completion(false, addresList, error?.localizedDescription ?? "")
                
                return
            }
            
            for item in response.mapItems {
                let thoroughfare = item.placemark.thoroughfare ?? ""
                let subThoroughfare = item.placemark.subThoroughfare ?? ""
                let lat = item.placemark.location?.coordinate.latitude ?? 0
                let lng = item.placemark.location?.coordinate.longitude ?? 0
                
                let addressPoint = AddressPoint(
                    country: item.placemark.country,
                    state: item.placemark.administrativeArea,
                    city:  item.placemark.locality,
                    address1: thoroughfare,
                    address2: subThoroughfare,
                    location: CLLocationCoordinate2D(latitude: lat,longitude: lng)
                )
                
                addresList.append(addressPoint)
            }
            
            completion(true, addresList, error?.localizedDescription ?? "")
        }
    }
    
}
