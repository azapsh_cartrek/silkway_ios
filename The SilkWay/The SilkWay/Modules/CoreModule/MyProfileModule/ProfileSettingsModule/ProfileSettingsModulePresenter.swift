//
//  ProfileSettingsModulePresenter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 31/08/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation

protocol ProfileSettingsModuleViewProtocol: class {
    func setUserProfile(userProfile: UserProfile)
}

protocol ProfileSettingsModuleViewPresenter: class {
    init(with view: ProfileSettingsModuleViewProtocol, router: ProfileSettingsModuleRouter)
    func logout()
}

final class ProfileSettingsModulePresenter: ProfileSettingsModuleViewPresenter {

    // MARK: Constants

    // MARK: Properties

    weak var view: ProfileSettingsModuleViewProtocol?
    var router: ProfileSettingsModuleRouter!

    var userProfile: UserProfile?
    
    init(with view: ProfileSettingsModuleViewProtocol, router: ProfileSettingsModuleRouter) {
        self.view = view
        self.router = router
    }
    
    // MARK: Methods
    
    func tapProfileEdit() {
        router?.showProfileEditVC(userProfile: userProfile)
    }
    
    func start() {
        print("start")
        if userProfile == nil {
            userProfile = ProfileService.shared.myProfile
        }
        print("userProfile")
        guard let userProfile = userProfile else { return }
        print("setUserProfile")
        view?.setUserProfile(userProfile: userProfile)
    }

    func logout() {
        AuthService.shared.logOut()
    }
}
