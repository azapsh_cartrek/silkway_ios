//
//  TravelStopOverModuleViewController.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 14/02/2021.
//  Copyright © 2021 SilkWay. All rights reserved.
//

import UIKit
import SnapKit

final class TravelStopOverModuleViewController: UIViewController {
    
    // MARK: Constants
    private enum Constants {
        static let offset: CGFloat = 20
    }
    // MARK: IBOutlets
    
    // MARK: - Subviews
    private let backButton: UIButton = {
        let button = UIButton()
        button.setImage(R.image.back(), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: -20, bottom: 0, right: 0)
        button.contentMode = .scaleAspectFit
        
        return button
    }()
    private let titleLabel: UILabel = {
        let label = UILabel()
        label.font = .h1TitleFont()
        label.textColor = R.color.labelTextColor()
        label.text = R.string.localizable.travelModuleTitle()
        label.numberOfLines = 0
        
        return label
    }()
    
    private let fromButtonView: ButtonView = {
        let buttonView = ButtonView()
        buttonView.configure(action: Action.from)
        buttonView.placeholder = R.string.localizable.travelModuleFromPlaceholder()
        buttonView.onRightButtonAction(action: .from, image: R.image.rightArrow())
        
        return buttonView
    }()
    
    private let deliveryDateButton: ButtonView = {
        let buttonView = ButtonView()
        buttonView.configure(action: Action.selectDate)
        buttonView.placeholder = R.string.localizable.travelModuleStopOverDatePlaceholder()
        buttonView.onLeftButtonAction(action: .selectDate, image: R.image.calendarIcon())
        
        return buttonView
    }()
    private let deliveryTimeButtonView: ButtonView = {
        let buttonView = ButtonView()
        buttonView.configure(action: Action.selectTime)
        buttonView.placeholder = R.string.localizable.travelModuleStopOverTimePlaceholder()
        buttonView.onLeftButtonAction(action: .selectTime, image: R.image.timeIcon())
        
        return buttonView
    }()
    
    private let saveButton: UIButton = {
        let button = UIButton()
        button.setTitle(R.string.localizable.travelModuleStopOverSaveButtonTitle(), for: .normal)
        
        return button
    }()
    
    private let datePicker: UITextView = {
        let picker = UITextView()
        picker.isHidden = true
        
        return picker
    }()
    
    // MARK: Properties
    
    var presenter: TravelStopOverModulePresenter!
    
    // MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func loadView() {
        super.loadView()
        
        view.backgroundColor = R.color.backgroundColor()
        view.addSubview(backButton)
        view.addSubview(titleLabel)
        view.addSubview(fromButtonView)
        view.addSubview(deliveryDateButton)
        view.addSubview(deliveryTimeButtonView)
        view.addSubview(saveButton)
        
        view.addSubview(datePicker)
        datePicker.snp.makeConstraints {
            $0.bottom.equalToSuperview().offset(-16)
            $0.leading.equalToSuperview()
            $0.trailing.equalToSuperview()
        }
        
        backButton.addTarget(self, action: #selector(tapBack), for: .touchUpInside)
        backButton.snp.makeConstraints {
            $0.top.equalToSuperview().offset(24)
            $0.left.equalToSuperview().offset(Constants.offset)
            $0.height.width.equalTo(40)
        }
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(backButton.snp.bottom).offset(16)
            $0.trailing.equalToSuperview().inset(Constants.offset)
            $0.leading.equalToSuperview().offset(Constants.offset)
            $0.height.equalTo(24)
        }
        
        fromButtonView.snp.makeConstraints {
            $0.top.equalTo(titleLabel.snp.bottom).offset(16)
            $0.trailing.equalToSuperview().inset(Constants.offset)
            $0.leading.equalToSuperview().offset(Constants.offset)
            $0.height.equalTo(56)
        }
        fromButtonView.delegate = self
        
        deliveryDateButton.snp.makeConstraints {
            $0.top.equalTo(fromButtonView.snp.bottom).offset(16)
            $0.trailing.equalToSuperview().inset(Constants.offset)
            $0.leading.equalToSuperview().offset(Constants.offset)
            $0.height.equalTo(56)
        }
        deliveryTimeButtonView.snp.makeConstraints {
            $0.top.equalTo(deliveryDateButton.snp.bottom).offset(16)
            $0.trailing.equalToSuperview().inset(Constants.offset)
            $0.leading.equalToSuperview().offset(Constants.offset)
            $0.height.equalTo(56)
        }
        deliveryDateButton.delegate = self
        deliveryTimeButtonView.delegate = self
        
        saveButton.snp.makeConstraints {
            $0.bottom.equalToSuperview().offset(-100)
            $0.trailing.equalToSuperview().inset(Constants.offset)
            $0.leading.equalToSuperview().offset(Constants.offset)
            $0.height.equalTo(52)
        }
        saveButton.setStyle(style: .blue)
        saveButton.addTarget(self, action: #selector(tapSave), for: .touchUpInside)
    }
    
    // MARK: Methods
    func checkDateValidation() -> Bool {
        var result = true
        fromButtonView.checkValidation(minChars: 1, result: &result)
        deliveryDateButton.checkValidation(minChars: 1, result: &result)
        deliveryTimeButtonView.checkValidation(minChars: 1, result: &result)
        
        return result
    }
    func tapSelectDateTime() {
        print(#function)
        let picker: UIDatePicker = UIDatePicker()
        picker.datePickerMode = UIDatePicker.Mode.dateAndTime
        
        if let deliveryDateTime = presenter?.waypointModel.deliveryDateTime {
            picker.date = deliveryDateTime
        } else {
            let calendar = Calendar.current
            var dateComponents: DateComponents? = calendar.dateComponents([.year, .month, .day, .hour, .month], from: Date())
            dateComponents!.month = dateComponents!.month!
            dateComponents?.hour = 10
            dateComponents?.minute = 0
            picker.date = calendar.date(from: dateComponents!)!
        }
        if #available(iOS 13.4, *) {
            picker.preferredDatePickerStyle = .wheels
        }

        datePicker.inputView = picker
        picker.addTarget(self, action: #selector(datePickerValueChanged), for: UIControl.Event.valueChanged)
        datePicker.becomeFirstResponder()
    }
    
    // MARK: IBActions
    @objc func tapBack(_ sender: UIButton) {
        presenter?.close()
    }
    @objc func tapSave(_ sender: UIButton) {
        if checkDateValidation() {
            presenter?.save()
        }
    }
    
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        print(sender.date)
        presenter?.changeDeliveryDateTime(date: sender.date)
    }
    
}

extension TravelStopOverModuleViewController: TravelStopOverModuleViewProtocol {
    func updateView(waypointModel: WaypointModel) {
        
        if let deliveryDateTime = waypointModel.deliveryDateTime {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.medium
            dateFormatter.timeStyle = DateFormatter.Style.none
            deliveryDateButton.title = dateFormatter.string(from: deliveryDateTime)
            dateFormatter.dateStyle = DateFormatter.Style.none
            dateFormatter.timeStyle = DateFormatter.Style.short
            deliveryTimeButtonView.title = dateFormatter.string(from: deliveryDateTime)
        }
        
        if let address = waypointModel.addrssPoint {
            fromButtonView.title = address.streetAddress
        }
        
    }
}

extension TravelStopOverModuleViewController: ButtonViewDelegate {

    func tapAction(action: Action) {
        print(action.rawValue)
        
        switch action {
        case .from:
            presenter?.tapAddressSelect(direction: .from)
            
        case .selectDate:
            tapSelectDateTime()
            
        case .selectTime:
            tapSelectDateTime()
            
        default: break
        }
    }
}
