//
//  RegistrationModulePresenter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 22/06/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD

protocol ProfileEditViewProtocol: class {
    func showMessage(title: String, message: String, buttonTitle: String)
    func configureViews(userProfile: UserProfile)
    func updateUserProfileModel(userProfile: UserProfile) -> UserProfile
}

protocol ProfileEditPresenterProtocol: class {
    init(with view: ProfileEditViewProtocol, router: ProfileEditRouter)
    var newProfileMode: Bool { get set }
    var organizationAvatar: UIImage? {get set }
    var userProfile: UserProfile? { get set }
    var personAvatar: UIImage? { get set }
    
    func start()
    func tapSegmentControl(segmentedIndex: Int)
    func tapSave()
    func selectImage (image: UIImage?)
    func tapAddBankCard()
}

final class ProfileEditPresenter: ProfileEditPresenterProtocol {
    
    // MARK: Constants
    
    // MARK: Properties
    weak var view: ProfileEditViewProtocol?
    var router: ProfileEditRouter?
    var personAvatar: UIImage?
    var organizationAvatar: UIImage?
    var userProfile: UserProfile?
    var newProfileMode = false
    
    init(with view: ProfileEditViewProtocol, router: ProfileEditRouter) {
        self.view = view
        self.router = router
    }
    
    // MARK: Methods
    func start() {
        guard let userProfile = userProfile else { return }
        view?.configureViews(userProfile: userProfile)
    }
    func tapSegmentControl(segmentedIndex: Int) {
        guard var userProfile = userProfile else { return }
        
        if let view = view {
            userProfile = view.updateUserProfileModel(userProfile: userProfile)
            switch segmentedIndex {
            case 0:
                userProfile.type = .individual
                break
            case 1:
                userProfile.type = .organization
                break
            default:
                //
                break
            }
            view.configureViews(userProfile: userProfile)
        }
    }
    
    func selectImage (image: UIImage?) {
        guard let userProfile = userProfile else { return }
        if userProfile.type == .organization {
            organizationAvatar = image
        } else {
            personAvatar = image
        }
    }
    func tapSave() {
        guard var userProfile = userProfile else { return }
        if let view = view {
            userProfile = view.updateUserProfileModel(userProfile: userProfile)
        }
        print("newProfileMode>", newProfileMode)
        print("userProfile.typr>>", userProfile.type.rawValue)
        if userProfile.type == .individual {
            savePersonProfile(userProfile: userProfile, img: personAvatar)
        }
        if userProfile.type == .organization {
            saveOrganizationProfile(userProfile: userProfile, img: organizationAvatar)
        }
    }
    
    func tapAddBankCard() {
        router?.showAddBankCard()
    }
    
    func getMyUserPrifle() {
        ProfileService.shared.getMyProfle { [weak self] (success, error, userProfile) in
            print("getMyUserPrifle>success>", success)
            if success {
                print("getMyUserPrifle>userProfile>fullName>", userProfile?.fullName)
                if let self = self {
                    
                    if let userProfile = userProfile {
                        ProfileService.shared.myProfile = userProfile
                        if self.newProfileMode {
                            self.router?.showPermissionGPSView()
                        }
                        if !self.newProfileMode {
                            self.router?.back()
                        }
                    }
                }
            } else {
                print("чтото пошло не так")
                //
            }
        }
    }
    
    func savePersonProfile(userProfile: UserProfile, img: UIImage?) {
        
        var isAllValidate = true
        
        if userProfile.firstName.isEmpty  {
            isAllValidate = false
        }
        if userProfile.lastName.isEmpty {
            isAllValidate = false
        }
        if img == nil {
            isAllValidate = false
        }
        
        if !isAllValidate {
            view?.showMessage(title: "Ошибка", message: "Пожалуйста, заполните выделеные поля", buttonTitle: "Хорошо")
            return
        }
        
        guard let imageUser = img else { return }
        
        SVProgressHUD.show()
        ProfileService.shared.saveProfile(userProfile: userProfile, imageUser: imageUser, newProfileMode: newProfileMode, completion: { [weak self] (soccess, error)  in
            SVProgressHUD.dismiss()
            print("soccess>", soccess)
            print("error>", error)
            if soccess {
                self?.getMyUserPrifle()
            } else {
                self?.view?.showMessage(title: "Ошибка", message: "" + error , buttonTitle: "Упс")
            }
        })
        
    }
    
    func saveOrganizationProfile(userProfile: UserProfile, img: UIImage?) {
        
        var isAllValidate = true
        
        if userProfile.organizationName.isEmpty {
            isAllValidate = false
        }
        if userProfile.organizationInnNumber.isEmpty {
            isAllValidate = false
        }
        print("userProfile.organizationInnNumber>",userProfile.organizationInnNumber)
        print("userProfile.organizationInnNumber>",userProfile.organizationInnNumber.count)
        
        if userProfile.organizationInnNumber.count != 10 && userProfile.organizationInnNumber.count != 12 {
            isAllValidate = false
        }
        if userProfile.organizationOgrnNumber.isEmpty {
            isAllValidate = false
        }
        if userProfile.organizationOgrnNumber.count != 13 && userProfile.organizationOgrnNumber.count != 15 {
            isAllValidate = false
        }
        if userProfile.organizationRepresentativeName.isEmpty {
            isAllValidate = false
        }
        if userProfile.organizationRepresentativeSurName.isEmpty {
            isAllValidate = false
        }
        if userProfile.organizationRepresentativePosition.isEmpty {
            isAllValidate = false
        }
        if img == nil {
            isAllValidate = false
        }
        
        if !isAllValidate {
            view?.showMessage(title: "Ошибка", message: "Пожалуйста, заполните выделеные поля", buttonTitle: "Хорошо")
            return
        }
        
        guard let imageUser = img else { return }
        
        SVProgressHUD.show()
        ProfileService.shared.saveProfile(userProfile: userProfile, imageUser: imageUser, newProfileMode: newProfileMode, completion: { [weak self] (soccess, error)  in
            SVProgressHUD.dismiss()
            print("soccess>", soccess)
            print("error>", error)
            if soccess {
                self?.getMyUserPrifle()
            } else {
                self?.view?.showMessage(title: "Ошибка", message: error, buttonTitle: "Упс")
            }
        })
    }
}
