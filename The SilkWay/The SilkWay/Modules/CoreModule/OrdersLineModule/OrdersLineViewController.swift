//
//  OrdersLineViewController.swift
//  The SilkWay
//
//  Created by Azapsh on 06.06.2021.
//  Copyright © 2021 Ахмед Фокичев. All rights reserved.
//

import Foundation
import UIKit

final class OrdersLineViewController: UIViewController {
    // MARK: Constants

    // MARK: IBOutlets

    // MARK: Properties
    var presenter: OrdersLinePresenter?

    // MARK: LifeCycle
    override
    func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override
    func loadView() {
        super.loadView()
        
        if #available(iOS 12.0, *) , traitCollection.userInterfaceStyle == .dark {
            view.backgroundColor = .theBlack()
        } else {
            view.backgroundColor = .white
        }
        
    }
    
    // MARK: Methods
    
    // MARK: IBActions

}

extension OrdersLineViewController: OrdersLineViewProtocol {
    
}
