//
//  OrderModel.swift
//  The SilkWay
//
//  Created by Azapsh on 03.07.2021.
//  Copyright © 2021 Ахмед Фокичев. All rights reserved.
//

import ObjectMapper

struct OrderModel: Mappable {
    var id: String?
    var state: String?
    var addressFrom: LocationPointModel?
    var addressTo: LocationPointModel?
    var dataModel: FbDateModel?
    var reward: String?
    var comment: String?
    var weight: Double?
    var compensation: MoneyModel?
    var cost: MoneyModel?
    var isSafeDelivery = false
    var fileUrl: String?
    var owner: String?
    var productName: String?
    
    init?(map: Map) {}

    mutating func mapping(map: Map) {
        id <- map["id"]
        addressFrom <- map["from"]
        addressTo <- map["to"]
        isSafeDelivery <- map["isSafeDelivery"]
        productName <- map["name"]
        weight <- map["weight"]
        comment <- map["comment"]
        owner <- map["owner"]
        fileUrl <- map["fileUrl"]
        cost <- map["cost"]
        compensation <- map["compensation"]
        dataModel <- map["date"]
    }
}
