//
//  LaunchModulePresenter.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 14/05/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import Foundation

protocol LaunchModuleViewProtocol: AnyObject {

}

protocol LaunchModuleViewPresenter: AnyObject {
    init(with view: LaunchModuleViewProtocol, router: LaunchModuleRouter)
}

final class LaunchModulePresenter: LaunchModuleViewPresenter {

    // MARK: Constants

    // MARK: Properties

    weak var view: LaunchModuleViewProtocol?
    var router: LaunchModuleRouter!
    var autoRoute = true
    init(with view: LaunchModuleViewProtocol, router: LaunchModuleRouter) {
        self.view = view
        self.router = router
    }
    
    // MARK: Methods
    func start() {
        print("start>")
        AuthService.shared.startAuth() { success in
            print("startAuth>success>",success)
            if success {
                ProfileService.shared.getMyProfle { [weak self] (success, error, userProfile) in
                    print("ProfileService.shared.getMyProfle>success>",success)
                    if let userProfile = userProfile, let self = self {
                        ProfileService.shared.myProfile = userProfile
                        if self.autoRoute {
                            self.router?.showCore()
                        }
                    } else {
                        self?.autoRoute = false
                        self?.router?.showLoginModule()
                    }
                }
                
            } else {
                self.autoRoute = false
                self.router?.showLoginModule()
            }
        }
    }
}
