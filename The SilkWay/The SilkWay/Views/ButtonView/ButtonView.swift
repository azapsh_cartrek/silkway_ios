//
//  TextFieldView.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 14.06.2020.
//  Copyright © 2020 Ахмед Фокичев. All rights reserved.
//
import UIKit
import Foundation

protocol ButtonViewDelegate: AnyObject {
    func tapAction(action: Action)
    func tapLeftAction(action: Action)
    func tapRightAction(action: Action)
}

extension ButtonViewDelegate {
    func tapLeftAction(action: Action) {}
    func tapRightAction(action: Action) {}
}

class ButtonView: UIView {
    weak var delegate: ButtonViewDelegate?
    
    @IBOutlet weak var widthLeftButton: NSLayoutConstraint!
    @IBOutlet weak var widthrightButton: NSLayoutConstraint!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var widthTitleLabelCostraint: NSLayoutConstraint!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var mainButton: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var MainButtonBottomConstraint: NSLayoutConstraint!
    
    var placeholderColor: UIColor? {
        didSet (value) {
            update()
        }
    }

    var titleColor: UIColor? {
        didSet (value) {
            update()
        }
    }
    
    var placeholder: String? {
        didSet (value) {
            update()
        }
    }
    
    var title: String? {
        didSet (value) {
            update()
        }
    }
    
    var leftButtonAction: Action?
    var rigthButtonAction: Action?
    var mainAction: Action?
    
    private var validCounts = [Int]()
    var validIfNotNil = false
    var upFirstChar = true
    
    // MARK: Methods
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        guard let nib = R.nib.buttonView.firstView(owner: self) else {
            return
        }
        addSubview(nib)
        nib.pin(to: self)
    }
    
    @IBAction func tapButton(_ sender: Any) {
        guard let action = mainAction else { return }
        delegate?.tapAction(action: action)
    }
    
    @IBAction func didTextChanged(_ sender: Any) {
        update()
    }
    
    func update() {
    
        borderView.layer.borderColor = R.color.textFieldViewBorderColor()?.cgColor
        borderView.layer.borderWidth = 1
        borderView.layer.cornerRadius = 5
        borderView.backgroundColor = R.color.backgroundColor()
    
        var buttonTitle = title ?? ""
        let buttonPlaceholder = placeholder ?? ""
        let titleColor = self.titleColor ?? R.color.buttonViewTitleColor()
        let placeholderColor = self.placeholderColor ?? R.color.buttonViewTitleColorPlaceHolder()
        
        if buttonTitle.isEmpty {
            buttonTitle = buttonPlaceholder
            titleLabel.isHidden = true
            mainButton.setTitleColor(placeholderColor, for: .normal)
        } else {
            titleLabel.isHidden = false
            titleLabel.text = placeholder
            mainButton.setTitleColor(titleColor, for: .normal)
        }
        
       
        mainButton.setTitle(buttonTitle, for: .normal)
        titleLabel.text = "  " + buttonPlaceholder + "  "
        widthTitleLabelCostraint.constant = titleLabel.intrinsicContentSize.width
        
    }
    
    func checkValidation (minChars: Int? = 0, _ validCounts: [Int]? = nil, result: inout Bool ) {
       
        let text = title ?? ""
        var validation = true
        
        if let min = minChars,  min > text.count {
            validation = false
            result = false
        }
        
        if let counts = validCounts, counts.count > 0, !counts.contains(text.count) {
            validation = false
            result = false
        }
        
        borderView.layer.borderWidth = 1
        borderView.layer.cornerRadius = 5
        borderView.layer.borderColor = validation ? R.color.textFieldViewBorderColor()?.cgColor : R.color.textFieldViewBorderColorNoValid()?.cgColor
    }
    
    func setNotValidBorder() {
        borderView.layer.borderWidth = 1
        borderView.layer.cornerRadius = 5
        borderView.layer.borderColor = R.color.textFieldViewBorderColorNoValid()?.cgColor
    }
    
    func checkValidationContent (minChars: Int? = 0, _ validCounts: [Int]? = nil, result: inout Bool ) {
       
        let text = title ?? ""
        var validation = true
        
        if let min = minChars,  min > text.count {
            validation = false
            result = false
        }
        
        if let counts = validCounts, counts.count > 0, !counts.contains(text.count) {
            validation = false
            result = false
        }
        
        borderView.layer.borderWidth = 1
        borderView.layer.cornerRadius = 5
        borderView.layer.borderColor = validation ? R.color.textFieldViewBorderColor()?.cgColor : R.color.textFieldViewBorderColorNoValid()?.cgColor
    }
    
    func onLeftButtonAction(action: Action, image: UIImage?) {
        leftButtonAction = action
        leftButton.setImage(image, for: .normal)
        widthLeftButton.constant = 32
        leftButton.isHidden = false
    }
    func onRightButtonAction(action: Action, image: UIImage?) {
        print(#function,action.rawValue)
        rigthButtonAction = action
        rightButton.setImage(image, for: .normal)
        widthrightButton.constant = 32
        rightButton.isHidden = false
    }
    
    func configure(action: Action) {
        if leftButtonAction == nil {
            widthLeftButton.constant = 0
            leftButton.isHidden = true
        }
        if rigthButtonAction == nil {
            widthrightButton.constant = 0
            rightButton.isHidden = true
        }
        mainAction = action
        
        titleLabel.backgroundColor = R.color.backgroundColor()
        mainButton.backgroundColor = R.color.backgroundColor()

        update()
    }
    func setDescription(text: String, textColor: UIColor? = R.color.infoLabelTextColor() ) {
        descriptionLabel.isUserInteractionEnabled = false
        descriptionLabel.isHidden = text.isEmpty
        descriptionLabel.text = text
        descriptionLabel.textColor = textColor
        descriptionLabel.isHidden = false
        
        if descriptionLabel.isHidden {
            MainButtonBottomConstraint.constant = 2
        } else {
            MainButtonBottomConstraint.constant = 8
        }
    }
    func setFont(font: UIFont) {
        mainButton.titleLabel?.font = font
    }

    func setValidateCounts(_ values: [Int]) {
        //validCounts = values
    }
    @IBAction func tapRightButton(_ sender: Any) {
        guard let action = rigthButtonAction else { return }
        delegate?.tapRightAction(action: action)
    }
    @IBAction func tapLeftButton(_ sender: Any) {
        guard let action = leftButtonAction else { return }
        delegate?.tapLeftAction(action: action)
    }
}
