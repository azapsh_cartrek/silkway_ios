//
//  SMSSignModuleViewController.swift
//  The SilkWay
//
//  Created by Ахмед Фокичев on 24/05/2020.
//  Copyright © 2020 SilkWay. All rights reserved.
//

import UIKit

final class SMSSignModuleViewController: UIViewController , UITextFieldDelegate{
    // MARK: Constants

    // MARK: IBOutlets
    @IBOutlet weak var titlePhoneViewLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var smsCodeText: UITextField!
    @IBOutlet weak var mainButton: UIButton!
    @IBOutlet weak var phoneNumberView: UIView!
    @IBOutlet weak var acceptTextLabel1: UILabel!
    @IBOutlet weak var acceptTextLabel2: UILabel!
    @IBOutlet weak var acceptTextLabel3: UILabel!
    @IBOutlet
    private weak var leadingContainerViewConstraint: NSLayoutConstraint! {
        didSet {
            leadingContainerViewConstraint.constant = 20
        }
    }
    @IBOutlet
    private weak var trailingContainerViewConstraint: NSLayoutConstraint! {
        didSet {
            trailingContainerViewConstraint.constant = 20
        }
    }
    @IBOutlet weak var widthTitlePhoneViewConstraint: NSLayoutConstraint!
    
    // MARK: Properties
    
    var presenter: SMSSignModulePresenter!
    
    // MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        smsCodeText.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        if #available(iOS 12.0, *) , traitCollection.userInterfaceStyle == .dark {
            view.backgroundColor = .theBlack()
        } else {
            view.backgroundColor = .white
        }
        mainButton.setStyle(style: .blue)
        titleLabel.font = .h1TitleFont()
        titleLabel.text = R.string.localizable.loginModule_Title()
        infoLabel.font = .subtitleFont()
        infoLabel.text = R.string.localizable.loginModule_info()
        infoLabel.backgroundColor = .clear
        titlePhoneViewLabel.font = .captionFont()
        titlePhoneViewLabel.text = " " + R.string.localizable.smsSingModule_titlePhoneLabel() + " "
        widthTitlePhoneViewConstraint.constant = titlePhoneViewLabel.intrinsicContentSize.width
        
        if #available(iOS 12.0, *) , traitCollection.userInterfaceStyle == .dark {
            titlePhoneViewLabel.backgroundColor = UIColor.theBlack()
            titlePhoneViewLabel.textColor = .greyLite2()
            smsCodeText.backgroundColor = .theBlack()
            
        } else {
            titlePhoneViewLabel.textColor = .darkgrey()
            titlePhoneViewLabel.backgroundColor = .white
             smsCodeText.backgroundColor = .white
        }
        
        mainButton.setTitle(R.string.localizable.smsSingModule_titleBottonPhone(), for: .normal)
        acceptTextLabel1.text = R.string.localizable.loginModule_acceptTerms1()
        
        let acceptTerms2 = NSMutableAttributedString.init(string: R.string.localizable.loginModule_acceptTerms2())
        var range = NSMakeRange(0, acceptTerms2.length)
        acceptTerms2.addAttribute(.underlineStyle, value: NSUnderlineStyle.patternDot.rawValue|NSUnderlineStyle.single.rawValue, range: range)
        acceptTextLabel2.attributedText = acceptTerms2
        
        let acceptTerms3 = NSMutableAttributedString.init(string: R.string.localizable.loginModule_acceptTerms3())
        range = NSMakeRange(2, acceptTerms3.length-2)
        acceptTerms3.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.patternDot.rawValue|NSUnderlineStyle.single.rawValue, range: range)
        acceptTextLabel3.attributedText = acceptTerms3
        
        smsCodeText.placeholder = R.string.localizable.smsSingModule_titlePhoneLabel()
        phoneNumberView.setBorder()
    }
    
    // MARK: Methods
    
    // MARK: IBActions
    @IBAction func tapBack(_ sender: Any) {
        presenter.tapBack()
    }
    @IBAction func tapMainButton(_ sender: Any) {
        if let code = smsCodeText.text {
            presenter?.sign(verificationCode: code)
        }
    }
    
    @IBAction func changeCode(_ sender: Any) {
       updateTitles()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        smsCodeText.tag = 1
        updateTitles()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
          smsCodeText.tag = 0
          updateTitles()
    }
    
    func updateTitles() {
        if let surname = smsCodeText.text {
            if surname.isEmpty {
                titlePhoneViewLabel.isHidden = true
                if smsCodeText.tag == 1 {
                    titlePhoneViewLabel.isHidden = false
                }
            } else {
                titlePhoneViewLabel.isHidden = false
            }
        }
    }
    
}

extension SMSSignModuleViewController: SMSSignModuleViewProtocol {
    
}
